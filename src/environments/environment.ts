// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  prodHideLinks : true,
  imedicUrl: 'http://10.1.12.201/mycareemr/',
  apiEndPoint: 'http://10.1.12.201/microservices/public',
  apiEndPoint2: 'http://10.1.1.135/apigateway/api/v1',
  showAllErrors: true,
  ssrsurl: 'http://10.1.1.167/ReportServer_SQL01/Pages/ReportViewer.aspx?%2fMCM%2f'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
