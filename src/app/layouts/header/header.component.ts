import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpeechRecognitionService } from '@app/core/services/speech-recognition/speech-recognition.service';


@Component({
  selector: 'app-header-component',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent implements OnInit {
  // Emit Layout functions


  @Output() logOutEvent: EventEmitter<any> = new EventEmitter();
  @Output() infoMsgShow: EventEmitter<any> = new EventEmitter();
  @Output() goToDashBoard: EventEmitter<any> = new EventEmitter();
  @Input() username: any;
  @Input() hideLinks: boolean;
  @Input() patientId: any;
  @Input() loggedinTimeZone: any;
  /**
   * Output  of header component
   */
  @Output() reload_Pages: EventEmitter<any> = new EventEmitter();
  /**
   * Hide html drop of header component
   */
  hideHtmlDrop: any;

  /**
   * Today  of header component
   * dispaly the current date
   */
  today: number = Date.now();

  /**
   * Logged time zone of header component
   */
  LoggedTimeZone: any;

  /**
   * Showtime zone of header component
   */
  showtimeZone: boolean;
  /**
   * Normaltime  of header component
   */
  normaltime: boolean;
  emrEnabled: boolean;
  disabledList: string;

  /**
   * @ignore
   */
  @Input() app: any;

  /**
   * Speech mic of header component for storing the css class of mic
   */
  speechMic: string;
  
  /**
   * Creates an instance of header component.
   * @param router for navigation
   * @param speechRecognitionService  for getting speech text data
   */
  constructor(
    private router: Router,
    private speechRecognitionService: SpeechRecognitionService
  ) { }
  ngOnInit(): void {
    this.speechMic = 'icon-mic';
    this.showtimeZone = false;
    this.normaltime = false;
    this.hideHtmlDrop = this.hideLinks;
    if (this.loggedinTimeZone.length > 0) {
      this.LoggedTimeZone = this.loggedinTimeZone;
      this.showtimeZone = true;
      this.normaltime = false;
    } else {
      this.showtimeZone = false;
      this.normaltime = true;
    }
    this.liveTimer();
    this.emrDisabled();
  }
  /**
   * Lives time
   * for showing live time
   */
  liveTimer() {
    setInterval(() => {
      this.today = Date.now();
    }, 1000);
  }

  /**
   * Completes examclick
   * If no patient is selected then it is taking to iMW Exam screen. So,
   * this code will block to take to iMW Exam screen when no patient selected.
   */
  authPages(event) {
    if (this.patientId > 0) {
      switch (event) {
        case 'completeExam': {
          this.router.navigateByUrl('/exam/complete-exam');
          break;
        }
        case 'contactlensExam': {
          this.router.navigateByUrl('/exam/contact-lens-exam');
          break;
        }
        case 'procedureExam': {
          this.router.navigateByUrl('/exam/procedures');
          break;
        }
        case 'patientInstructions': {
          this.router.navigateByUrl('/exam/patient-instructions');
          break;
        }
        case 'paymentLedgers': {
          this.router.navigateByUrl('/').then(success => {
            this.router.navigateByUrl('/accounting/payment-ledgers');
          });
          break;
        }
        case 'demographics': {
          this.router.navigate(['/patient/demographics'], { queryParams: { grid: 'demographics' } });
          break;
        }
        case 'patFamily': {
          this.router.navigate(['/patient/demographics'], { queryParams: { grid: 'family' } });
          break;
        }
        case 'patientinsurance': {
          this.router.navigateByUrl('/patient/insurance');
          break;
        }
        case 'patientdocuments': {
          this.router.navigateByUrl('/patient/documents');
          break;
        }
        case 'patocular': {
          this.router.navigateByUrl('/patient-history/ocular');
          break;
        }
        case 'patgeneral-health': {
          this.router.navigateByUrl('/patient-history/general-health');
          break;
        }
        case 'patmedication': {
          this.router.navigateByUrl('/patient-history/medication');
          break;
        }
        case 'patallergies': {
          this.router.navigateByUrl('/patient-history/allergies');
          break;
        }
        case 'sx_procedures': {
          this.router.navigateByUrl('/patient-history/sx_procedures');
          break;
        }
        case 'immunizations': {
          this.router.navigateByUrl('/patient-history/immunizations');
          break;
        }
        case 'cc-history': {
          this.router.navigateByUrl('/patient-history/cc-history');
          break;
        }
        case 'vital-signs': {
          this.router.navigateByUrl('/patient-history/vital-signs');
          break;
        }
        case 'patOrder-sets': {
          this.router.navigateByUrl('/patient-history/order-sets');
          break;
        }
        case 'patProblem-list': {
          this.router.navigateByUrl('/patient-history/problem-list');
          break;
        }
        case 'patientLab': {
          this.router.navigateByUrl('/patient-history/lab');
          break;
        }
        case 'patientRad': {
          this.router.navigateByUrl('/patient-history/rad');
          break;
        }
        case 'patientTests': {
          this.router.navigateByUrl('/patient-history/tests');
          break;
        }
      }

    } else {
      this.goToDashBoard.emit('noPatient');
    }
  }
  /**
   * Emrs disabled for enable/disable the drop-downs
   */
  emrDisabled() {
    const emrDisabled = this.app.emrDisabled;
    if (emrDisabled === 0) {
      this.emrEnabled = false;
      this.disabledList = 'EmrDisabledList';
    } else {
      this.emrEnabled = true;
      this.disabledList = '';
    }
  }
/**
 * Orders drop click
 */
  orderDropClick() {
      this.app.bannerOrderId = null;
    }

    /**
     * Mouses leave for deactivating the speech while mouse leaving the mic icon
     */
    mouseLeave() {
      this.speechMic = 'icon-mic';
      this.speechRecognitionService.DestroySpeechObject();
    }

  /**
   * Listners on for lisiting the speech while mouse enter the mic icon
   */
  listnerOn(): void {
    this.speechMic = 'icon-mic-on';
    console.log('yes');
    this.speechRecognitionService.record()
      .subscribe(
        (value) => {
          console.log(value);
          if (value.includes('patient')) {
            const str1 = value.replace(/[^\d.]/g, '');
            if (str1) {
                this.app.patientId = 0;
                this.app.patientId = str1;
                this.app.isPatientSigned = true;
                this.router.navigate(['patient/demographics'], { queryParams: { grid: 'demographics' } });
            }
          }
          if (value === 'logout' || value === 'lockout' ||
            value === 'plazo suit' || value === 'Lago' ||
            value === 'about' || value === 'Lockout') {
            this.router.navigate(['login']);
            this.speechRecognitionService.DestroySpeechObject();
          }
          if (value === 'patient search') {
            this.router.navigate(['/patient/search']);
            this.speechRecognitionService.DestroySpeechObject();
          }
          if (value === 'add patient' || value === 'new patient') {
            this.router.navigate(['/patient/add-patient']);
            this.speechRecognitionService.DestroySpeechObject();
          }
          if (value === 'patient document' || value === 'patient documents') {
            this.router.navigate(['/reports/scheduler/patient-documents']);
            this.speechRecognitionService.DestroySpeechObject();
          }
          if (value === 'daily reports' || value === 'day reports' ||
            value === 'day report' || value === 'weather reports' || value === 'daily report') {
            this.router.navigate(['/reports/scheduler'], { queryParams: { sch_temp_id: 2 } });
            this.speechRecognitionService.DestroySpeechObject();
          }
          if (value === 'tracker' || value === 'tractor' || value === 'crackers' || value === 'Agar' || value === 'Trackon') {
            this.router.navigate(['/patient/track']);
            this.speechRecognitionService.DestroySpeechObject();
          }
          this.speechRecognitionService.DestroySpeechObject();
        },
        (err) => {
          console.log(err);
          if (err.error === 'no-speech') {

          }
        },
        () => {
        });

  }
}
