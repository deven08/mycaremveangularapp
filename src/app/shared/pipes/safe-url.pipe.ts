import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform {
  constructor(public sanitizer: DomSanitizer) {

  }

  transform(value: any, args?: any): any {
    return null;
  }
  safeUrl(url) {
     return  this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
