import { Component, Input, OnChanges, ViewChild, Renderer2, ElementRef } from '@angular/core';

declare var require: any;

let jsbarcode = require('jsbarcode');

@Component({
  selector: 'ngx-barcode6',
  template: `<div #bcElement></div>`,
  styles: []
})
export class NgxBarcodeComponent implements OnChanges {

  /**
   * Input  of ngx barcode component for element type
   */
  @Input('bc-element-type') elementType: 'svg' | 'img' | 'canvas' = 'svg';
//   @Input('bc-class') cssClass = 'barcode'; // this should be done more elegantly

  /**
   * Input  of ngx barcode component for formate
   */
  @Input('bc-format') format: '' | 'CODE128' | 'CODE128A' | 'CODE128B' | 'CODE128C' | 'EAN' | 'UPC' | 'EAN8' | 'EAN5' |
  'EAN2' | 'CODE39' | 'ITF14' | 'MSI' | 'MSI10' | 'MSI11' | 'MSI1010' | 'MSI1110' | 'pharmacode' | 'codabar' = 'CODE128';
  /**
   * Input  of ngx barcode component  for line color
   */
  @Input('bc-line-color') lineColor = '#000000';
  /**
   * Input  of ngx barcode component for width
   */
  @Input('bc-width') width = 2;
  /**
   * Input  of ngx barcode component for height
   */
  @Input('bc-height') height = 100;
  /**
   * Input  of ngx barcode component for value
   */
  @Input('bc-display-value') displayValue = false;
  /**
   * Input  of ngx barcode component for font options
   */
  @Input('bc-font-options') fontOptions = '';
  /**
   * Input  of ngx barcode component for font
   */
  @Input('bc-font') font = 'monospace';
  /**
   * Input  of ngx barcode component for text align
   */
  @Input('bc-text-align') textAlign = 'center';
  /**
   * Input  of ngx barcode component for text position
   */
  @Input('bc-text-position') textPosition = 'bottom';
  /**
   * Input  of ngx barcode component for text margin
   */
  @Input('bc-text-margin') textMargin = 2;
  /**
   * Input  of ngx barcode component for font size
   */
  @Input('bc-font-size') fontSize = 20;
  /**
   * Input  of ngx barcode component for background
   */
  @Input('bc-background') background = '#ffffff';
  /**
   * Input  of ngx barcode component for margin
   */
  @Input('bc-margin') margin = 10;
  /**
   * Input  of ngx barcode component for margin top
   */
  @Input('bc-margin-top') marginTop = 10;
  /**
   * Input  of ngx barcode component for margin bottom
   */
  @Input('bc-margin-bottom') marginBottom = 10;
  /**
   * Input  of ngx barcode component for margin left
   */
  @Input('bc-margin-left') marginLeft = 10;
  /**
   * Input  of ngx barcode component for margin right
   */
  @Input('bc-margin-right') marginRight = 10;
  /**
   * Input  of ngx barcode component for value
   */
  @Input('bc-value') value = '';
  /**
   * View child of ngx barcode component for element
   */
  @ViewChild('bcElement') bcElement: ElementRef;

  /**
   * Input  of ngx barcode component for valid
   */
  @Input('bc-valid') valid: () => boolean = () => true;


  /**
   * Gets options
   * for returning the complete barcode events
   */
  get options() {
    return {
      format: this.format,
      lineColor: this.lineColor,
      width: this.width,
      height: this.height,
      displayValue: this.displayValue,
      fontOptions: this.fontOptions,
      font: this.font,
      textAlign: this.textAlign,
      textPosition: this.textPosition,
      textMargin: this.textMargin,
      fontSize: this.fontSize,
      background: this.background,
      margin: this.margin,
      marginTop: this.marginTop,
      marginBottom: this.marginBottom,
      marginLeft: this.marginLeft,
      marginRight: this.marginRight,
      valid: this.valid,
    };
  }
  constructor(private renderer: Renderer2) { }

  /**
   * on changes
   * for creating brcode labels
   */
  ngOnChanges() {
    this.createBarcode();
  }

  /**
   * Creates barcode
   * @returns  {object} for getting barcode result
   */
  createBarcode() {
    if (!this.value) { return; };
    let element: Element;
    switch (this.elementType) {
      case 'img':
        element = this.renderer.createElement('img');
        break;
      case 'canvas':
        element = this.renderer.createElement('canvas');
        break;
      case 'svg':
      default:
        element = this.renderer.createElement('svg', 'svg');
    }

    jsbarcode(element, this.value, this.options);

    for (let node of this.bcElement.nativeElement.childNodes) {
      this.renderer.removeChild(this.bcElement.nativeElement, node);
    }
    this.renderer.appendChild(this.bcElement.nativeElement, element);

  }

}
