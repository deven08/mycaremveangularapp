import { TestBed } from '@angular/core/testing';

import { IframesService } from './iframes.service';

describe('IframesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IframesService = TestBed.get(IframesService);
    expect(service).toBeTruthy();
  });
});
