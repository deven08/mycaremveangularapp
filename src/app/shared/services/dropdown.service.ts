import { ApiService } from '@services/api.service';
import { Injectable } from '@angular/core';
import { ErrorService } from '@services/error.service';
import { FrameStyleMaster, Material, MasterDropDown, LocationsMaster, TransactionType, PaymentTerms } from '@app/core/models/masterDropDown.model';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {

  locations: object;
  inventoryLocation: any;
  APIstring: string;
  Mainurl: any;
  Accesstoken: any;
  Manufactures: Array<MasterDropDown>;
  brands: any = [];
  colors: any = [];
  lensTreatments: any = [];
  lensTreatmentsList: any = [];
  framesshapes: any = [];
  materialsvalues: any = [];
  lensLab: Array<object>;
  styles: any = [];
  /**
   * Materials  of dropdown service for binding material dropdata.
   */
  materials: Array<object>;
  supplierlist: any = [];
  framedatalist: any = [];
  POtype: any = [];
  colorsData: Array<object>;
  /**
   * Purchaselocations  of dropdown service
   */
  purchaselocations: any = [];
  /**
   * Lablist  of dropdown service for binding lab data
   */
  lablist: any = [];
  /**
   * Labstatus  of dropdown service for binding labstatus data
   */
  labstatus: any = [];
  /**
   * Reprocessreason  of dropdown service for binding Reprocessreason data
   */
  Reprocessreason: any = [];
  /**
   * Sold by of dropdown service for binding soldBy data
   */
  soldBy: any = [];
  /**
   * Fittedby  of dropdown service for binding fittedby data
   */
  fittedby: any = [];
  /**
   * Inspectedbydata  of dropdown service for binding inspectedby data
   */
  inspectedbydata: any = [];
  /**
   * Deliveredbdata  of dropdown service for binding delivere data
   */
  deliveredbdata: any = [];
  /**
   * Order locations of dropdown service for binding orderLocations data
   */
  orderLocations: any = [];
  /**
   * Frames rim type of dropdown service for binding RimType data
   */
  framesRimType: any = [];
  /**
   * Frame shapes of dropdown service for binding Shapes data
   */
  frameShapes: any = [];
  /**
   * Frame types of dropdown service for binding frameTypes data
   */
  frameTypes: any = [];
  /**
   * Frames usage of dropdown service for binding usage data
   */
  framesUsage: any = [];
  /**
   * Framename  of dropdown service for binding framename data
   */
  framename: any = [];
  /**
   * Lens treatments of dropdown service for binding LensTreatments data
   */
  LensTreatments: any = [];
  /**
   * Treat category of dropdown service for binding treatCategory data
   */
  treatCategory: any = [];


  treatCategorylist: any = [];

  /**
  * Lens treatment of dropdown service
  * @returns storing categories data
  */
  lensTreatment: any = [];
  /**
  * Lens material of dropdown service
  * For Handling lensmaterial data
  */
  lensMaterial: Array<object>;
  /**
  * Rim type of dropdown service
  * @returns storing rimtype data
  */
  rimType: any = [];

  /**
   * Provider  of dropdown service
   *  @returns storing provider data
   */
  provider: any = [];
  /**
   * Framesmaterials  of dropdown 
   * @returns storing Material drop data
   */

  /**
   * Returns  of dropdown service
   */
  orderType: object;
  /**
   * Status  of dropdown service
   */
  status:any =[];



  /**
   * Lens colors for Cache mechanism 
   */
  lensColors: Array<object>;

  /**
   * Framesmaterials  of dropdown service
   */
  framesmaterials: Material[];
  /**
   * Frame styles of dropdown service
   */
  frameStyles: Array<FrameStyleMaster>;
  /**
   * Frame supplier of inventory service
   * storing supplier data
   */
  frameSupplier: Array<object>;

  /**
   * Locations data of dropdown service for cache
   */
  locationsData: Array<LocationsMaster>;

  /**
   * Transaction type of dropdown service for cache
   */
  transactionType: Array<object>;
  /**
   * Payment terms of dropdown service for cache
   */
  paymentTerms: Array<PaymentTerms>;
  constructor(private api: ApiService, public error: ErrorService) {
    this.locations = [];
    this.inventoryLocation = [];
    this.orderType = [];
    this.status = [];
    this.Manufactures = [];
    this.lensColors = [];
    this.frameStyles = [];
    this.framesmaterials = [];
    this.lensLab = [];
    this.colorsData = [];
    this.locationsData = [];
    this.transactionType = [];
    this.paymentTerms = [];
    this.frameSupplier = [];
    this.materials = [];
    this.lensMaterial = [];
  }

  getLocations() {

    const path = 'IMWAPI/optical/getLocations';
    return this.api.GetApi(path);
  }

  getInventtoryLocations() {
    const path = 'IMWAPI/optical/getLocations';
    return this.api.GetApi(path);

  }
  //  dropdown service
  getDemographics(id: any) {
    // this._AppSettings.getToken()

    const mainpart = 'IMWAPI/getDemographics';
    const inputPart = '&patientId=' + id;
    return this.api.GetApi(mainpart, inputPart);
  }
  // dropdown service
  getManufactures() {
    // this._AppSettings.getToken()

    const mainpart = 'IMWAPI/optical/getManufacturers';
    const inputPart = '&includeInactive=&sortBy=Id';
    return this.api.GetApi(mainpart, inputPart);
  }




  /**
   * Gets brands
   * @returns  for getting brands dropdown values
   */
  getBrands() {
    this.Accesstoken = '';
    const mainpart = '/frame/brand';
    return this.api.getApiMicro(mainpart);
  }


  /**
   * Gets colors
   * @returns   frame colors
   */
  getColors() {
    const mainpart = '/frame/color';
    return this.api.getApiMicro(mainpart);
  }

  // dropdown.service
  getLensCategories() {
    this.Mainurl = '/spectacleLens/treatmentCategory';
    return this.api.getApiMicro(this.Mainurl);
  }


  // dropdown.service
  getFrameShapes() {
    this.Accesstoken = '';
    // this._StateInstanceService.accessTokenGlobal;
    this.APIstring = 'IMWAPI/optical/getFrameShapes';
    return this.api.GetApi(this.APIstring);
  }

  // dropdown.service
  getSpectaclesLensMaterialListDetails(Id: any, Name: any, includeInactive: any, maxRecord: any, offset: any, sortBy: any, sortOrder: any) {
    // this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    const mainpart = 'IMWAPI/optical/getSpectaclesLensMaterialList';
    const inputPart = '&Id=' + Id + '&Name=' + Name + '&includeInactive=' +
      includeInactive + '&maxRecord=' + maxRecord + '&sortBy=' + sortBy + '&sortOrder=' + sortOrder + '   ';


    return this.api.GetApi(mainpart, inputPart);
  }

  // dropdown.service
  /**
   * Gets lens lab details
   * @returns  for getting labs dropdown data
   */
  getLensLabDetails() {
   
    this.Mainurl = '/lensLab';
    return this.api.getApiMicro(this.Mainurl);
  }

  // dropdown.service
  /**
   * Gets spectacle lens style
   * @returns  for getting spectacle lens style dropdown data
   */
  getSpectacleLensStyle() {
    this.Mainurl = '/spectacleLens/style';
    return this.api.getApiMicro(this.Mainurl);
  }
  /**
   * Gets frame lens style
   * @returns  for getting frame lens style dropdown data
   */
  getFrameLensStyle() {
    this.Mainurl = '/frame/style';
    return this.api.getApiMicro(this.Mainurl);
  }
  // dropdown.service
  getFrameLenscolor() {
    this.Mainurl = '/spectacleLens/color';
    return this.api.getApiMicro(this.Mainurl);
  }
  // dropdown.service
  getFrameMaterial() {
    this.Mainurl = '/frame/material';
    return this.api.getApiMicro(this.Mainurl);
  }

  /**
   * Gets lens material
   * @returns materials data 
   */
  getLensMaterial() {
    this.Mainurl = '/spectacleLens/material';
    return this.api.getApiMicro(this.Mainurl);
  }

  getframesdetails(upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '') {
    const mainpart = 'IMWAPI/optical/getFrames';
    const inputPart = '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' +
      colorId + '&maxRecord=' + maxRecord + '';
    return this.api.GetApi(mainpart, inputPart);
  }

  getFrameTypes() {
    // this.Mainurl = this._AppSettings.ImedicUrl;
    // this.Accesstoken = this._StateInstanceService.accessTokenGlobal;
    // this.APIstring = this.Mainurl + 'IMWAPI/optical/getFrameTypes?accessToken=' + this.Accesstoken;
    // return this.http.get<any[]>(this.APIstring)
    //   .do(data => data);
    const mainpart = 'IMWAPI/optical/getFrameTypes';
    return this.api.GetApi(mainpart);
  }

  getUsage() {
    const mainpart = '/frame/usage/';
    return this.api.getApiMicro(mainpart);
  }

  getLabsVender() {
    const mainpart = '/vendor';
    // console.log(this.mainpart);
    return this.api.getApiMicro(mainpart);
  }

  getUsers() {
    const mainpart = '/users';
    return this.api.getApiMicro(mainpart);
  }

  /**
   * Gets reporcessdata
   * @returns  for getting ReprocessReason dropdown data
   */
  GetReporcessdata() {
    const mainpart = '/reprocessReason';
    return this.api.getApiMicro(mainpart);
  }
  getRimType() {
    const mainpart = '/frame/rimType';
    return this.api.getApiMicro(mainpart);
  }

  /**
   * Gets patient insurance will pull the list of insurances related to patient*
   * Input parameter
   * PatientID- the selected patientID will be passed as an input parameter*
   */
  getPatientInsurance(PatientID: string) {
    return this.api.getApiMicro('/patient/' + PatientID + '/insurance');
  }

  /**
   * Patients alerts gives the all the patients alerts avalible
   * @param {string} PatientID 
   * @returns  resp for the subscription
   */
  patientAlerts(PatientID: string) {
    return this.api.getApiMicro('/patient/' + PatientID + '/alert');
  }

  getInsuranceMaster(){
    return this.api.getApiMicro('/insuranceCompanies');
  }


  /**
   * Loads order type
   * @returns  ordertype details
   */
  loadOrderType() {
    const Mainurl =  '/orderTypes ';
    return this.api.getApiMicro(Mainurl);
  }




  /**
   * Loads orderstatus
   * @returns  order status details
   */
  loadOrderstatus() {
    const Mainurl = '/orderStatus ';
    return this.api.getApiMicro(Mainurl);
  }

    /**
     * Loads manufactures
     * @returns  manufactures dropdown values
     */
  manufacturesDropDown(reqbody) {
    const Mainurl = '/manufacturer/filter';
    return this.api.postApiMicro(Mainurl, reqbody);
  }


  getContactlensColors(id: '') {
    const Mainurl = '/contactLens/color/' + id;
    return this.api.getApiMicro(Mainurl);
  }


  /**
   * Gets supplier drop data
   * @returns  for getting supplier dropdown data
   */
  getSupplierDropData() {
    const Mainurl = '/vendor';
    return this.api.getApiMicro(Mainurl);
  }

  /**
   * Gets Locations drop data
   * @returns  for getting Locations dropdown data
   */
  getLocationsDropData() {
    const Mainurl = '/locations/dropdown';
    return this.api.getApiMicro(Mainurl);
  }
  /**
   * Gets transactionType drop data
   * @returns  for getting transactionType dropdown data
   */
  getTypeTrasactionframeData() {
    const mainpart = '/transactionType';
    return this.api.getApiMicro(mainpart);
  }
  /**
   * Gets paymentTerms drop data
   * @returns  for getting paymentTerms dropdown data
   */
  getPaymentTerms() {
    const mainpart = '/paymentTerms';
    return this.api.getApiMicro(mainpart);
  }
}
