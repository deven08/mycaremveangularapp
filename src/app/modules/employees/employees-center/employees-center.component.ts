import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';


@Component({
  selector: 'app-employees-center',
  templateUrl: './employees-center.component.html',
  styleUrls: ['./employees-center.component.css']
})
export class EmployeesCenterComponent implements OnInit {

  EmployeeCenterUrl: any = '';
  constructor(private service: IframesService) {
    this.EmployeeCenterUrl = this.service.routeUrl('EmployeeCenterUrl', true);
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
