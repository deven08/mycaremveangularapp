import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesCenterComponent } from './employees-center.component';

describe('EmployeesCenterComponent', () => {
  let component: EmployeesCenterComponent;
  let fixture: ComponentFixture<EmployeesCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
