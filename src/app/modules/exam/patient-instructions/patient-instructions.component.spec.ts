import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientInstructionsComponent } from './patient-instructions.component';

describe('PatientInstructionsComponent', () => {
  let component: PatientInstructionsComponent;
  let fixture: ComponentFixture<PatientInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
