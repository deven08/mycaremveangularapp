import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteExamComponent } from './complete-exam.component';

describe('CompleteExamComponent', () => {
  let component: CompleteExamComponent;
  let fixture: ComponentFixture<CompleteExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
