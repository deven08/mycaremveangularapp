// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';




// Setting Up Component
@Component({
  selector: 'app-contact-lens-exam',
  templateUrl: './contact-lens-exam.component.html'
})
export class ContactLensExamComponent implements OnInit {

  ContactLensExamUrl: Object;

  constructor(private service: IframesService) {

     this.ContactLensExamUrl = this.service.routeUrl('ContactLensExamUrl', true);

  }

  ngOnInit() {

  }
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
