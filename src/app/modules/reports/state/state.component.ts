// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IframesService } from '@app/shared/services/iframes.service';

// App Configuration Module

// Setting Up Component
@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {

  StateUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.StateUrl = this.service.routeUrl('KYStateReportUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


@Component({
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateTNComponent implements OnInit {

  StateUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.StateUrl = this.service.routeUrl('TNStateReportUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
