// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-financials',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.SchedulerId = +params['sch_temp_id'] || 0;
        this.FinancialUrl = this.service.routeUrl('FinancialUrl', true, '?sch_temp_id=' + this.SchedulerId);
      });
    
  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// PreviousHCFA
// Setting Up Component
@Component({
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsPreviousHCFAComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.FinancialUrl = this.service.routeUrl('PreviousHCFAUrl', true);
  }


  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// EID Status
// Setting Up Component
@Component({
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsEIDStatusComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.FinancialUrl = this.service.routeUrl('EIDStatusUrl', true);
  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// EID Payment
// Setting Up Component
@Component({
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsEIDPaymentComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.FinancialUrl = this.service.routeUrl('EIDPaymentUrl', true);
  }


  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// New Statement Component
// Setting Up Component
@Component({
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsNewStatementComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.FinancialUrl = this.service.routeUrl('NewStatementUrl', true);
  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Previous Statement Component
// Setting Up Component
@Component({
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.css']
})

export class FinancialsPreviousStatementComponent implements OnInit {

  FinancialUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.FinancialUrl = this.service.routeUrl('PreviousStatementUrl', true);
  }


  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
