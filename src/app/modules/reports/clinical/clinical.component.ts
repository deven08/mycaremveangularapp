// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// // App Configuration Module
// import { AppSettings } from '../../shared/config/AppSettings';

@Component({
  selector: 'app-clinical',
  templateUrl: './clinical.component.html',
  styleUrls: ['./clinical.component.css']
})
export class ClinicalComponent implements OnInit {

  ClinicalReportUrl: any = '';

  constructor(private service: IframesService) {
    this.ClinicalReportUrl = this.service.routeUrl('ClinicalReport', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
