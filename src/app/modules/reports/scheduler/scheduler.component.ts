// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})


export class SchedulerComponent implements OnInit, OnDestroy {

  SchedulerUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.SchedulerId = +params['sch_temp_id'] || 0;
        this.SchedulerUrl = this.service.routeUrl('SchedulerUrl', true, '?sch_temp_id=' + this.SchedulerId);
      });

  }

  // Unsubcribe Service
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Patient MonitorUrl
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerPatientMonitorComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('SchedulerPatientMonitorUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}




// Patient Facesheet
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerFaceSheetComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('FaceSheetUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}





// Appointment Information
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerApptInfoComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('ApptInfoUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// Patient Document
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerPtDocsComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('SPtDocsUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// Patient SX Planning
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerSxPlanningComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('SxPlanningSheetUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// Patient SX Planning
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerRecallUrlComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('RecallUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Consult Letter
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerConsultLetterComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('ConsultLetterUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// Scheduler Report
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerReportComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('SchedulerReportUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}

// Patient CSV Export
@Component({
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerPatientCSVExportComponent implements OnInit {
  SchedulerUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.SchedulerUrl = this.service.routeUrl('PatientCSVReportUrl', true);
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
