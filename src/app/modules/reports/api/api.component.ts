// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  ApiUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.ApiUrl = this.service.routeUrl('ApiAccessLogUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}


// Call Log
@Component({
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiCallLogComponent implements OnInit {

  ApiUrl: any = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    this.ApiUrl = this.service.routeUrl('ApiCallLogUrl', true);
  }

  // Get Parameters on Intialize
  ngOnInit() {


  }

  // Loading
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
