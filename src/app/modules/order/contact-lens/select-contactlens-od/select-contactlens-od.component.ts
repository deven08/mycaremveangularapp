import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';

// import { ContactlensService } from '../../../ConnectorEngine/services/contactlens.service';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';
import { Subscription } from 'rxjs';
import { ContactlensService } from '@app/core/services/order/contactlens.service';
import { OrderService } from '@app/core/services/order/order.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService, ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { GettingContactLensDetails } from '../contactlens.model';
import { ManufacturerDropData, LocationsMaster } from '@app/core/models/masterDropDown.model';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';


@Component({
    selector: 'app-order-select-contactlens-od',
    templateUrl: 'select-contactlens-od.component.html'
})
export class OrderSelectContactlensodComponent implements OnInit {
    IsHardcontactLensdata: Subscription;
    requrestvalues: any;
    manufacturerlist: any;
    Manufacturers: any = [];
    orderTypes: any[];
    public checked: boolean;
    cols: any[];
    /**
     * Grid lists of order select contactlensod component
     * stores grid data
     */
    gridLists: object;
    public locations: any = [];
    selectform: FormGroup;
    completefiltervalues: any[];
    filtervalues: any[];
    hardcontactlens: any;
    Lenstype = '';
    @Output() selectoutput = new EventEmitter<any>();
    /**
     * Pagination value of order select contactlensod component
     * stores current page value
     */
    paginationValue: number;
    /**
     * Total records of order select contactlensod component
     * stores total number of records
     */
    totalRecords: string;
    /**
     * Pagination page of order select contactlensod component
     *  declare the default pagination number
     */
    paginationPage: number;
    /**
     * View child of order select contactlensod component
     * reset the table grid
     */
    @ViewChild('dt') public dt: any;
    /**
     * Creates an instance of order select contactlensod component.
     * @param router for navigation
     * @param contactlensService for contactlens methods
     * @param _fb  for formbuilder
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     * @param app global service for instanceses 
     */
    constructor(private _fb: FormBuilder, private contactlensService: ContactlensService, private orderService: OrderService,
        private app: AppService, private dropdownService: DropdownService, 
        private utility: UtilityService,
        private error: ErrorService,
    ) {

        this.paginationPage = 0;
        this.paginationValue = 0;
        this.IsHardcontactLensdata = this.contactlensService.IsHardContactLens$.subscribe(data => {


            this.hardcontactlens = parseInt(data.IshardContactlens, 10);
            this.Lenstype = data.LensType;
            this.gridLists = [];
            this.getgridvalues(this.hardcontactlens);
            // this.GetcontactlensListwithfilter("", "", "", "", "", "", "",  this.hardcontactlens);
        });
        this.IsHardcontactLensdata = this.contactlensService.IsSoftContactLens$.subscribe(data => {
            this.hardcontactlens = parseInt(data.IshardContactlens, 10);
            this.Lenstype = data.LensType;
            this.gridLists = [];
            this.getgridvalues(this.hardcontactlens);
            // this.GetcontactlensListwithfilter("", "", "", "", "", "", "",  this.hardcontactlens);
        });

    }


    ngOnInit() {
        this.LoadForm();
        this.LoadLocations();
        this.Loadmanufacturer();
        // this.locations = [];
        // this.locations = this.dropdownService.getLocations();
        // this.Manufacturers = [];
        // this.Manufacturers = this.dropdownService.getManufactures();
        // this.GetcontactlensListwithfilter("", "", "", "", "", "", "", "",'0');
    }

    ngDestroy() {
        this.IsHardcontactLensdata.unsubscribe();

    }

    LoadForm() {
        this.selectform = this._fb.group({
            // accessToken: this._StateInstanceService.accessTokenGlobal,
            patient_id: this.app.patientId,
            manufacturerid: [''],
            location: [''],
            sphere: '',
            cylinder: '',
            axis: '',
            sadd: '',
            bc: '',
            diameter: '',
            modifierMultifocal: ''
        });
    }
    /**
     * Loads locations
     *  @param {Array} locations for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
    /**
     * Loadmanufacturers order select contactlensod component
     *  @param {Array} Manufacturers for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    Loadmanufacturer() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    // GetcontactlensList(ID = '', upc = '', manufacturerid = '', brandid = '', colorid = '', hardcontactlen) {
    //     this.contactlensService.getcontactlens(ID, upc, manufacturerid, brandid, colorid).subscribe(
    //         data => {
    //             this.gridLists = [];
    //             // Check whether the result is empty
    //             if (data['result']['Optical'] !== 'No record found') {
    //                 for (let i = 0; i <= data['result']['Optical']['ContactLenses'].length - 1; i++) {
    //                     data['result']['Optical']['ContactLenses'][i]['ItemId'] =
    //                         parseInt(data['result']['Optical']['ContactLenses'][i]['ItemId'], 10);
    //                 }
    //                 const values = data['result']['Optical']['ContactLenses'];
    //                 this.gridLists = values.filter(x => x.HardContact === hardcontactlen);
    //             }
    //         });
    // }


    FilterValues() {
        this.GetcontactlensListwithfilter(this.selectform.controls['manufacturerid'].value,
            this.selectform.controls['sphere'].value,
            this.selectform.controls['cylinder'].value,
            this.selectform.controls['axis'].value,
            this.selectform.controls['bc'].value,
            this.selectform.controls['sadd'].value,
            this.selectform.controls['diameter'].value,
            this.selectform.controls['modifierMultifocal'].value,
            this.selectform.controls.location.value);
    }

    DoubleClick(values) {
        if (this.hardcontactlens === 0) {

            const data = {
                'IshardContactlens': '0',
                'Softcontactlensod': values
            };
            if (this.Lenstype === 'OD') {
                this.contactlensService.SoftContactODvalue.next(data);
            }
            if (this.Lenstype === 'OS') {
                this.contactlensService.SoftContactOSvalue.next(data);
            }
        }
        if (this.hardcontactlens === 1) {
            const data = {
                'IshardContactlens': '1',
                'Hardcontactlensod': values
            };
            if (this.Lenstype === 'OD') {
                this.contactlensService.HardContactODvalue.next(data);
            }
            if (this.Lenstype === 'OS') {
                this.contactlensService.HardContactOSvalue.next(data);
            }
        }

    }

    getgridvalues(contactlens) {
        const filter = {

            'filter': [
                {
                    'field': 'del_status',
                    'operator': '=',
                    'value': '0'
                },
                {
                    'field': 'module_type_id',
                    'operator': '=',
                    'value': '3'
                }
            ],
            'sort': [
                {
                    'field': 'name',
                    'order': 'ASC'
                }
            ]

        };
        this.contactlensService.dynamicfilterforcontactlens(filter).subscribe(
            data => {
                this.gridLists = [];
                // Check whether the result is empty
                if (data['data'] !== 'Data not found.') {

                    const values = data['data'];
                    const contactlensType = this.contactlensService.savecontactlensupdate === 'Hard Contact Lens' ? 1 : null;                    
                    this.gridLists = values.filter(x => x.hard_contact === contactlensType);
                    // if (this.gridlists != null && this.gridlists !== undefined && this.gridlists.length !== undefined) {
                    //     for (let i = 0; i <= this.gridlists.length - 1; i++) {
                    //         if (this.gridlists[i].manufacturer_id != null && this.gridlists[i].manufacturer_id !== undefined &&
                    //             this.gridlists[i].manufacturer_id !== '') {
                    //             const value = this.gridlists[i].manufacturer_id.toString();
                    //             const manufname = this.Manufacturers.filter(x => x.Id === value);
                    //             if (manufname.length !== undefined && manufname.length !== 0) {
                    //                 this.gridlists[i].manufacturename = manufname[0]['Name'];
                    //             }
                    //         }
                    //     }

                    // }
                    this.gridLists = this.gridLists;
                }
            },
            error => {
                if (error.error['status'] === 'Data not found.') {
                    this.gridLists = [];
                }
            }
        );
    }


    GetcontactlensListwithfilter(manufacture: string = '', sphere: string = '', cyclinde: string = '', axis: string = '',
                                 add: string = '', basecure: string = '', diameter: string = '', multifocal: string = '',
                                 searchLocation: string = '') {

        // Dynamic filter
        this.completefiltervalues = [];
        this.requrestvalues = {

            'filter': [
                {
                    'field': 'del_status',
                    'operator': '=',
                    'value': '0'
                },
                {
                    'field': 'module_type_id',
                    'operator': '=',
                    'value': '3'
                }
            ],
            'sort': [
                {
                    'field': 'name',
                    'order': 'ASC'
                }
            ]

        };

        if (searchLocation !== '' && searchLocation !== undefined) {
            const loc = {
                field: 'loc_id',
                operator: '=',
                value: searchLocation
            };
            this.requrestvalues.filter.push(loc);
        }

        // this.filtervalues.push(values);
        if (manufacture !== '' && manufacture !== undefined) {
            const manuf = {
                'field': 'manufacturer_id',
                'operator': '=',
                'value': parseInt(manufacture, 10)
            };
            this.requrestvalues.filter.push(manuf);
        }

        if (sphere !== '' && sphere !== undefined) {
            const spherevalue = {
                'field': 'sphere',
                'operator': 'LIKE',
                'value': '%' + sphere + '%'
            };
            this.requrestvalues.filter.push(spherevalue);
        }
        if (cyclinde !== '' && cyclinde !== undefined) {
            const cyclindevalue = {
                'field': 'cylinder',
                'operator': 'LIKE',
                'value': '%' + cyclinde + '%'
            };
            this.requrestvalues.filter.push(cyclindevalue);
        }
        if (axis !== '' && axis !== undefined) {
            const axisvalue = {
                'field': 'axis',
                'operator': 'LIKE',
                'value': '%' + axis + '%'
            };
            this.requrestvalues.filter.push(axisvalue);
        }
        if (add !== '' && add !== undefined) {
            const addvalue = {
                'field': 'sadd',
                'operator': 'LIKE',
                'value': '%' + add + '%'
            };
            this.requrestvalues.filter.push(addvalue);
        }
        if (basecure !== '' && basecure !== undefined) {
            const bcvalue = {
                'field': 'bc',
                'operator': 'LIKE',
                'value': '%' + basecure + '%'
            };
            this.requrestvalues.filter.push(bcvalue);
        }
        if (diameter !== '' && diameter !== undefined) {
            const diametervalue = {
                'field': 'diameter',
                'operator': 'LIKE',
                'value': '%' + diameter + '%'
            };
            this.requrestvalues.filter.push(diametervalue);
        }
        if (multifocal !== '' && multifocal !== undefined) {
            const multifocalvalue = {
                'field': 'modifierMultifocal',
                'operator': 'LIKE',
                'value': '%' + multifocal + '%'
            };
            this.requrestvalues.filter.push(multifocalvalue);
        }
        this.contactlensService.dynamicfilterforcontactlens(this.requrestvalues).subscribe(
            data => {
                this.gridLists = [];
                // Check whether the result is empty
                if (data['data'] !== 'Data not found.') {
                    this.totalRecords = data['total'];
                    const values = data['data'];
                    this.gridLists = values;
                }
            },
            error => {
                if (error.error['status'] === 'Data not found.') {
                    this.gridLists = [];
                }
            }
        );
    }
    onSelectClose() {
        this.dt.reset();
        this.selectoutput.emit('close');
    }

    /**
     * Gets frame list by paginator
     * @param event diplay current page number
     */
    getContactLensListByPaginator(event) {
        this.paginationValue = event.first;
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.loadContactLensData(page);
    }
    /**
     * Loads frame data
     * @param [page] loading pagewise data
     */
    loadContactLensData(page: number = 0) {
        this.GetcontactlensListwithfilter();
        this.orderService.getDataByPagination( this.requrestvalues, page).subscribe(
            (data: GettingContactLensDetails) => {
                const frameData = data.data;
                this.totalRecords = data.total;
                this.gridLists = [];
                this.gridLists = frameData;
            },
        );
    }

    /**
     * Refreshs order select contactlensod component for clear the filter search
     */
    refresh() {
        this.GetcontactlensListwithfilter();
        this.selectform.reset();
        this.selectform.controls.manufacturerid.setValue('');
        this.selectform.controls.sphere.setValue('');
        this.selectform.controls.cylinder.setValue('');
        this.selectform.controls.axis.setValue('');
        this.selectform.controls.bc.setValue('');
        this.selectform.controls.sadd.setValue('');
        this.selectform.controls.diameter.setValue('');
        this.selectform.controls.modifierMultifocal.setValue('');
        this.selectform.controls.location.setValue('');
    }
}
