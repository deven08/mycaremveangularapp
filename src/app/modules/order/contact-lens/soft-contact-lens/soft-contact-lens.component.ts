import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderService } from '@app/core/services/order/order.service';
import { ContactlensService } from '@app/core/services/order/contactlens.service';
import { AppService, ErrorService } from '@app/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { SoftContact } from '@app/core/models/order/order.model';
import { ManufacturerDropData } from '@app/core/models/masterDropDown.model';

// import { StateInstanceService } from 'src/app/shared/state-instance.service';

@Component({
    selector: 'app-order-soft-contact-lens',
    templateUrl: 'soft-contact-lens.component.html'
})
export class OrderSoftContactlensComponent implements OnInit, OnDestroy {
    subscriptionSavevaluesdetails: Subscription;
    subscriptioncontactlensOrderIdvalues: Subscription;
    softcontatlensOS: Subscription;
    softcontatlensOD: Subscription;
    subscriptionSavevalues: Subscription;
    public selectContactlensSidebar: boolean;
    public checked: boolean;
    RxSphereValues: any = [];
    orderLensSoftLensForm: FormGroup;
    RxCylinderValues: any = [];
    AxisValues: any = [];
    RxAddValues: any = [];
    // Default Droppers
    sphereDropperDefaultod: any = '';
    sphereDropperDefaultos: any = '';
    cylinderDropperDefaultod: any = '';
    cylinderDropperDefaultos: any = '';
    addDropperDefaultos: any = '';
    addDropperDefaultod: any = '';
    contactlensvalue: any;
    softcontactlensodsavevalue: any;
    softcontactlensossavevalue: any;
    softcontactpostosvalues: any;
    softcontactpostodvalues: any;
    orderdata: any[];
    orderid: any;
    orderitemid: any;
    ordersavedvalues: any;
    ordervalueupdate = false;
    softcontactlens = false;
    disablecheckbox = false;
    issoftcontactlensvalid = false;
    public manufacturerlist: Manufacturer[];
    public Manufacturers: any = [];
    /**
     * Subscription save new of order soft contactlens component
     */
    subscriptionSaveNew: Subscription;
    /**
     * Subscription location id of order soft contactlens component for location dropdown status
     */
    subscriptionLocationId: Subscription;
    /**
     * Subscription physician id of order soft contactlens component for location values
     */
    subscriptionPhysicianId: Subscription;
    /**
     * Location id of order soft contactlens component for physician dropdown status
     */
    LocationId = '';
    /**
     * Retail price od of order soft contactlens component
     */
    retail_PriceOD: string;

    /**
     * Retail price os of order soft contactlens component
     */
    retail_PriceOS: string;
    /**
     * Creates an instance of order soft contactlens component.
     * @param router for navigation
     * @param contactlensService for contactlens methods
     * @param _fb  for formbuilder
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     * @param app global service for instanceses 
     */
    constructor(private _fb: FormBuilder,
        private router: Router, private orderService: OrderService, private orderSearchService: OrderService,
        private contactlensService: ContactlensService, public app: AppService, private errorService: ErrorService,
        private dropdownService: DropdownService, private utility: UtilityService) {
        this.orderService.disablePaymentButton = true;

        this.subscriptionSavevaluesdetails = orderSearchService.orderFrameSave$.subscribe(

            datavalue => {
                if (this.orderLensSoftLensForm.controls['physician_id'].value !== '' && this.orderLensSoftLensForm.controls['physician_id'].value !== 0) {
                    // tslint:disable-next-line:max-line-length
                    if (this.softcontactlens === false && this.ordervalueupdate === true && datavalue === 'Contact Lens' && this.orderid != null && this.orderid !== undefined && this.contactlensService.savecontactlensupdate === 'Soft Contact Lens') {
                        // tslint:disable-next-line:max-line-length
                        this.UpdatecompleteOrdervalues(this.orderid, this.orderLensSoftLensForm.controls['ODitemid'].value, this.orderLensSoftLensForm.controls['OSitemid'].value, '');
                        // tslint:disable-next-line:max-line-length
                    } else if (this.softcontactlens === false && datavalue === 'Contact Lens' && this.contactlensService.savecontactlensupdate === 'Soft Contact Lens') {
                        this.savesoftcontactlenscompleteorder(true);
                    }
                } else {
                    this.errorService.displayError({
                        severity: 'info',
                        summary: 'Required',
                        detail: 'Please select physician'
                    });

                }
            });
        this.subscriptionSaveNew = orderService.orderSaveNew$.subscribe((data: string) => {
            if (this.contactlensService.savecontactlensupdate === 'Soft Contact Lens') {
                this.softcontactlens = false;
                this.orderid = null;
                this.softcontactlensodsavevalue.id = this.softcontactlensodsavevalue.item_id;
                this.softcontactlensossavevalue.id = this.softcontactlensossavevalue.item_id;
                this.contactlensService.savecontactlensupdate = 'Soft Contact Lens';
            }
        });
        this.softcontatlensOD = this.contactlensService.SoftContactODvalue$.subscribe((data: SoftContact) => {


            const value = data;
            this.orderService.softContactlensOd = value.Softcontactlensod.id;
            this.retail_PriceOD = value.Softcontactlensod.retail_price;
            this.selectContactlensSidebar = false;
            if (value.Softcontactlensod['manufacturer']) {
                this.orderLensSoftLensForm.controls['manufactureod'].patchValue(value.Softcontactlensod['manufacturer'].manufacturer_name);
            }
            this.orderLensSoftLensForm.controls['nameod'].patchValue(value.Softcontactlensod.name);
            this.orderLensSoftLensForm.controls['colorod'].patchValue(value.Softcontactlensod.color_code);
            this.orderLensSoftLensForm.controls['upcod'].patchValue(value.Softcontactlensod.upc_code);
            this.orderLensSoftLensForm.controls['sourceod'].patchValue(value.Softcontactlensod.sourceod);
            this.orderLensSoftLensForm.controls['qtyod'].patchValue('1');
            this.softcontactlensodsavevalue = value.Softcontactlensod;
            const values = {
                'LensType': 'OD',
                'data': data
            };
            this.softcontactlensodsavevalue.item_id = value.Softcontactlensod.id;
            this.softcontactlensodsavevalue.item_name = value.Softcontactlensod.name;
            this.issoftcontactlensvalid = true;
            this.contactlensService.contactlensotherdetails = [];
            this.contactlensService.SoftContactOtherODvalue.next(values);
        });

        this.softcontatlensOS = this.contactlensService.SoftContactOSvalue$.subscribe((data: SoftContact) => {

            const value = data;
            this.orderService.softContactlensOS = value.Softcontactlensod.id;
            this.retail_PriceOS = data.Softcontactlensod.retail_price;
            this.selectContactlensSidebar = false;
            if (value.Softcontactlensod['manufacturer']) {
                this.orderLensSoftLensForm.controls['manufactureos'].patchValue(value.Softcontactlensod['manufacturer'].manufacturer_name);
            }
            this.orderLensSoftLensForm.controls['nameos'].patchValue(value.Softcontactlensod.name);
            this.orderLensSoftLensForm.controls['coloros'].patchValue(value.Softcontactlensod.color_code);
            this.orderLensSoftLensForm.controls['upcos'].patchValue(value.Softcontactlensod.upc_code);
            this.orderLensSoftLensForm.controls['sourceos'].patchValue(value.Softcontactlensod.sourceod);
            this.orderLensSoftLensForm.controls['qtyos'].patchValue('1');
            this.softcontactlensossavevalue = value.Softcontactlensod;
            const values = {
                'LensType': 'OS',
                'data': data
            };
            this.softcontactlensossavevalue.item_id = value.Softcontactlensod.id;
            this.softcontactlensossavevalue.item_name = value.Softcontactlensod.name;
            this.issoftcontactlensvalid = true;
            this.contactlensService.contactlensotherdetails = [];
            this.contactlensService.SoftContactOtherOSvalue.next(values);
        });


        this.subscriptioncontactlensOrderIdvalues = this.orderService.orderSoftContactLens$.subscribe((value: string) => {
            if (value !== 'New Order') {
                this.orderLensSoftLensForm.reset();
                this.contactlensService.contactlensotherdetails = [];
                this.orderid = value;
                this.getordertype(value);
                // this.getcontactlensordervaluewithorderid(value);
                this.orderid = value;
                this.orderLensSoftLensForm.controls['module_type_id'].patchValue('3');
                this.orderLensSoftLensForm.controls['patient_id'].patchValue(this.app.patientId);
                this.orderLensSoftLensForm.controls['order_id'].patchValue(value);
                this.orderLensSoftLensForm.controls['location_id'].patchValue(this.app.locationId);
                this.orderLensSoftLensForm.controls['physician_id'].patchValue('');
                this.orderService.orderContactLensById.next(value);
            } else {
                this.disablecheckbox = false;
                this.softcontactlens = false;
                this.orderService.orderContactLensById.next(value);
                // this.contactlensService.Disablehardcontactcomponent.next(false);
                // this.contactlensService.HardContactbyorderID.next(value);
                this.orderid = value;
                this.ordervalueupdate = false;
                this.issoftcontactlensvalid = false;
                this.orderLensSoftLensForm.reset();
                // localStorage.removeItem('orderId');
                this.contactlensService.contactlensotherdetails = [];
                this.softcontactlensodsavevalue = [];
                this.softcontactlensossavevalue = [];
                this.orderLensSoftLensForm.controls['module_type_id'].patchValue('3');
                this.orderLensSoftLensForm.controls['patient_id'].patchValue(this.app.patientId);
                this.orderLensSoftLensForm.controls['sphere_od'].patchValue('PL');
                this.orderLensSoftLensForm.controls['sphere_os'].patchValue('PL');
                this.orderLensSoftLensForm.controls['location_id'].patchValue(this.app.locationId);
                this.orderLensSoftLensForm.controls['physician_id'].patchValue('');
                this.orderService.softContactlensOS = '';
                this.orderService.softContactlensOd = '';

            }
        });

        this.subscriptionLocationId = orderService.orderLocationId$.subscribe(data => {
            if (data !== '') {
                this.LocationId = data;
                this.orderLensSoftLensForm.controls['location_id'].patchValue(data);
                // this.orderService.orderFrameSave.next(false)
            } else {
                this.LocationId = data;
                this.orderLensSoftLensForm.controls['location_id'].patchValue(data);
                // this.orderService.orderFrameSave.next(false)
            }
        });

        this.subscriptionPhysicianId = orderService.lensPhysicianId$.subscribe(data => {
            this.orderLensSoftLensForm.patchValue({
                physician_id: data
            });
        });
    }

    checkunchekevent(value) {
        this.softcontactlens = value;
        if (value === true) {
            this.contactlensService.savecontactlensupdate = 'Hard Contact Lens';
            this.contactlensService.Disablehardcontactcomponent.next(true);
            this.orderService.orderSelectTypeobj.next(4);
        }
        if (value === false) {
            this.contactlensService.savecontactlensupdate = 'Soft Contact Lens';
            this.contactlensService.Disablehardcontactcomponent.next(false);
            this.orderService.orderSelectTypeobj.next(5);
        }

    }



    equalvaluesofOS(value: string) {
        this.retail_PriceOD = this.retail_PriceOD;
        if (this.softcontactlensodsavevalue.item_id !== undefined && this.softcontactlensodsavevalue.item_id != null) {
            const data = {
                'IshardContactlens': '0',
                'Softcontactlensod': this.softcontactlensodsavevalue
            };
            this.equalvalues();
            this.contactlensService.SoftContactOSvalue.next(data);
        }
    }

    validationcheck(data): boolean {
        let value = true;
        // this.contactlensService.errorHandle.msgs = [];
        if (data === 'Contact Lens') {
            // localStorage.setItem('PatientId',"1");
            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please Select a Patient'
                });
                // this.WarningShow('Please Select a Patient');

                value = false;
                return false;
            }
            if (this.softcontactlensossavevalue.length === 0 && this.softcontactlensodsavevalue.length === 0) {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please Enter the values'
                });
                // this.WarningShow('Please Enter the values');
                value = false;
                return false;
            }
            // tslint:disable-next-line:max-line-length
            if (this.orderLensSoftLensForm.controls['sphere_od'].value === '' || this.orderLensSoftLensForm.controls['sphere_od'].value == null && this.orderLensSoftLensForm.controls['sphere_od'].value === '' || this.orderLensSoftLensForm.controls['sphere_od'].value == null) {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Sphere OD and OS mandatory'
                });
                // this.WarningShow('Sphere OD and OS mandatory');
                value = false;
                return false;
            }


        }
    }

    ngOnDestroy(): void {

        //   this.subscriptionSavevalues.unsubscribe();
        //   this.subscriptioncontactlensOrderIdvalues.unsubscribe();
        this.subscriptionSavevaluesdetails.unsubscribe();
        this.softcontatlensOD.unsubscribe();
        this.softcontatlensOS.unsubscribe();
        this.subscriptioncontactlensOrderIdvalues.unsubscribe();
        this.subscriptionLocationId.unsubscribe();
        this.subscriptionPhysicianId.unsubscribe();
        this.subscriptionSaveNew.unsubscribe();
    }




    formGroupData() {
        this.orderLensSoftLensForm = this._fb.group({
            // accessToken: localStorage.getItem('tokenKey'),
            patient_id: this.app.patientId,
            id: '0',
            base_od: ['', [Validators.required]],
            base_os: ['', [Validators.required]],
            diameter_od: '',
            diameter_os: '',
            sphere_od: ['PL', [Validators.required]],
            sphere_os: ['PL', [Validators.required]],
            axis_od: '',
            axis_os: '',
            add_od: '',
            add_os: '',
            cylinder_od: '',
            cylinder_os: '',
            multifocalid_os: '',
            multifocalid_od: '',
            manufactureod: '',
            nameod: '',
            colorod: '',
            upcod: '',
            sourceod: '',
            qtyod: '',
            manufactureos: '',
            nameos: '',
            coloros: '',
            upcos: '',
            sourceos: '',
            qtyos: '',
            module_type_id: '3',
            ODitemid: '',
            OSitemid: '',
            patientrxid: '',
            warranty_od: '',
            warranty_os: '',
            order_id: '',
            custom_rx: '1',
            det_order_id: '1',
            location_id: this.app.locationId,
            outside_rx: '',
            hard_contact: 0,
            physician_id: this.orderService.physicianID,

        }
        );
    }



    ngOnInit(): void {
        this.orderService.orderStatusGlobal = '9';
        this.formGroupData();
        this.loadformvalues();
        this.LoadMasters();
        this.softcontactlensossavevalue = [];
        this.softcontactlensodsavevalue = [];
        this.contactlensService.contactlensotherdetails = [];
        this.orderSearchService.orderScreenMultipleSavePath = 'Contact Lens';
        this.contactlensService.savecontactlensupdate = 'Soft Contact Lens';
        this.disablecheckbox = false;
        this.softcontactlens = false;
        this.ordervalueupdate = false;
        const patientid = this.app.patientId;
        // this.contactlensService.errorHandle.msgs = [];
        if (patientid == null || patientid === undefined) {
            this.errorService.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select a Patient'
            });
            // this.WarningShow('Please Select a Patient');

            return;
        }
        // this.Manufacturers = [];
        // this.Manufacturers = this.dropdownService.getManufactures();
        // this.manufacturerlist = this.Manufacturers;
        // localStorage.removeItem('orderId');
        this.Loadmanufactures();

        if (this.app.ordersearchvalueService !== undefined) {
            if (this.app.ordersearchvalueService.length !== 0) {
                this.orderService.OrderSearchFilterValue.next((
                    {
                        Id: this.app.ordersearchvalueService[0].Id,
                        OrderType: this.app.ordersearchvalueService[0].OrderType
                    })
                );
                this.app.ordersearchvalueService = [];
            }
        }
    }
    /**
     * Loadmanufactures order soft contactlens component
     * @param {Array} Manufacturers for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    Loadmanufactures() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }
    saveupdatevalues(patientid) {
        const itemid = this.orderLensSoftLensForm.controls['id'].value;
        if (itemid === '0') {
            this.savecontactlens(patientid);
        } else {
            this.updatecontactlens(patientid, itemid);
        }
    }

    savecontactlens(patientid: any) {

        if (patientid != null || patientid !== undefined) {
            this.orderSearchService.SaveContactLens(patientid, this.orderLensSoftLensForm.value).subscribe(data => {
                this.loadformvalues();
                //   this.orderService.OrderAlertPopup.next("required");
                this.orderLensSoftLensForm.reset();

            });
        }
    }

    updatecontactlens(patientid: any, itemid) {
        // this.contactlensService.errorHandle.msgs = [];
        if (patientid != null || patientid !== undefined) {
            this.orderSearchService.UpdateContactLens(patientid, this.orderLensSoftLensForm.value, itemid).subscribe(data => {
                // this.SuccessShow('Updated Successfully.');
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Updated Successfully.'
                });

                this.loadformvalues();
            });
        }
    }

    loadformvalues() {
        this.formGroupData();
        const patientid = this.app.patientId;
        if (patientid != null) {
            // this.loadgroupdetails(patientid);
        }
    }

    equalvalues() {

        this.orderLensSoftLensForm.controls['diameter_os'].patchValue(this.orderLensSoftLensForm.controls['diameter_od'].value);
        this.orderLensSoftLensForm.controls['base_os'].patchValue(this.orderLensSoftLensForm.controls['base_od'].value);
        this.orderLensSoftLensForm.controls['sphere_os'].patchValue(this.orderLensSoftLensForm.controls['sphere_od'].value);
        this.orderLensSoftLensForm.controls['cylinder_os'].patchValue(this.orderLensSoftLensForm.controls['cylinder_od'].value);
        this.orderLensSoftLensForm.controls['axis_os'].patchValue(this.orderLensSoftLensForm.controls['axis_od'].value);
        this.orderLensSoftLensForm.controls['add_os'].patchValue(this.orderLensSoftLensForm.controls['add_od'].value);
        this.orderLensSoftLensForm.controls['multifocalid_os'].patchValue(this.orderLensSoftLensForm.controls['multifocalid_od'].value);
    }


    savesoftcontactlenscompleteorder(Issave) {
        // Issave true indicated a new order id true
        const messagedetails = {
            'messagetype': 'required',
            'message': 'Please Enter the required fields'
        };

        const patientid = this.app.patientId;
        if (patientid == null || patientid === undefined) {
            messagedetails.message = 'Please Select a Patient';
            this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
            return;
        }

        if (this.issoftcontactlensvalid === false) {
            messagedetails.message = 'Please Select Contact Lens OD/OS';
            this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
            return;
        }

        if (!this.orderLensSoftLensForm.valid) {
            messagedetails.message = 'Please Enter the required fields';
            this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
            return;
        }
        if (this.contactlensService.savecontactlensupdate === 'Soft Contact Lens') {
            this.CreateNewcontactlensorder();
        }
        // rx save update
        // this.saveupdatevalues(this.app.patientId) ;
    }


    
    /**
     * Here we can create the new soft contactlens order.
     */
    CreateNewcontactlensorder() {
        if (this.orderService.orderStatusGlobal !== undefined) {
            let odqty;
            let praccode = '';
            if (this.softcontactlensodsavevalue != null && this.softcontactlensodsavevalue !== undefined) {
                const value = this.contactlensService.contactlensotherdetails.filter(x => x.itemId ===
                    this.softcontactlensodsavevalue.item_id);
                if (value.length === 0) {
                    odqty = 1;
                    praccode = '';
                } else {
                    odqty = value[0].qty;
                    praccode = value[0].item_prac_code;
                }
            }
            const softcontactlensodvaluessave = {
                'patient_id': this.app.patientId,
                'item_prac_code': praccode,
                'loc_id': this.app.locationId,
                'item_id': this.softcontactlensodsavevalue.id,
                'upc_code': this.softcontactlensodsavevalue.upc_code,
                'item_name': this.softcontactlensodsavevalue.item_name,
                'module_type_id': this.orderLensSoftLensForm.controls['module_type_id'].value,
                'lens_vision': 'OD',
                'qty': odqty, // this.orderLensSoftLensForm.controls['qtyod'].value,
                'ordertypeid': '5',
                'manufacturer_id': this.softcontactlensodsavevalue.manufacturer_id,
                'order_status_id': this.orderService.orderStatusGlobal,
                'price_retail': this.retail_PriceOD

            };


            this.softcontactpostodvalues = softcontactlensodvaluessave;

            if (this.softcontactpostodvalues.upc_code !== '' && this.softcontactpostodvalues.upc_code !== undefined) {
                this.orderService.saveOrderFrames(this.softcontactpostodvalues).subscribe(data => {
                    // if (data['status'] === 'New Order Crated') {

                    this.errorService.displayError({
                        severity: 'success',
                        summary: 'Contact Lens Order',
                        detail: 'Order Saved Successfully'
                    });
                    this.orderService.disablePaymentButton = false;
                    // this.orderService.orderId = data['order_id'];
                    this.orderService.orderId = data['order_id'];
                    this.orderService.orderspectacleLensSave.next('5');
                    this.orderid = this.orderService.orderId;
                    // localStorage.getItem('orderId');
                    this.createneworderOS(this.orderService.orderId);
                    this.savepatientrxdetails(this.orderid);

                    // }
                });
            }

            if (this.softcontactpostodvalues.upc_code === '' || this.softcontactpostodvalues.upc_code === undefined) {
                this.createneworderOS(this.orderService.orderId);
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'Required',
                detail: 'Please provide the Order Status'
            });
        }


    }

    updatecontactlensodvaluesbyorderid(orderidvalue, oditemid, ositemvalues) {
        if (this.orderService.orderStatusGlobal !== undefined) {
            let odqty;
            let praccode = '';
            if (this.softcontactlensodsavevalue != null && this.softcontactlensodsavevalue.length !== 0) {
                const value = this.contactlensService.contactlensotherdetails.filter(x => x.itemId ===
                    this.softcontactlensodsavevalue.item_id);
                if (value.length === 0) {
                    odqty = 1;
                    praccode = '';
                } else {
                    odqty = value[0].qty;
                    praccode = value[0].item_prac_code;
                }
            }

            if (this.softcontactlensodsavevalue.upc_code !== '' && this.softcontactlensodsavevalue.upc_code !== undefined) {
                const updatecontactlensodvalues = {
                    'patient_id': this.app.patientId,
                    'item_prac_code': praccode,
                    'loc_id': this.app.locationId,
                    'item_id': this.softcontactlensodsavevalue.item_id,
                    'upc_code': this.softcontactlensodsavevalue.upc_code,
                    'item_name': this.softcontactlensodsavevalue.item_name,
                    'module_type_id': this.orderLensSoftLensForm.controls['module_type_id'].value,
                    'lens_vision': 'OD',
                    'qty': odqty,
                    'ordertypeid': '5',
                    'manufacturer_id': this.softcontactlensodsavevalue.manufacturer_id,
                    'order_status_id': this.orderService.orderStatusGlobal,
                    'price_retail': this.retail_PriceOD

                };
                this.softcontactpostodvalues = updatecontactlensodvalues;

                if (oditemid !== '' && oditemid !== null) {
                    this.orderService.updateOrderFrames(this.softcontactpostodvalues, orderidvalue, oditemid).subscribe(data => {
                        /*this.Savecontactlensosvalueswithorderid(orderidvalue,false,ositemvalues); */
                        if (data['status'] === 'New Order Crated') {
                            this.orderService.orderId = data['order_id'];
                        }
                    });
                } else if ((oditemid === '' || oditemid == null) && orderidvalue !== '') {
                    this.orderService.saveorderdetailswithorderId(this.softcontactpostodvalues, orderidvalue).subscribe(data => {
                        if (data['status'] === 'New Item added to the order.') {
                            // this.Savecontactlensosvalueswithorderid(orderidvalue,false,ositemvalues);
                            this.orderService.orderId = data['order_id'];
                            this.orderid = this.orderService.orderId;

                        }
                    });
                }
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select order status'
            });
        }
    }


    UpdatecompleteOrdervalues(orderid, oditemvalue, ositemvalues, patientrxid) {
        if (this.contactlensService.savecontactlensupdate === 'Soft Contact Lens') {

            const messagedetails = {
                'messagetype': 'required',
                'message': 'Please Enter the required fields'
            };
            if (!this.orderLensSoftLensForm.valid) {
                messagedetails.message = 'Please Enter the required fields';

                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }

            const patientid = this.app.patientId;
            if (patientid == null || patientid === undefined) {
                messagedetails.message = 'Please Select a Patient';
                this.orderService.ContactLensPopupAlertPopup.next(messagedetails);
                return;
            }
            this.updatecontactlensodvaluesbyorderid(orderid, oditemvalue, ositemvalues);
            this.Savecontactlensosvalueswithorderid(orderid, false, ositemvalues);
            this.updatepatientrxdetails(this.app.patientId, this.orderLensSoftLensForm.controls['patientrxid'].value);
        }
    }


    createneworderOS(orderid) {
        if (this.orderService.orderStatusGlobal != undefined) {
            let osqty;
            let praccode = '';
            if (this.softcontactlensossavevalue !== undefined && this.softcontactlensossavevalue !== '' &&
                this.softcontactlensossavevalue !== undefined) {
                const value = this.contactlensService.contactlensotherdetails.filter(x => x.itemId ===
                    this.softcontactlensossavevalue.item_id);

                if (value.length === 0) {
                    osqty = 1;
                    praccode = '';
                } else {
                    osqty = value[0].qty;
                    praccode = value[0].item_prac_code;
                }


                const softcontactlensosvaluedetails = {
                    'patient_id': this.app.patientId,
                    'item_prac_code': praccode,
                    'loc_id': this.app.locationId,
                    'item_id': this.softcontactlensossavevalue.id,
                    'upc_code': this.softcontactlensossavevalue.upc_code,
                    'item_name': this.softcontactlensossavevalue.item_name,
                    'module_type_id': this.orderLensSoftLensForm.controls['module_type_id'].value,
                    'lens_vision': 'OS',
                    'qty': osqty,
                    'ordertypeid': '5',
                    'manufacturer_id': this.softcontactlensossavevalue.manufacturer_id,
                    'order_status_id': this.orderService.orderStatusGlobal,
                    'price_retail': this.retail_PriceOS

                };

                this.softcontactpostosvalues = softcontactlensosvaluedetails;

                if (orderid === '' || orderid == null) {
                    this.orderService.saveOrderFrames(this.softcontactpostosvalues).subscribe(data => {

                        if (data['status'] === 'New Order Crated') {
                            this.orderService.orderId = data['order_id'];
                            this.orderid = this.orderService.orderId;
                            this.savepatientrxdetails(this.orderid);
                        }
                    });
                } else {
                    this.orderService.saveorderdetailswithorderId(this.softcontactpostosvalues, orderid).subscribe(data => {
                        if (data['status'] === 'New Item added to the order.') {
                            this.orderService.orderId = data['order_id'];

                            this.orderid = this.orderService.orderId;
                            /*this.savepatientrxdetails(this.orderid);*/
                        }
                    });
                }
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select order status'
            });
        }
    }



    Savecontactlensosvalueswithorderid(orderid, newsave, ositemid) {
        if (this.orderService.orderStatusGlobal !== undefined) {
            let osqty;
            let praccode = '';
            if (this.softcontactlensossavevalue !== undefined && this.softcontactlensossavevalue.length !== 0 &&
                this.softcontactlensossavevalue !== '') {
                const value = this.contactlensService.contactlensotherdetails.filter(x => x.itemId ===
                    this.softcontactlensossavevalue.item_id);

                if (value.length === 0) {
                    osqty = 1;
                    praccode = '';
                } else {
                    osqty = value[0].qty;
                    praccode = value[0].item_prac_code;
                }
            }

            const softcontactlensosvaluedetails = {
                'patient_id': this.app.patientId,
                'item_prac_code': praccode,
                'loc_id': this.app.locationId,
                'item_id': this.softcontactlensossavevalue.item_id,
                'upc_code': this.softcontactlensossavevalue.upc_code,
                'item_name': this.softcontactlensossavevalue.item_name,
                'module_type_id': this.orderLensSoftLensForm.controls['module_type_id'].value,
                'lens_vision': 'OS',
                'qty': osqty,
                'ordertypeid': '5',
                'manufacturer_id': this.softcontactlensossavevalue.manufacturer_id,
                'order_status_id': this.orderService.orderStatusGlobal,
                'price_retail': this.retail_PriceOS
            };

            this.softcontactpostosvalues = softcontactlensosvaluedetails;
            if (ositemid !== '' && ositemid != null) {
                this.orderService.updateOrderFrames(this.softcontactpostosvalues, orderid, ositemid).subscribe(data => {
                    if (data['status'] === 'New Order Crated') {
                        this.orderService.orderId = data['order_id'];

                    }
                });
            } else if ((ositemid === '' || ositemid == null) && orderid !== '') {
                this.orderService.saveorderdetailswithorderId(this.softcontactpostosvalues, orderid).subscribe(data => {
                    if (data['status'] === 'New Item added to the order.') {
                        this.orderService.orderId = data['order_id'];
                        this.orderid = this.orderService.orderId;
                    }
                });
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select Orderstatus'
            });
        }
    }

    SoftcontactDataClick(value) {

        this.selectContactlensSidebar = true;
        const data = {
            'IshardContactlens': '0',
            'LensType': value
        };
        this.contactlensService.IsSoftContactLens.next(data);
    }


    /**
     * Savepatientrxdetails order soft contactlens component
     * @param {string} orderid getting order id
     * @returns {object} for saving rxdetails
     */
    savepatientrxdetails(orderid) {
        const outsidevalue = this.orderLensSoftLensForm.controls['outside_rx'].value;
        if (outsidevalue !== true) {
            this.orderLensSoftLensForm.controls['outside_rx'].patchValue(false);
        }
        this.orderLensSoftLensForm.controls['order_id'].patchValue(this.orderService.orderId);
        this.orderLensSoftLensForm.controls['custom_rx'].patchValue('1');
        this.orderLensSoftLensForm.controls['det_order_id'].patchValue('1');
        this.orderLensSoftLensForm.controls.physician_id.patchValue(this.orderService.physicianID);
        this.orderLensSoftLensForm.controls.hard_contact.patchValue(0);
        if (this.orderLensSoftLensForm.controls['physician_id'].value !== '') {
            if (this.app.patientId) {
                if (this.orderLensSoftLensForm.value) {
                    this.orderSearchService.savepatientlensdetails(this.orderLensSoftLensForm.value,
                        this.app.patientId).subscribe(data => {
                            // this.orderService.OrderAlertPopup.next('save');
                            this.orderService.orderFormUpdate.next('success');
                            this.ordervalueupdate = true;
                            // let encounter = this.orderService.orderId;
                            // this.orderService.getOrderStatusData(encounter).subscribe(data=>{
                            //     this.orderService.orderEncounterId.next( data['order_enc_id']);
                            // })
                            // this.SuccessShow("Saved Successfully.");
                            // //this.orderService.ordercontactlensotherdetailsselection.next(patientid);

                        });
                }
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select physician'
            });
            return false;
        }

    }

    /**
     * Update patientrx details order soft contactlens type
     * @param {string} orderid getting order id
     * @returns {object} for update rxdetails
     */
    updatepatientrxdetails(patientid, patientrx) {
        // this.contactlensService.errorHandle.msgs = [];
        this.orderLensSoftLensForm.controls['custom_rx'].patchValue('1');
        this.orderLensSoftLensForm.controls['det_order_id'].patchValue('1');
        this.orderLensSoftLensForm.controls.physician_id.patchValue(this.orderService.physicianID);
        this.orderLensSoftLensForm.controls.hard_contact.patchValue(0);
        if (this.orderLensSoftLensForm.controls.physician_id.value > 0) {
            if (this.app.patientId) {
                if (this.orderLensSoftLensForm.value) {
                    this.orderSearchService.updatepatientlensdetails(this.orderLensSoftLensForm.value, patientid, patientrx)
                        .subscribe(data => {
                            // this.SuccessShow('Updated Successfully.');
                            this.errorService.displayError({
                                severity: 'success',
                                summary: 'success Message', detail: 'Updated Successfully.'
                            });
                            if (this.orderid != null) {
                                this.orderService.orderspectacleLensSave.next('5');
                                if (this.orderSearchService.paymentOrderNavigation === true) {
                                    this.orderService.getOrderStatusData(this.orderid).subscribe(DataSet => {
                                        this.orderSearchService.orderSaveUpdateStatus = false;
                                        this.orderSearchService.orderFormChangeStatus = true;
                                        this.router.navigate(['/order/payments'], { queryParams: { encounter_id: DataSet['order_enc_id'] } });
                                        // this.orderService.orderEncounterId.next(data['order_enc_id']);
                                        // console.log(this.locationId)
                                        this.orderService.OrderAlertPopup.next('update');
                                    });
                                }
                            }
                        });

                }
            }
        } else {
            this.errorService.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'Please select physician'
            });
            return false;
        }
    }


    getordertype(orderid) {
        this.orderdata = [];
        this.orderSearchService.getOrderSearchMaster(orderid, '', '', '', '', '', '', 1000, '', '', '').subscribe(DataSet => {
            this.orderdata = DataSet['result']['Optical']['Orders'];
            this.disablecheckbox = true;
            if (this.orderdata[0].ordertypeid === '5') {
                this.contactlensService.savecontactlensupdate = 'Soft Contact Lens';
                this.getcontactlensordervaluewithorderid(orderid);

                this.softcontactlens = false;
            }
            if (this.orderdata[0].ordertypeid === '4') {
                this.contactlensService.Disablehardcontactcomponent.next(true);
                this.contactlensService.HardContactbyorderID.next(orderid);
                //  this.getcontactlensordervaluewithorderid(orderid);
                this.contactlensService.savecontactlensupdate = 'Hard Contact Lens';
                //  this.Ordertype="Contact Lens";
            }
        });
    }




    getcontactlensordervaluewithorderid(orderidvalue) {
        this.orderid = orderidvalue;
        this.orderSearchService.getOrderItemDetails(orderidvalue).subscribe(orderdetails => {
            this.ordersavedvalues = [];
            this.softcontactlensodsavevalue = [];
            this.softcontactlensossavevalue = [];
            this.ordersavedvalues = orderdetails;
            this.orderLensSoftLensForm.controls['module_type_id'].patchValue('3');
            this.orderLensSoftLensForm.controls['patient_id'].patchValue(this.app.patientId);
            this.issoftcontactlensvalid = true;
            const odsavedvalues = this.ordersavedvalues.filter(x => x.lens_vision === 'OD' && x.order_id === parseInt(orderidvalue, 10));
            if (odsavedvalues !== '' && odsavedvalues !== undefined) {
                this.orderService.softContactlensOd = odsavedvalues[0].item_id;
                const oddetails = odsavedvalues;
                this.softcontactlensodsavevalue = odsavedvalues;
                this.softcontactlensodsavevalue = odsavedvalues[0];
                this.softcontactlensodsavevalue.item_id = odsavedvalues[0].item_id;
                this.softcontactlensodsavevalue.item_name = odsavedvalues[0].item_name;
                this.softcontactlensodsavevalue.name = odsavedvalues[0].item_name;
                this.retail_PriceOD = odsavedvalues[0].price_retail;
                this.orderLensSoftLensForm.controls['ODitemid'].patchValue(odsavedvalues[0].id);
                this.orderLensSoftLensForm.controls['manufactureod'].patchValue(odsavedvalues[0].manufacturename);
                // tslint:disable-next-line:triple-equals
                const manufacturename = this.Manufacturers.filter(x => x.Id == odsavedvalues[0].manufacturer_id);
                if (manufacturename !== undefined && manufacturename.length !== 0) {
                    this.orderLensSoftLensForm.controls['manufactureod'].patchValue(manufacturename[0].Name);
                }
                // this.orderLensSoftLensForm.controls['manufactureodid'].patchValue(value.Softcontactlensod.manufacturer_id);
                this.orderLensSoftLensForm.controls['nameod'].patchValue(odsavedvalues[0].item_name);
                this.orderLensSoftLensForm.controls['colorod'].patchValue(odsavedvalues[0].color_id);
                this.orderLensSoftLensForm.controls['upcod'].patchValue(odsavedvalues[0].upc_code);
                // this.orderLensSoftLensForm.controls['sourceod'].patchValue(odsavedvalues[0].sourceod);
                this.orderLensSoftLensForm.controls['qtyod'].patchValue(odsavedvalues[0].qty);
            }
            const OSsavesvalues = this.ordersavedvalues.filter(x => x.lens_vision === 'OS' && x.order_id === parseInt(orderidvalue, 10));
            if (OSsavesvalues !== '' && OSsavesvalues !== undefined) {
                this.orderService.softContactlensOS = OSsavesvalues[0].item_id;
                this.softcontactlensossavevalue = OSsavesvalues;
                this.softcontactlensossavevalue = OSsavesvalues[0];
                this.softcontactlensossavevalue.item_id = OSsavesvalues[0].item_id;
                this.softcontactlensossavevalue.item_name = OSsavesvalues[0].item_name;
                this.retail_PriceOS = OSsavesvalues[0].price_retail;
                this.orderLensSoftLensForm.controls['OSitemid'].patchValue(OSsavesvalues[0].id);
                this.orderLensSoftLensForm.controls['manufactureos'].patchValue(OSsavesvalues[0].manufacturename);
                // tslint:disable-next-line:triple-equals
                const manufactureosname = this.Manufacturers.filter(x => x.Id == OSsavesvalues[0].manufacturer_id);
                if (manufactureosname !== undefined && manufactureosname.length !== 0) {
                    this.orderLensSoftLensForm.controls['manufactureos'].patchValue(manufactureosname[0].Name);
                }
                this.orderLensSoftLensForm.controls['nameos'].patchValue(OSsavesvalues[0].item_name);
                this.orderLensSoftLensForm.controls['coloros'].patchValue(OSsavesvalues[0].color_code);
                this.orderLensSoftLensForm.controls['upcos'].patchValue(OSsavesvalues[0].upc_code);
                // this.orderLensSoftLensForm.controls['sourceos'].patchValue(OSsavesvalues[0].sourceod);
                this.orderLensSoftLensForm.controls['qtyos'].patchValue(OSsavesvalues[0].qty);
            }
            this.orderLensSoftLensForm.controls['order_id'].patchValue(orderidvalue);
            this.getpatientrxdetailsbyorderid(this.orderid);
            this.ordervalueupdate = true;
        },
            error => {

            });
    }

    getpatientrxdetailsbyorderid(orderid) {
        const filter = {
            'filter': [{
                'field': 'order_id',
                'operator': '=',
                'value': orderid
            }], 'sort': [
                {
                    'field': 'id',
                    'order': 'ASC'
                }]
        };

        this.orderSearchService.getPatientrxContactLens(filter, this.app.patientId).subscribe(
            valuepaientrx => {
                this.contactlensvalue = [];
                const data = valuepaientrx['data'];
                this.contactlensvalue = valuepaientrx['data'];
                const index = this.contactlensvalue.length - 1;
                this.orderLensSoftLensForm.controls['id'].patchValue(data[index].id);
                this.orderLensSoftLensForm.controls['patientrxid'].patchValue(data[index].id);
                this.orderLensSoftLensForm.controls['base_od'].patchValue(data[index].base_od);
                this.orderLensSoftLensForm.controls['base_os'].patchValue(data[index].base_os);
                this.orderLensSoftLensForm.controls['diameter_od'].patchValue(data[index].diameter_od);
                this.orderLensSoftLensForm.controls['diameter_os'].patchValue(data[index].diameter_os);
                this.orderLensSoftLensForm.controls['sphere_od'].patchValue(data[index].sphere_od);
                this.orderLensSoftLensForm.controls['sphere_os'].patchValue(data[index].sphere_os);
                this.orderLensSoftLensForm.controls['axis_od'].patchValue(data[index].axis_od);
                this.orderLensSoftLensForm.controls['axis_os'].patchValue(data[index].axis_os);
                this.orderLensSoftLensForm.controls['add_od'].patchValue(data[index].add_od);
                this.orderLensSoftLensForm.controls['add_os'].patchValue(data[index].add_os);
                this.orderLensSoftLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensSoftLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensSoftLensForm.controls['cylinder_od'].patchValue(data[index].cylinder_od);
                this.orderLensSoftLensForm.controls['cylinder_os'].patchValue(data[index].cylinder_os);
                this.orderLensSoftLensForm.controls['multifocalid_os'].patchValue(data[index].multifocalid_os);
                this.orderLensSoftLensForm.controls['multifocalid_od'].patchValue(data[index].multifocalid_od);
                this.orderLensSoftLensForm.controls['physician_id'].patchValue(data[index].physician_id);
                this.orderService.physicianID = data[index].physician_id;
                if (data[index].outside_rx === 1) {
                    this.orderLensSoftLensForm.controls['outside_rx'].patchValue(true);
                } else {
                    this.orderLensSoftLensForm.controls['outside_rx'].patchValue(false);
                }
            },
            error => {

            });
    }



    LoadMasters() {
        this.RxSphereValues = this.orderSearchService.genDropDowns(
            { start: -20, end: 20, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'PL' });
        this.RxCylinderValues = this.orderSearchService.genDropDowns(
            { start: -10, end: 10, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: 'D.S.' });

        // Axis Degrees
        this.AxisValues = this.orderSearchService.genDropDowns(
            { start: 1, end: 180, step: 1, sign: false, roundOff: 0, slice: 3, neutral: '0' });

        // Add Values
        this.RxAddValues = this.orderSearchService.genDropDowns(
            { start: 0.25, end: 3.75, step: 0.25, sign: true, roundOff: 2, slice: 0, neutral: '0' });

    }

    // SuccessShow(message) {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: message });
    // }
    // WarningShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: message });
    // }
    selectevent(event) {
        if (event === 'close') {
            this.selectContactlensSidebar = false;
        }
    }

}
