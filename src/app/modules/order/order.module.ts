import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { OrderRoutingModule } from './order-routing.module';
import { SharedModule } from '@shared/shared.module';
import {
  OrderFrameComponent,
  OrderMainComponent,
  OrderFrameMainComponent,
  OrderHardContactLensComponent,
  OrderSoftContactlensComponent,
  OrderLensSelectionComponent,
  OrderShippingDetailsComponent,
  OrderOtherDetailsComponent,
  OrderButtonGroupComponent,
  OrderSelectContactlensodComponent,
  OrderOtherDetailsMainComponent,
  OrderSpectacleLensMainComponent,
  OrderSelectSpectacleLensComponent,
  OrderSearchComponent,
  OrderContactLensMainComponent,
  OrderSoftContactLensMainComponent,
  OrderHardContactLensMainComponent,
  OrderFilterComponent,
  OrderPrintSelectionComponent,
  OrderOtherDiscountComponent,
  OrderLabHistoryComponent,
  OrderOrderedItemsComponent,
  OrderTrayLabelComponent,
  PromiseDateHistoryComponent,
  StatusHistoryComponent,
  OrderSelectInventoryComponent,
  DeliveryComponent,
  AfterSalesComponent,
  OrderPaymentsComponent,
  SelectInventoryComponent,
  ReturnasWizardComponent,
  ModifySoftContactComponent,
  ModifyFramesComponent,
  ModifySpectacleLensComponent,
  ModifyLensTreatmentComponent,
  AddOrderOtherDetailsComponent,
  ReturnReasonComponent,
  AddOtherDiscountComponent,
  ClaimordersComponent
} from './';
import { OrderService } from '@services/order/order.service';
import { ConfirmationService } from 'primeng/api';
// import { ClaimsComponent } from './common/claims/claims/claims.component';


@NgModule({
  imports: [
    CommonModule,
    OrderRoutingModule,
    SharedModule
  ],
  declarations: [
    // order
    OrderFrameComponent,
    OrderMainComponent,
    OrderFrameMainComponent,
    OrderHardContactLensComponent,
    OrderSoftContactlensComponent,
    OrderLensSelectionComponent,
    OrderShippingDetailsComponent,
    OrderOtherDetailsComponent,
    OrderButtonGroupComponent,
    OrderSelectContactlensodComponent,
    OrderOtherDetailsMainComponent,
    AddOrderOtherDetailsComponent,
    OrderSpectacleLensMainComponent,
    OrderSelectSpectacleLensComponent,
    OrderSearchComponent,
    OrderContactLensMainComponent,
    OrderSoftContactLensMainComponent,
    OrderHardContactLensMainComponent,
    OrderFilterComponent,
    OrderPrintSelectionComponent,
    OrderOtherDiscountComponent,
    OrderLabHistoryComponent,
    OrderOrderedItemsComponent,
    OrderTrayLabelComponent,
    PromiseDateHistoryComponent,
    StatusHistoryComponent,
    ModifySoftContactComponent,
    ModifyFramesComponent,
    ModifySpectacleLensComponent,
    ModifyLensTreatmentComponent,
    OrderSelectInventoryComponent,
    DeliveryComponent,
    AfterSalesComponent,
    OrderPaymentsComponent,
    // PopupComponent,
    SelectInventoryComponent,
    ReturnasWizardComponent,
    ReturnReasonComponent,
    AddOtherDiscountComponent,
    // ClaimsComponent,
    ClaimordersComponent
  ],


  providers: [OrderService, ConfirmationService],
  schemas: []
})
export class OrderModule { }
