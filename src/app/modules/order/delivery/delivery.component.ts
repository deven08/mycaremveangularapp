import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
    selector: 'app-delivery-component',
    templateUrl: 'delivery.component.html'
})

export class DeliveryComponent implements OnInit {

    searchOrderSidebar = false;
    orderlists: any[];
    display: boolean;

    constructor( private confirmationService: ConfirmationService) {

    }
    ngOnInit(): void {
        this.orderlists = [
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
            {
                orderno: '6123', status: 'Open', delivereddate: '', location: 'MVE',
                orderdate: '', patientID: '2', lastname: '', firstname: '', balance: '1145.00', insbalance: '2', patbalance: '0.00'
            },
        ];
    }


    searchorder() {
        this.searchOrderSidebar = true;
    }
    deliveryPopup() {
        this.display = true;
    }
    onOkClick() {
        this.display = false;
    }
    deliverConfirmPopup() {
        this.confirmationService.confirm({
            message: 'Are you sure you want to deliver the orders selected?',
            header: 'myCare|MVE',
            icon: 'fa fa-exclamation-triangle',
            accept: () => {

            },
            reject: () => {

            }
        });
    }

}
