import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-ordered-items',
    templateUrl: 'ordered-items.component.html'
})
export class OrderOrderedItemsComponent implements OnInit {
    checked = false;
    @Output() orderedItemOutput = new EventEmitter<any>();
    ngOnInit(): void {

    }
    onOrderItemClose() {
        this.orderedItemOutput.emit('close');
    }
}
