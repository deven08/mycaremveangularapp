import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-print-selection',
    templateUrl: 'print-selection.component.html'
})
export class OrderPrintSelectionComponent implements OnInit {

    checked = false;
    pr: any;
    ipr: any;
    pir: any;
    br: any;
    lc: any;
    rlc: any;
    orderTypes;
    @Output() printOutput = new EventEmitter<any>();
    spinner: number;
    ngOnInit() { }
    onPrintClose() {
        this.printOutput.emit('close');
    }
}


