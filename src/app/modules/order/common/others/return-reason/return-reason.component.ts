import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { OrderService } from '@app/core/services/order/order.service';
import { ErrorService } from '@app/core/services/error.service';


@Component({
    selector: 'app-return-reason',
    templateUrl: 'return-reason.component.html'
})

export class ReturnReasonComponent implements OnInit {
    orderTypes: any[];
    groupBy: any[];
    returnorderlist: any[];
    @Input() returnInput: any[];
    @Output() returnoutput = new EventEmitter<any>();
    orderReturn: any = [];
    orderItemDetails: any = [];
    detailsItemId: any = []
    assignclass: String
    qty: any = '';
    /**
     * Original item of return reason component
     */
    originalItem: Array<object>;
    /**
     * Creates an instance of return reason component.
     * @param orderService common service for complete order
     * @param error for syntaxerrors
     */
    constructor(private orderService: OrderService,
        private error: ErrorService) {
    }
    ngOnInit() {
        this.originalItem = [{ id: '', name: 'Select Item' }, { id: 'Return To inventory', name: 'Return To inventory' },
        { id: 'Return To Manufacture', name: 'Return To Manufacture' }, { id: 'Salvage', name: 'Salvage' }];
        this.assignclass = 'form-control';
        for (let i = 0; i <= this.returnInput.length - 1; i++) {
            this.returnInput[i]['returnQty'] = '';
            this.returnInput[i]['returnreason'] = '';
            this.returnInput[i]['checked'] = false;
        }
        this.returnorderlist = this.returnInput;
        console.log(this.orderService.orderId);
        this.orderService.getOrderItemDetails(this.orderService.orderId).subscribe(data => {
            this.orderItemDetails = data;
        });
    }

    /**
     * Returns return reason component
     * closing the Return page
     */
    onReturnClose() {
        this.returnoutput.emit('close');
    }

    /**
     * Returns quantity
     * @returns  for save the Return Quantity details
     */
    returnQuantity() {
        this.orderReturn = [];
        let selectItem = [];

        for (let i = 0; i <= this.returnorderlist.length - 1; i++) {
            if (this.returnorderlist[i]['checked'] == true) {
                selectItem.push(this.returnorderlist[i]['checked'])
                if (this.returnorderlist[i]['returnQty'] > this.returnorderlist[i]['quantity']) {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'The return quantity should not be more then Actual quantity.Please verify the data'
                    });
                    return false;
                }
                if (this.returnorderlist[i]['returnreason'] === '') {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'The select reason for return'
                    });
                    return false;
                }
                const newobjec = {
                    order_detail_id: this.orderItemDetails[i]['id'],
                    return_qty: this.returnorderlist[i]['returnQty'] === '' ? '0' : this.returnorderlist[i]['returnQty'],
                    reason: this.returnorderlist[i]['returnreason']
                };
                this.orderReturn.push(newobjec);
            }
        }
        const returnData = {
            orderReturn: this.orderReturn
        };

        if (selectItem.length !== 0) {
            let returnQtyArray =[]
            for (let i = 0; i <= returnData.orderReturn.length - 1; i++) {
                if (returnData.orderReturn[i].return_qty !== '0') {
                    returnQtyArray.push(returnData.orderReturn[i].return_qty);
                } else {
                    this.error.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'Please select Quantity'
                    });
                }
            }
            this.orderService.returnDisplayQuantity(returnData).subscribe(data => {
                const returnResponse = []
                let resData:any = [];
                resData = data;
                for(let j=0;j<=resData.length - 1;j++){
                    returnResponse.push(data[j]);
                }
                
                if (returnQtyArray.length=== returnResponse.length) {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'success Message', detail: 'Order return Successfully'
                    });
                    this.returnoutput.emit('save');
                }
            });

        } else {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please select one of the Item to return'
            });
        }
       

    }
    /**
     * Orginals item reason
     * @param index get data Order return reason Item details
     * 
     */
    OrginalItemReason(index, selectReason) {
        this.returnorderlist[index]['returnreason'] = selectReason.target.value;
    }
    /**
     * Returns itemschecked
     * @param event for the chechbox 
     * @param index get data Order return Item details
     */
    returnItemschecked(event, index) {
        this.returnorderlist[index]['checked'] = event;
    }

}
