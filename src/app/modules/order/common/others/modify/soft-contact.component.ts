import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService, AppService } from '@app/core';
import { OrderService } from '@app/core/services/order/order.service';
import { Subscription } from 'rxjs';


@Component({
    selector: 'app-modify-soft-contact',
    templateUrl: 'soft-contact.component.html'
})
export class ModifySoftContactComponent implements OnInit {
    @Input() modifyId: any;
    @Output() modifysoftoutput = new EventEmitter<any>();
    formOrder: FormGroup;
    tax1_id: any;
    tax2_id: any;
    tax1_paid: any;
    tax2_paid: any;
    retailPrice: any;
    qty: any;
    subTotal: any;
    public labbylist: any[];
    public source: any[];
    public DataSet: any;
    orderOtherDetails: any;
    public insuranceDropDown = [];
    public insurancemodel: any = [];
    // TypeDropdown = [{ id: '1', type: 'Frame Only' }, { id: '2', type: 'Spectacle Lens' },
    // { id: '3', type: 'Contact Lens' }, { id: '4', type: 'Other' }];


    // orderReqbody: any[];
    // itemName: any;
    labId: any;
    locationId: any;
    patientId: any;
    orderTypeId: any;
    sourceId: any;
    upcCode: any;
    public chargevalues: any;
    frameUPCid: string;
    lensUPCODid: string;
    orderItemDetails: any = [];
    orderDetailIdItem: any;
    subscriptionType: Subscription;

    /**
     * Creates an instance of modify soft contact component.
     * @param _fb form builder
     * @param utility provide utility service
     * @param dropdownService provide dropdownService
     * @param errorService provide errorService
     * @param orderService provide orderService
     * @param app provide appservice
     */
    constructor(
        private _fb: FormBuilder,
        private utility: UtilityService,
        private dropdownService: DropdownService,
        private errorService: ErrorService,
        private orderService: OrderService,
        public app: AppService,
    ) {
        // this.subscriptionType = orderService.orderSelectTypeobj$.subscribe(data => {
        //     this.app.orderTypeId = data;
        // });
        // this.loadorderDetailsFortax()
    }
    ngOnInit() {
        // alert(this.modifyId[0]);
        this.LoadFormData();
        this.SupplierDropData();
        this.LoadSource();
        // this.getPatientInsurance();
        // this.LoadDetails(this.orderService.orderId);

        this.LoadDetails()

    }
    LoadFormData() {
        this.formOrder = this._fb.group({
            patient_id: '',
            upc_code: '',
            item_id: '',
            item_name: '',
            module_type_id: '',
            tax1_id: '',
            tax2_id: '',
            tax1_paid: '',
            tax2_paid: '',
            tax1_applied: false,
            tax2_applied: true,
            loc_id: '',
            ordertypeid: '',
            retailPrice: ['', [Validators.required]],
            subTotal: '',
            qty: ['', [Validators.required]],
            lab_id: '',
            source_id: '',
            description: '',


        });
        // this.ClaculateSubTotal();
    }

    /**
     * Loadorders details fortax for binding the form
     */
    loadorderDetailsFortax() {
        this.formOrder.controls['tax1_applied'].patchValue(this.modifyId[0].tax1_applied);
        this.formOrder.controls['tax1_id'].patchValue(this.modifyId[0].tax1_id);
        this.formOrder.controls['tax1_paid'].patchValue(this.modifyId[0].tax1_paid);
        this.formOrder.controls['tax2_applied'].patchValue(this.modifyId[0].tax2_applied);
        this.formOrder.controls['tax2_id'].patchValue(this.modifyId[0].tax2_id);
        this.formOrder.controls['tax2_paid'].patchValue(this.modifyId[0].tax2_paid);
        this.formOrder.controls['retailPrice'].patchValue(this.modifyId[0].price_retail);
        this.formOrder.controls['qty'].patchValue(this.modifyId[0].qty);
        this.formOrder.controls['description'].patchValue(this.modifyId[0].item_name);

    }


    CalculateSubTotal() {
        if (this.formOrder.controls['retailPrice'].value !== null || this.formOrder.controls['qty'].value !== null) {
            try {
                const data = this.formOrder.controls['retailPrice'].value * this.formOrder.controls['qty'].value;
                this.formOrder.controls['subTotal'].patchValue(data.toFixed(2));
            } catch (error) {
                this.errorService.syntaxErrors(error);

            }
        }
        // this.subTotal = this.retailPrice * this.qty;
    }
    CalculateTax() {
        // this.ClaculateSubTotal();
        if (this.formOrder.controls['subTotal'].value !== null) {
            try {
                let taxp1 = (this.formOrder.controls['subTotal'].value) * (this.formOrder.controls['tax1_id'].value / 100);
                this.formOrder.controls['tax1_paid'].patchValue(taxp1.toFixed(2));
                let taxp2 = (this.formOrder.controls['subTotal'].value) * (this.formOrder.controls['tax2_id'].value / 100);
                this.formOrder.controls['tax2_paid'].patchValue(taxp2.toFixed(2));
            } catch (error) {
                this.errorService.syntaxErrors(error);
            }
        }
        // if (this.formOrder.controls['tax1_id'].value === undefined ) {

        // }

    }


    SupplierDropData() {
        this.labbylist = [];
        let labsVenderData: any = [];
        this.labbylist = this.dropdownService.lablist;
        if (this.utility.getObjectLength(this.labbylist) === 0) {
            this.labbylist.push({ id: '', vendor_name: 'Select Lab' });
            this.dropdownService.getLabsVender().subscribe(
                data => {
                    try {
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            this.labbylist.push(labsVenderData.data[i]);
                        }
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.dropdownService.lablist = this.labbylist;
                });
        }

    }
    LoadSource() {
        let getsourcedata: any = [];
        this.source = [];
        this.DataSet = [];
        this.source = this.orderService.ordersource;
        if (this.utility.getObjectLength(this.source) === 0) {
            this.source.push({ Id: '', Name: 'Select Source' });
            this.orderService.getSourceMaster('').subscribe(
                data => {
                    try {
                        getsourcedata = data;
                        this.DataSet = getsourcedata.data;
                        this.DataSet.forEach(item => {
                            if (!this.source.find(a => a.Name === item.sourcename)) {
                                this.source.push({ Id: item.id, Name: item.sourcename });
                            }
                        });
                        this.orderService.ordersource = this.source;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }


                });
        }
    }
    saveDetails() {
        // alert(this.orderService.orderStatusGlobal)
        const orderdata = this.formOrder.value;

        const reqBody = {
            item_id: this.modifyId[0].id,
            item_name: this.modifyId[0].item_name,
            // "lab_id": this.labId,

            loc_id: this.app.locationId,

            module_type_id: this.modifyId[0].module_type_id,
            ordertypeid: this.app.orderTypeId,
            patient_id: this.app.patientId,
            // "qty": this.qty,
            // "retailPrice": this.retailPrice,
            // "source_id": this.sourceId,
            // "subTotal": this.subTotal,
            tax1_applied: false,
            tax1_id: orderdata.tax1_id === "" ? 0 : parseInt(orderdata.tax1_id),
            tax1_paid: parseInt(orderdata.tax1_paid),
            tax2_applied: true,
            tax2_id: orderdata.tax2_id === "" ? 0 : parseInt(orderdata.tax2_id),
            tax2_paid: parseInt(orderdata.tax2_paid),
            upc_code: this.modifyId[0].upc_code,
            order_status_id: this.orderService.orderStatusGlobal,
            price_retail: orderdata.retailPrice,
            qty: orderdata.qty
        };
        if ((this.modifyId[0]['module_type_id'] !== 1 && this.modifyId[0]['module_type_id'] !== 5) || (this.modifyId[0]['module_type_id'] !== '1' && this.modifyId[0]['module_type_id'] !== '5')) {
            reqBody['lens_vision'] = this.modifyId[0]['lens_vision']
        }

        // console.log(this.optical.chargevalues);
        // if( this.chargevalues.filter(x => x.checked === true)) {

        // }

        this.orderService.saveOtherDetails(this.orderService.orderId, this.modifyId[0].id, reqBody).subscribe(data => {
            this.errorService.displayError({
                severity: 'success',
                summary: 'Order Status', detail: 'Items Detail updated successfully'
            });
            this.orderService.getOrderItemDetails(this.orderService.orderId).subscribe(datas => {
                // alert('gggg');
                // console.log(datas);
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'Other Status', detail: 'Items OtherDetails updated successfully'
                });
                this.modifysoftoutput.emit('ok');
            });
        });
    }
    // saveDetails() {
    //     console.log(this.formOrder.value);
    // }
    onModifySoftClose() {
        this.modifysoftoutput.emit('close');
    }

    getPatientInsurance() {

        if (this.app.patientId !== 0) {
            const id = this.app.patientId;
            this.insuranceDropDown = [];
            this.dropdownService.getPatientInsurance(id).subscribe(data => {
                this.insurancemodel = data;
                this.insurancemodel.forEach(element => {
                    this.insuranceDropDown.push({ Id: element.id, Name: element.type + '-' + element.name });
                });
            });
        }
    }
    Insuraneselect(event) {
        this.formOrder.controls.insuranceplan.patchValue('');
        this.insurancemodel.forEach(element => {
            if (element.id.toString() === event.target.value) {

                this.formOrder.controls.insuranceplan.patchValue(element.plan_name);

            }
        });
    }

    /**
     * Loads details of single item
     */
    LoadDetails() {
        this.orderService.getOrderItemDetailsofItem(this.modifyId[0].id).subscribe(data => {
            // alert(data)
            this.modifyId[0] = [];
            // console.log(data);
            this.modifyId[0] = data;
            this.loadorderDetailsFortax();
        });
    }
}




