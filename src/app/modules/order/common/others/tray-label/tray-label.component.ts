import { Component, OnInit } from '@angular/core';
import { OrderService } from '@app/core/services/order/order.service';


@Component({
    selector: 'app-order-tray-label',
    templateUrl: 'tray-label.component.html'
})
export class OrderTrayLabelComponent implements OnInit {
    constructor(private orderService: OrderService) {

    }
    ngOnInit() {

    }
close() {
    this.orderService.FramesOnly.next('close');
}
}


