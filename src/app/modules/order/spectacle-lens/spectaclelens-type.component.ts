import { Component, OnInit, ViewChild } from '@angular/core';

// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { Manufacturer } from 'src/app/ConnectorEngine/models/Manufacturer.model';
// import { OrderSearchService } from 'src/app/ConnectorEngine/services/order.service';
// import { SpectcalelensService } from 'src/app/ConnectorEngine/services/spectcalelens.service';
import { Subscription } from 'rxjs';
import { SpectcalelensService } from '@app/core/services/order/spectcalelens.service';
import { OrderService } from '@app/core/services/order/order.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { Manufacturer, ManufacturerObject } from '@app/core/models/inventory/Manufacturer.model';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core/services';
import {  Data, LenColorData} from '@app/core/models/inventory/Colors.model';
import { InventoryModel, dataModel } from '@app/core/models/inventory/inventoryItems.model';
import { MasterDropDown, LensStyleMaster, SpectacleMaterial } from '@app/core/models/masterDropDown.model';
import { SpecMaterialObject, SpecMaterial } from '@app/core/models/inventory/SpecMaterial.model';
import { GettingSpectacleDetails } from './spectaclelens.model';
import { ManufacturerDropData } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-order-select-spectaclelens-od',
    templateUrl: 'spectaclelens-type.component.html'
})
export class OrderSelectSpectacleLensComponent implements OnInit {
    LensTypeData: String;
    serachSpectaclesForm: FormGroup;
    displayData: string;
    checked: boolean;
    subscriptionSaveLensTypeUPC: Subscription;
    orderTypes: any[];
    specatableList: object;
    Spectcolumns = [];
    MaterialListDropDown = [];
    lensstypellist = [];

    /**
     * Styles  is master data for binding the data to styles master dropdown of order select spectacle lens component
     */
    styles: Array<object>;
    materialid = '';
    lenstypeid = '';
    upcfilterdata = '';
    lensfilterdata = '';
    materialfilterdata = '';
    colorcodefilterdata = '';

    /**
     * Colors  of order master data to bind the master color dropdown select spectacle lens component
     */
    public colors: Array<object>;

    /**
     * Lensstylcopy  of master data is object for bind the style dropdown  order select spectacle lens component
     */
    lensstylecopy: object;

    /**
     * filterData object to filter this of the spectacle lens grid  of order select spectacle lens component
     */
    filterData: object;

    /**
     * Materials  of order master data for binding the data materails dropdown  spectacle lens component
     */
    materials: Array<object>;

    /**
     * Materiallist  of master data binding the api ressponse to the model  spectaclens material order select spectacle lens component
     */
    materiallist: Array<SpecMaterial>;


    // Manufacturers = [];

    /**
     * Manufacturers  of  maste r data for binding the data to the manufactures dropdown order select spectacle lens component
     */
    public Manufacturers: Array<MasterDropDown>;

    /**
     * Manufacturerlist  of master data binding api response to the model Manufacturer of order select spectacle lens component
     */
    public manufacturerlist: Array<Manufacturer>;
    public locations: Array<MasterDropDown>;
    /**
     * View child of order select spectacle lens component
     * reset the table
     */
    @ViewChild('dt') public dt: any;
    /**
     * Total records of order select spectacle lens component
     * stores total number of records
     */
    totalRecords: string;
    /**
     * Pagination value of order select spectacle lens component
     * stores current page value
     */
    paginationValue: number;
    /**
     * Pagination page of order select spectacle lens component
     * declare the default pagination number
     */
    paginationPage: number;
    /**
     * Creates an instance of order select spectacle lens component.
     * @param _fb  for formbuilder
     * @param spectcalelensServicee for spectacle lens methods
     * @param orderService  common service for complete order 
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength 
     * @param error for syntaxerrors
     */
    constructor(private _fb: FormBuilder, private spectcalelensServicee: SpectcalelensService,
        private orderService: OrderService, private dropdownService: DropdownService,
        private utility: UtilityService, private error: ErrorService,
    ) {
        this.LensTypeData = this.orderService.spectaclesLensType;
        this.paginationValue = 0;
        this.paginationPage = 0;
        // this.subscriptionSaveLensTypeUPC = orderService.orderLensTypeUPC$.subscribe(data => {
        //     this.LensTypeUPC = data;
        // });
    }
    ngOnInit() {
        this.Loadformdata();
        this.getSpectacleLensData();
        this.Spectcolumns = [
            { field: 'Name', header: 'Name' },
            { field: 'StyleName', header: 'Style' },
            { field: 'Manufacturer', header: 'Manufacturer' },
            { field: 'MaterialName', header: 'Material' },
            { field: 'UPC', header: 'UPC' },
            { field: 'ColorName', header: 'Color' },
            { field: 'Color Code', header: 'Code' }

        ];
        this.LoadMaterialTypeDetails();
        this.LoadManufacturers();
        this.LoadLensStyle();
        this.LoadColors();

    }

    /**
     * Loadformdatas order select spectacle lens component
     */
    Loadformdata() {

        this.serachSpectaclesForm = this._fb.group({
            materialid: '',
            lenstypeid: '',
            upc_code: '',
            color_code: ''

        });

    }


    /**
     * filterPayLoad here it will create the object for the filtering of grid data
     */
    filterPayLoad() {
        const spectaclelens = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "2"
                },
                // {
                //     field: "id",
                //     operator: 'LIKE',
                //     value: '%' + this.searchValue + '%'
                // },
                {
                    field: "upc_code",
                    operator: 'LIKE',
                    value: '%' + this.serachSpectaclesForm.controls.upc_code.value + '%'
                },
                {
                    field: "material_id",
                    operator: "=",
                    value: this.serachSpectaclesForm.controls.materialid.value
                },
                // {
                //     field: "manufacturer_id",
                //     operator: "=",
                //     value: this.searchManufa
                // },
                {
                    field: "type_id",
                    operator: "=",
                    value: this.serachSpectaclesForm.controls.lenstypeid.value
                },
                {
                    field: "color_code",
                    operator: 'LIKE',
                    value: '%' + this.serachSpectaclesForm.controls.color_code.value + '%'
                }
            ],
        };
        const resultData = spectaclelens.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.filterData = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }
            ]
        };
    }

    /**
     * Gets spectacle lens data for grid loading spectacle lens grid for selection of the spectacle lens items.
     */
    getSpectacleLensData() {
        this.filterPayLoad();
        this.orderService.getUpcItemsGridData(this.filterData).subscribe((resp: InventoryModel) => {
            try {
                this.specatableList = resp.data;
                this.totalRecords = resp['total'];
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        })
    }

     /**
      * Loads manufacturers
      *  @param {Array} materials for binding dropdown data
      * @returns {object}  for dropdown data binding
      */
    LoadMaterialTypeDetails() {
        this.materials = this.dropdownService.materials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    this.materials.push({ value: '', name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materials.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.materials = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads manufacturers
     *  @param {Array} Manufacturers for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadManufacturers() {

        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
        /**
         * Loads manufacturers
         *  @param {Array} styles for binding dropdown data
         * @returns {object}  for dropdown data binding
         */
    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        this.lensstylecopy = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    this.lensstylecopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads colors
     * @returns color list de
     */
    LoadColors() {
        this.colors = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.colors) === 0) {
                this.dropdownService.getFrameLenscolor().subscribe((values: LenColorData) => {
                    try {
                    this.colors.push({ Id: '', Name: 'Select Color' });
                    values.data.forEach(element => {
                        this.colors.push({ Id: element.id, Name: element.colorname, colorcode: element.colorcode });
                    });
                    this.dropdownService.lensColors = this.colors;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
                });
        }

    }


    /**
     * Objs changed in this fuction when data changed in the dropdown then data can be filters based on the changed data.
     */
    objChanged() {
        this.getSpectacleLensData();
    }

    /**
     * OnRowselect we can get the particular line of data from the grid.
     * @param {object} data 
     */
    onRowSelect(data) {
        // console.log(type);
        // let displayData = data+type
        // console.log(displayData)
        if (this.LensTypeData === 'OD') {
            this.displayData = data.data;
            //   data.data.UPC + "(" + data.data.Name + ")"
        }
        if (this.LensTypeData === 'OS') {
            this.displayData = data.data;
            // data.data.UPC + "(" + data.data.Name + ")"

        }

    }

    /**
     * On cancel button click going close the side bar of spectacle lens inventory items.
     */
    Cancel() {
        this.orderService.orderRowSeletedUPCOD.next('close');
    }
    selectedValue() {
        if (this.LensTypeData === 'OD') {
            this.orderService.orderRowSeletedUPCOD.next(this.displayData);
        }
        if (this.LensTypeData === 'OS') {

            this.orderService.orderRowSeletedUPCOS.next(this.displayData);
        }

    }
    DoubleClickLensTypeUPC(data, type) {
        console.log(data);
        if (this.LensTypeData === 'OD') {
            this.displayData = data;
            //  data.UPC + "(" + data.Name + ")"
            this.orderService.orderRowSeletedUPCOD.next(this.displayData);

        }
        if (this.LensTypeData === 'OS') {
            this.displayData = data;
            //    data.UPC + "(" + data.Name + ")"
            this.orderService.orderRowSeletedUPCOS.next(this.displayData);
        }
    }

    /**
     * Resets order filters  select spectacle lens component
     */
    reset() {
        this.serachSpectaclesForm.reset();
        this.serachSpectaclesForm.controls.color_code.patchValue('');
        this.serachSpectaclesForm.controls.upc_code.patchValue('');
        this.getSpectacleLensData();
        this.dt.reset();
    }


    /**
     * Gets frame list by paginator
     * @param event diplay current page number
     */
    getSpectacleListByPaginator(event) {
        this.paginationValue = event.first;
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.loadSpectacleData(page);
    }
    /**
     * Loads frame data
     * @param [page] loading pagewise data
     */
    loadSpectacleData(page: number = 0) {
        this.filterPayLoad();
        this.orderService.getDataByPagination(this.filterData, page).subscribe(
            (data: GettingSpectacleDetails) => {
                const frameData = data.data;
                this.totalRecords = data.total;
                this.specatableList = [];
                this.specatableList = frameData;
            },
        );
    }



}
