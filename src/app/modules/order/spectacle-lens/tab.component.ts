// import { ApiAlertPipe } from 'src/app/shared/pipes/api-alert.pipe';
import { Component, OnInit } from '@angular/core';

import { OrderService } from '@app/core/services/order/order.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs';
import { AppService, ErrorService } from '@app/core';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';

@Component({
    selector: 'app-order-spectaclelens-main',
    templateUrl: './tab.component.html'
})
export class OrderSpectacleLensMainComponent implements OnInit, OnDestroy {
    addPatientInfoSidebar;
    paymentDetails;
    orderTypes: any[];
    table2: any[];
    ShippingEnableStatus = false;
    FramesScreenVisible = true;
    LensScreenVisible = false;
    OtherScreenVisible = false;
    ShippingScreenVisible = false;
    subscribeShippingStatus: Subscription;
    subscribelensOdPrice: Subscription;
    subscribelensOsPrice: Subscription;
    subscribeFramePrice: Subscription;
    subscribeorderPrice: Subscription;
    subscribeAlertPopUp: Subscription;
    framesSpecPrice = '';
    lensSpecPrice = '';
    otherSpecPrice = '';
    orderPrice = '';
    framesSpeclable = false;
    lensSpeclable = false;
    otherSpeclable = false;
    orderlable = false;
    rxId: string;
    orderlensalertpopupmessage: Subscription;
    subscriptionType: Subscription;
    SelectTypeModel: any;
    constructor(private orderService: OrderService, private errorService: ErrorService,
        private app: AppService) {
        this.subscribeFramePrice = orderService.framesPrice$.subscribe(data => {
            this.framesSpeclable = true;
            this.framesSpecPrice = ('Frames:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.framesSpeclable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });
        this.subscribelensOdPrice = orderService.lensSelectionODPrice$.subscribe(data => {
            this.lensSpeclable = true;
            this.lensSpecPrice = ('Lens:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.lensSpeclable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });
        this.subscribelensOsPrice = orderService.lensSelectionOSPrice$.subscribe(data => {
            this.otherSpeclable = true;
            this.otherSpecPrice = ('other details:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.otherSpeclable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });
        this.subscribeorderPrice = orderService.orderDetails$.subscribe(data => {
            this.orderlable = true;
            this.orderPrice = ('Order total:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.orderlable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });

        this.orderService.orderScreenMultipleSavePath = 'Spectacle Lens';
        this.orderService.orderScreenSavePath = 'Frames';
        this.subscribeShippingStatus = orderService.ShippingDetalesEnable$.subscribe(data => {
            if (data === true) {
                this.ShippingEnableStatus = true;
                this.ShippingScreenVisible = true;
                this.FramesScreenVisible = false;
                this.LensScreenVisible = false;
                this.OtherScreenVisible = false;
                this.orderService.ShippingTo.next(true);
            } else {
                this.ShippingEnableStatus = false;
                this.ShippingScreenVisible = false;
                this.FramesScreenVisible = true;
                this.LensScreenVisible = false;
                this.OtherScreenVisible = false;
            }

        });
        this.errorService.msgs = [];
        this.subscribeAlertPopUp = orderService.OrderAlertPopup.subscribe(data => {
            if (data === 'save') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Saved Successfully' });

            }
            if (data === 'update') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Updated Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Updated Successfully' });

            }
        });

        this.orderlensalertpopupmessage = this.orderService.orderPopupAlertPopup$.subscribe(data => {
            if (data.messagetype === 'required') {
                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: data.message });
                // this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: data.message });

                return false;
            }
        });
    }
    ngOnDestroy() {
        this.subscribeShippingStatus.unsubscribe();
        this.subscribeFramePrice.unsubscribe();
        this.subscribelensOdPrice.unsubscribe();
        this.subscribelensOsPrice.unsubscribe();
        this.subscribeorderPrice.unsubscribe();
        this.subscribeAlertPopUp.unsubscribe();
        this.orderlensalertpopupmessage.unsubscribe();
    }
    ngOnInit(): void {
        this.orderService.lensSelectionPopCondition = false;
        this.orderService.orderId = 'New Order';
        this.orderService.showCategory = true;
        console.log('orderspectaclelens');
        // localStorage.removeItem('Order_id');
        this.orderService.orderSelectTypeobj.next(7);
        this.app.orderTypeId =  '2';
        this.orderService.showorderdiv.next(true);
        this.orderTypes = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];
        this.table2 = [
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '0.00', itemtotal: '6.00', retail: '3.00', unitprice: '6.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '6.00', total: '3.00',
                insurance: '6.00', itemtotal: '6.00', retail: '6.00', unitprice: '1.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '10.00', itemtotal: '12.00', retail: '3.00', unitprice: '0.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '', total: '6.00',
                insurance: '', itemtotal: '6.00', retail: '3.00', unitprice: '12.00'
            },
        ];
        this.orderService.orderLocation.next(this.app.locationId);

        this.orderService.FramesOnly.next('Spectacle lens');
        this.rxId = this.app.RxValue;
        // localStorage.getItem('RxValue');
        if (this.rxId != null && this.rxId !== '' && this.rxId !== undefined) {
            this.orderFromPreviousPrescriptionPage();
        }
        this.orderService.lensSelectionUPCStatus = '';
        this.orderService.disableSavePayBtn = false;
        // this.orderService.orderStatusGlobal = '';
        this.orderService.orderLabValid = false;
    }
    orderFromPreviousPrescriptionPage() {
        this.orderService.orderScreenSavePath = 'Lens Selection';
        this.orderService.lensSelectionPopUP.next(this.rxId);
        // this.orderService.lensSelectionPopCondition = true;
        this.FramesScreenVisible = false;
        this.LensScreenVisible = true;
        this.OtherScreenVisible = false;
        this.orderService.lensSelectionPopCondition = false;
        this.orderService.orderScreenMultipleSavePath = 'Spectacle Lens';
        // localStorage.removeItem('RxValue');
    }

    PageClick(event) {
        // localStorage.removeItem('RxValue');
        const data = event.index;

        if (data === 0) {
            this.orderService.orderScreenSavePath = 'Frames';
            this.FramesScreenVisible = true;
            this.LensScreenVisible = false;
            this.OtherScreenVisible = false;
            this.orderService.lensSelectionPopCondition = false;

        }
        if (data === 1) {
            this.orderService.orderScreenSavePath = 'Lens Selection';

            this.orderService.lensSelectionPopUP.next('');
            this.orderService.lensSelectionPopCondition = true;
            this.FramesScreenVisible = false;
            this.LensScreenVisible = true;
            this.OtherScreenVisible = false;

        }
        if (data === 2) {
            this.orderService.orderScreenSavePath = 'Other Details';
            this.FramesScreenVisible = false;
            this.LensScreenVisible = false;
            this.OtherScreenVisible = true;
            this.orderService.lensSelectionPopCondition = false;
        }
        if (data === 3) {
            this.orderService.orderScreenSavePath = 'Shipping Details';
            this.FramesScreenVisible = false;
            this.LensScreenVisible = false;
            this.OtherScreenVisible = false;
            this.orderService.lensSelectionPopCondition = false;
        }
    }

}
