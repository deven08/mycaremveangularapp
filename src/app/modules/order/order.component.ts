import { Component, OnInit } from '@angular/core';
import { OrderService } from '@app/core/services/order/order.service';


@Component({
    selector: 'app-order-main',
    templateUrl: './order.component.html'
})
export class OrderMainComponent implements OnInit {
    addPatientInfoSidebar;

    constructor(private orderService: OrderService) {
    }
    ngOnInit(): void {
        this.orderService.showorderdiv.next(true);

    }
}
