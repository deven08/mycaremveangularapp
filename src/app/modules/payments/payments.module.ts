import { CommonModule } from '@angular/common';
import {
  ManagerDiscountComponent,
  PaymentAdjustmentComponent,
  PaymentCheckoutComponent,
  PaymentCheckoutPaymentComponent,
  PaymentLayoutComponent,
  PaymentPrintReceiptComponent,
  PaymentSearchOrderComponent,
  PatientSearchPaymentsComponent,
  PaymentSearchCheckoutComponent

} from './';
import { NgModule } from '@angular/core';
import { PaymentsRoutingModule } from './payments-routing.module';
import { SharedModule } from '@app/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    // CommonLoadsModule,
    PaymentsRoutingModule,
    SharedModule
  ],
  declarations: [
    PaymentSearchOrderComponent,
    PaymentCheckoutComponent,
    PaymentSearchCheckoutComponent,
    ManagerDiscountComponent,
    PaymentPrintReceiptComponent,
    PaymentLayoutComponent,
    PaymentAdjustmentComponent,
    PaymentCheckoutPaymentComponent,
    PatientSearchPaymentsComponent
  ]

})
export class PaymentsModule { }
