import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';
import { AppService, ErrorService } from '@app/core';
// import { PatientSearchService } from '../ConnectorEngine/services/patient-search.service';

// import { StateInstanceService } from '../shared/state-instance.service';
// import { UserService } from '../ConnectorEngine/services/user.service';




@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html'
})
export class CalendarComponent implements OnInit {

  CalendarUrl: Object;
  data: any = null;
  recallPatient = 1;
  /**
   * Creates an instance of calendar component.
   * @param service for calling iframes methods
   * @param app for calling app service methods
   */
  constructor(private service: IframesService,private app:AppService,
    private error: ErrorService) {
    this.CalendarUrl = this.service.routeUrl('CalendarUrl', true);
  }

  ngOnInit() {
    this.setPatientDynamically();
  }

  /**
   * Sets patient dynamically
   * @returns {object}  for adding patient dynamiclly 
   */
  setPatientDynamically() {
    let dt: any = [];
    this.app.getPatientInCON().subscribe(
      data => {
        try {
          dt = data;
          if (dt.result.Optical.PatientId !== undefined) {
            this.app.patientId = dt.result.Optical.PatientId;
            this.app.isPatientSigned = true;
            // this.user.resetPatientAssigned();
            // this.user.setPatientAssigned(dt.result.Optical.PatientId);
            // this._StateInstanceService.PatientId = dt.result.Optical.PatientId;
            // localStorage.setItem('PatientId', dt.result.Optical.PatientId);
          }
        } catch (err) {
          this.error.syntaxErrors(err);
        }
      },
      (err) => {

      }
    );


  }

  onLoad(e) {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
    this.setPatientDynamically();
  }

}
