import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit, AfterViewInit {

  BatchPaymentUrl: any = '';

  constructor(private iframeService: IframesService) {

    this.BatchPaymentUrl = this.iframeService.routeUrl('BatchPaymentUrl', true);

  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.iframeService.resizeIframe();
  }

}
