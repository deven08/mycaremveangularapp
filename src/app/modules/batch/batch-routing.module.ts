import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  EraComponent,
  LabOrdersComponent,
  PaymentsComponent,
  BatchOrderStatusUpdateComponent
} from './'


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'era',
        component: EraComponent,
        data: { 'title': 'ERA  Batch' }
      },
      {
        path: 'order-status',
        component: BatchOrderStatusUpdateComponent,
        data: { 'title': 'Order Status  Batch' }
      },
      {
        path: 'payments',
        component: PaymentsComponent,
        data: { 'title': 'Payments  Batch' }
      },
      {
        path: 'laborder',
        component: LabOrdersComponent,
        data: { 'title': 'Lab Orders  Batch' }
      }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports: [RouterModule]
})
export class BatchRoutingModule { }
