import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppService } from '@app/core';

@Component({
    selector: 'app-criteria-search',
    templateUrl: 'criteria-search.component.html'
})
export class CriteriaSearchComponent implements OnInit {
    constructor(public app: AppService) {

    }
    @Output() labordersearch = new EventEmitter<any>();
    ngOnInit() { }
    onSearchClose() {
        this.labordersearch.emit('close');
    }
}
