import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-lab-order-component',
    templateUrl: 'lab-order.component.html'
})

export class LabOrdersComponent implements OnInit {
    criteriaSearchSidebar = false;
    ngOnInit() { }
    criteriaSearch() {
        this.criteriaSearchSidebar = true;
    }
    labordersearchevent(event) {
        if (event === 'close') {
            this.criteriaSearchSidebar = false;
        }
    }
}
