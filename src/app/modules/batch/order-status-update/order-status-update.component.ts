import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-order-status-update',
    templateUrl: 'order-status-update.component.html'
})
export class BatchOrderStatusUpdateComponent implements OnInit {
    batchOrderStatus: any[];
    batchAddAllSidebar = false;
    batchOrderStatusSidebar = false;

    ngOnInit(): void {
        this.batchOrderStatus = [
            { name: '1127', value: '03/30/2018', price: 'Jennifer Watstein' },
        ];
    }

}
