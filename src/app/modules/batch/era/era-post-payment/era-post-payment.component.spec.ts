import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EraPostPaymentComponent } from './era-post-payment.component';

describe('EraPostPaymentComponent', () => {
  let component: EraPostPaymentComponent;
  let fixture: ComponentFixture<EraPostPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EraPostPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EraPostPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
