// Core Modules
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';




// Setting Up Component
@Component({
  selector: 'app-era-post-payment',
  templateUrl: './era-post-payment.component.html',
  styleUrls: ['./era-post-payment.component.css']
})
export class EraPostPaymentComponent implements OnInit, AfterViewInit {

  PostPaymentsEra: any = '';

  constructor(private iframeService: IframesService) {

    this.PostPaymentsEra = this.iframeService.routeUrl('PostPaymentsEra', true);

  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.iframeService.resizeIframe();
  }

}
