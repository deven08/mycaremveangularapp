// Core Modules
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-upload-era',
  templateUrl: './upload-era.component.html',
  styleUrls: ['./upload-era.component.css']
})
export class UploadEraComponent implements OnInit, AfterViewInit {

  UploadEra: any = '';

  constructor(private iframeService: IframesService) {

    this.UploadEra = this.iframeService.routeUrl('UploadEra', true);

  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.iframeService.resizeIframe();
  }

}
