// Core Modules
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';


// Setting Up Component
@Component({
  selector: 'app-era-claim-rejection',
  templateUrl: './era-claim-rejection.component.html',
  styleUrls: ['./era-claim-rejection.component.css']
})
export class EraClaimRejectionComponent implements OnInit, AfterViewInit {

  ClaimRejectionEra: any = '';

  constructor(private iframeService: IframesService) {

    this.ClaimRejectionEra = this.iframeService.routeUrl('ClaimRejectionEra', true);

  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.iframeService.resizeIframe();
  }

}
