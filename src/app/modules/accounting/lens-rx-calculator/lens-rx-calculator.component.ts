import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'lens-rx-calculator',
    templateUrl: 'lens-rx-calculator.component.html'
})

export class LensRXCalculatorComponent implements OnInit {

    intermediateShow = true;
    powerCalculatorshow = false;
    vertexDistanceShow = false;
    ngOnInit() {

    }
    showIntermediate() {
        this.intermediateShow = true;
        this.powerCalculatorshow = false;
        this.vertexDistanceShow = false;
    }

    showPower() {
        this.powerCalculatorshow = true;
        this.intermediateShow = false;
        this.vertexDistanceShow = false;
    }

    showVertex() {
        this.vertexDistanceShow = true;
        this.intermediateShow = false;
        this.powerCalculatorshow = false;
    }
}

