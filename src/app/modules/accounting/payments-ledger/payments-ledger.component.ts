// App Settings
// Core Module
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-payments-ledger',
  templateUrl: './payments-ledger.component.html',
  styleUrls: ['./payments-ledger.component.css']
})
export class PaymentsLedgerComponent implements OnInit {

  paymentLedgerUrl: any = '';
  /**
   * Creates an instance of payments ledger component.
   * @param service for calling iframes methods
   * @param router for navigation
   */
  constructor(private service: IframesService, private router: Router) {
    this.paymentLedgerUrl = this.service.routeUrl('paymentLedgerUrl', true);
    /* For active routing*/
    // // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    // //   return false;
    // // };
    // this.router.events.subscribe((evt) => {
    //   if (evt instanceof NavigationEnd) {
    //     this.router.navigated = false;
    //   }
    // });
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
