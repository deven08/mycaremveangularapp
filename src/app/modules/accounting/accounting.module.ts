import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingRoutingModule } from './accounting-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { LensRXCalculatorComponent, IntermediateComponent, PowerPrismSlaboffComponent, VertexDistanceComponent } from './lens-rx-calculator';
import { PaymentsLedgerComponent } from './payments-ledger/payments-ledger.component';
import { QuickbooksSyncComponent } from './quickbooks-sync/quickbooks-sync.component';

@NgModule({
  declarations: [LensRXCalculatorComponent,
    PaymentsLedgerComponent,
    QuickbooksSyncComponent,
    IntermediateComponent,
    PowerPrismSlaboffComponent,
    VertexDistanceComponent,
    QuickbooksSyncComponent],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    SharedModule
  ]
})
export class AccountingModule { }
