import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { InventoryService } from '@app/core/services/inventory/inventory.service';


@Component({
    selector: 'app-sort-all-inventory',
    templateUrl: 'sort-all-inventory.component.html'
})

export class SortAllInventoryComponent implements OnInit {

    checked = false;
    /**
     * Output  of sort all inventory component
     */
    @Output() sortalloutput = new EventEmitter<any>();
    /**
     * Input  of sort all inventory component
     */
    @Input() sortAllInput: any;
    /**
     * SortList  of sort all inventory component for loading the header data dropdown
     */
    sortList: any[];
    /**
     * Sort data of sort all inventory component for assign the data
     */
    sortData: any;
    /**
     * Sort value of sort all inventory component for filter data
     */
    sortValue = '';
    /**
     * Module type id of sort all inventory component for load module Type ID
     */
    moduleTypeId = '';
    addThenBy = [];
    thenByValue = [];
    constructor(private inventoryService: InventoryService) {

    }

    ngOnInit() {
        this.sortAllList();
        this.addThenBy.push(this.addThenBy.length);
        this.addThenBy.push(this.addThenBy.length);
    }
    /**
     * Sorts all list for inventory sort all dropdown list
     */
    sortAllList() {
        switch (this.sortAllInput) {
            case 'frames': {
                this.sortList = [
                    { Id: 'id', name: 'ID' },
                    { Id: 'manufacturer_name', name: 'Manufacturer' },
                    { Id: 'brand', name: 'Brand Name' },
                    { Id: 'name', name: 'Name' },
                    { Id: 'color', name: 'Color Name' },
                    { Id: 'color_code', name: 'Color Code' },
                    { Id: 'qty_on_hand', name: 'On Hand' },
                    { Id: 'upc_code', name: 'UPC' }
                ];
                this.moduleTypeId = '1';
                break;
            }
            case 'spectacle-lens': {
                this.sortList = [
                    { Id: 'id', name: 'ID' },
                    { Id: 'name', name: 'Name' },
                    { Id: 'manufacturer_id', name: 'Manufacturer' },
                    { Id: 'type_id', name: 'Lens Style' },
                    { Id: 'material_id', name: 'Material' },
                    { Id: 'upc_code', name: 'UPC' }
                ];
                this.moduleTypeId = '2';
                break;
            }
            case 'contact-lens': {
                this.sortList = [
                    { Id: 'id', name: 'ID' },
                    { Id: 'manufacturer_id', name: 'Manufacturer' },
                    { Id: 'name', name: 'LensName' },
                    { Id: 'colorid', name: 'Color' },
                    { Id: 'cl_packaging', name: 'Packaging' },
                    { Id: 'IsTrial', name: 'Trial' },
                    { Id: 'IsDot', name: 'Dot' },
                    { Id: 'qty_on_hand', name: 'On Hand' },
                    { Id: 'upc_code', name: 'UPC' }
                ];
                this.moduleTypeId = '3';
                break;
            }
        }
    }
    /**
     * Sorts all ok for sort the data 
     */
    sortAllOk() {
        if (this.sortValue != null) {
            this.getSortAll();
        }
    }

    /**
     * Gets sort all filter the data and assign data
     */
    getSortAll() {

        const sortFilterdata = {
            filter:
                [
                    {
                        field: 'module_type_id',
                        operator: '=',
                        value: this.moduleTypeId
                    }
                ],
            sort:
                [
                    {
                        field: this.sortValue,
                        order: 'DESC'

                    }
                ]
        };
        if (this.thenByValue.length) {
            for (let i = 0; i <= this.thenByValue.length - 1; i++) {
                sortFilterdata.sort.push({
                    field: this.thenByValue[i],
                    order: 'DESC'
                })
            }
        }
        const getdata = 'data';
        this.inventoryService.getSortByAll(sortFilterdata).subscribe(data => {
            this.sortData = data[getdata];
            const obj = {
                gridData: this.sortData,
                sortValue: this.sortValue,
                thenValue: this.thenByValue,
                sortEnable:true
            };
            this.sortalloutput.emit(obj);
        });
    }

    /**
     * Adds dropdown list for addding dropdown dynamic
     */
    AddDropdownList() {
        if (this.addThenBy.length < this.sortList.length - 2) {
            this.addThenBy.push(this.addThenBy.length);
        }
    }

}
