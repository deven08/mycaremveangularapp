
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { DatePipe } from '@angular/common';
import { CommonValidator } from '@app/shared/validator/common-validator';
import { AppService } from '@app/core';
import { FrameSupplier, LocationsMaster, TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-frame-add-transaction',
    templateUrl: 'add-transaction.component.html'
})

export class FrameAddTransactionComponent implements OnInit {
    StockDetails: any = [];
    paymentTermsData: any = [];
    /**
     * Type of transaction data of frame add transaction component
     */
    typeOfTransactionData: Array<object>;
    locationlist: any;
    locations: Array<LocationsMaster>;
    /**
     * Suppliers  of frame add transaction component
     */
    suppliers: Array<object>;
    public supplierslist: any[];
    FarmesTransactionForm: FormGroup;
    Accesstoken = '';
    trasactionFramesMsg: Object;
    frameTrasactionItemId = '';
    @Output() FramesTransaction = new EventEmitter<any>();

    /**
     * Creates an instance of frame add transaction component.
     * @param contactlensService provide contactlensService
     * @param frameService provide frameService
     * @param _fb formbuilder
     * @param _InventoryCommonService provide inventoryCommonService
     * @param error  provide error service
     * @param utility provide utility service
     */
    constructor(
        private contactlensService: ContactlensService,
        private frameService: FramesService,
        private _fb: FormBuilder,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private dropdowns: DropdownService,
        public app: AppService,
        private datePipe: DatePipe ) {

    }

    ngOnInit(): void {
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();

        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        // this.getFramesStockDetails();
        this.Loadformdata();
        this.GetSupplierDetails();
        this.LoadLocations();
    }

    Loadformdata() {
        // this.Accesstoken = localStorage.getItem('tokenKey');
        this.frameTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        // alert(this.frameTrasactionItemId);
        // localStorage.getItem('FortrasactionItemID');
        this.FarmesTransactionForm = this._fb.group({
            // accessToken: this.Accesstoken,
            item_id: this.frameTrasactionItemId,
            loc_id: ['', [Validators.required]],
            // source: '',
            lot_no: ['',[Validators.required]],
            stock: ['',[Validators.required]],
            trans_type: ['', [Validators.required]],
            // reason: '',           
            cost: ['',[Validators.required]],
            invoiceno: ['',[Validators.required, CommonValidator.invoiceNumberValidator, Validators.maxLength(50)]],
            expirationdate: ['',[Validators.required]],
            supplierid: ['', [Validators.required]],
            notes: ['',[Validators.required]],
            received_date: ['',[Validators.required]],
            paymentterms: ['', [Validators.required]],
            returnDate: ['',[Validators.required]]
        });
    }

    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];

    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }
    LoadLocations() {
        this.locations = this.dropdowns.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdowns.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdowns.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    /**
     * Gets supplier details
     * @abstract supplierdetails data 
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdowns.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdowns.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdowns.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    typeOfInventoryTrasaction() {

        this.typeOfTransactionData = this.dropdowns.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdowns.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData  = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdowns.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }

    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdowns.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdowns.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdowns.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    saveFrameTransaction() {
        // this._ContactlensService.errorHandle.msgs = [];
        // console.log(this.FarmesTransactionForm.value);

        const transactionData = {
            inventory:
            {
                locationid: this.FarmesTransactionForm.controls['loc_id'].value,
                receive_date: this.datePipe.transform(this.FarmesTransactionForm.controls['received_date'].value, 'yyyy-MM-dd hh:mm:ss'),
                invoice: this.FarmesTransactionForm.controls['invoiceno'].value,
                notes: this.FarmesTransactionForm.controls['notes'].value,
                supplierid: this.FarmesTransactionForm.controls['supplierid'].value,
                module_type_id: '1'

            }
            ,
            stock:
            {
                trans_type: this.FarmesTransactionForm.controls['trans_type'].value,
                item_id: this.frameTrasactionItemId,
                paymenttermsid: this.FarmesTransactionForm.controls['paymentterms'].value,
                returndt: this.datePipe.transform(this.FarmesTransactionForm.controls['returnDate'].value, 'yyyy-MM-dd hh:mm:ss'),
                cost: parseInt(this.FarmesTransactionForm.controls['cost'].value),
                stock: this.FarmesTransactionForm.controls['stock'].value,
                expirationdate: this.datePipe.transform(this.FarmesTransactionForm.controls['expirationdate'].value, 'yyyy-MM-dd hh:mm:ss'),
                lot_no: this.FarmesTransactionForm.controls['lot_no'].value
            }

        };

        this.frameService.saveFrameTransactionDetails(transactionData).subscribe(data => {
            // alert(data);
            // for (let i = 0; i <= data['record'].length - 1; i++) {
            //     data['record'][i]['id'] = data['record'][i]['id'].toString();
            //     // data["result"]["Optical"]["Frames"][i]["del_status"] =
            //     // data["result"]["Optical"]["Frames"][i]["del_status"] == 0 ? "Active" : "Discontinue";

            // }
            this.trasactionFramesMsg = data;
            console.log(this.trasactionFramesMsg);
            if (this.trasactionFramesMsg['status'] === 'Inventory Transaction added successfully') {
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Frame Transaction Saved Successfully'
                });
                // this.SuccessShow();
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Frame Transaction Saved Successfully' });
                this.FramesTransaction.emit('');
                // this._FrameService.transactionClose.next('');
            } else {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Frame Transaction Failed'
                });
                // this.ErrorShow();

            }
        });
    }
    cancleClick() {
        this.FramesTransaction.emit('cancle');
    }
}
