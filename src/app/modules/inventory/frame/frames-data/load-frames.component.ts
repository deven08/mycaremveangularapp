import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';



@Component({
    selector: 'app-load-frames',
    templateUrl: 'load-frames.component.html',
    providers: [ConfirmationService]
})
export class LoadFramesComponent implements OnInit {

    ngOnInit() {
    }
    constructor( private confirmationService: ConfirmationService) {

    }
    printBarCode() {
        this.confirmationService.confirm({
            message: 'One or more selected row has not been imported. Would you like to import and print barcode?',
            header: 'myCare|MVE',
            icon: 'fa fa-exclamation-triangle',
            accept: () => {

            },
            reject: () => {

            }
        });
    }

}

