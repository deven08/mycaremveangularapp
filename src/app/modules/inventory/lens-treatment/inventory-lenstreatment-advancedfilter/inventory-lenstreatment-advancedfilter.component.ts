import { Component, OnInit, EventEmitter, Output } from '@angular/core';

// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService } from '@app/core/services/error.service';
import { TreatmentCategory } from '@app/core/models/masterDropDown.model';
// import { LensTreatmentService } from '../../../ConnectorEngine/services/lenstreatments.service';


@Component({
    selector: 'app-inventory-lenstreatment-advancedfilter',
    templateUrl: 'inventory-lenstreatment-advancedfilter.component.html'
})
export class InventoryLenstreatmentAdvancedFilterComponent implements OnInit {
    AdvanceSearchLensTreatmentForm: FormGroup;
    //  Id='';
    //  Name='';
    //  categories="";
    LensTreatments = [];
    lensTreatmentslist: any;
    locationlist = [];
    locations = [];
    SearchformObj: object = {
        Name: '',
        Id: '',
        VisionWebCode: '',
        categories: '',
        upc: '',
        onHand: '',
        retailPrice: '',
        code: '',
        loc_id: ''
    };
    @Output() lensAdvFilter = new EventEmitter<any>();
    constructor(private lensTreatmentService: LenstreatmentService,
        private _fb: FormBuilder,
        private utility: UtilityService,
        private dropdownService: DropdownService,
        private error: ErrorService,

        // private _DropdownsPipe: DropdownsPipe
    ) {

    }


    ngOnInit() {
        this.AdvanceSearchLensTreatmentForm = this._fb.group(this.SearchformObj);
        // this.Clear();
        this.LoadLenstreatmentdetailsAPI();
        // this.LensTreatments = [];
        // this.lensTreatmentslist = [];
        // this.LensTreatments = this._DropdownsPipe.treatCategoryDropDown();
        // this.lensTreatmentslist = this._DropdownsPipe.treatCategoryDropDown();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
    }

    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];
    //             this.locations.push({ Id: '', Name: 'Select Category' });
    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }
    /**
     * Loads lenstreatmentdetails api
     * @returns categories details
     */
    LoadLenstreatmentdetailsAPI() {
        this.LensTreatments = this.dropdownService.lensTreatments;
        if (this.utility.getObjectLength(this.LensTreatments) === 0) {
            this.dropdownService.getLensCategories().subscribe(
                (Values: TreatmentCategory) => {
                    try {
                        const dropData = Values.data;
                        this.LensTreatments.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.LensTreatments.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                        }
                        this.dropdownService.lensTreatments = this.LensTreatments;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    
    }
    advanceLensTreatmentSearch() {
        const form = this.AdvanceSearchLensTreatmentForm.value;
        this.lensAdvFilter.emit(form);
        // this.lensTreatmentService.advanceSearchObjLenstreatment.next(form);
    }

    Clear() {
        this.AdvanceSearchLensTreatmentForm = this._fb.group(this.SearchformObj);
    }
}


