import { Component, OnInit, ViewChild, OnDestroy, EventEmitter, Output } from '@angular/core';
// import { LensTreatmentService } from '../../../ConnectorEngine/services/lenstreatments.service';
// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';
import { Subscription } from 'rxjs';
import { InventoryService } from '@services/inventory/inventory.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { ErrorService } from '@app/core/services/error.service';


@Component({
    selector: 'app-lenstreatment-reactivate',
    templateUrl: 'lenstreatment-reactivate.component.html'
})
export class LenstreatmentReactivateComponent implements OnInit, OnDestroy {
    cols: any[];
    gridlists: any[];
    reActivateItems: boolean;
    lensactivateMultipleSelection = [];
    activatedReqbody: any;
    reactiverefreshdetails: Subscription;
    @ViewChild('dtreactive') public dtreactive: any;
    @Output() lensReactivate = new EventEmitter<any>();
    constructor(
        private lensTreatmentService: LenstreatmentService,
        private _InventoryService: InventoryService,
        private error: ErrorService) {
        // this.reactiverefreshdetails = lensTreatmentService.loadLenstreatmentReactive$.subscribe(data => {
        //     this.dtreactive.selection = false;
        //     this.GetLensTreatmentDetails();
        // });
    }
    ngOnInit() {
        this.cols = [
            // { field: 'active', header: 'Active' },
            // { field: 'location', header: 'Location' },
            { field: 'itemId', header: 'ID' },
            { field: 'Category', header: 'Category' },
            { field: 'Name', header: 'Name' },
            { field: 'UPC', header: 'UPC' }
        ];
        this.dtreactive.selection = false;
        this.GetLensTreatmentDetails();
    }
    ngOnDestroy(): void {
        // this.reactiverefreshdetails.unsubscribe();
    }
    GetLensTreatmentDetails(Id = '', Name = '', upc = '', categoryId = '', maxRecord = '', offset = '') {
        this.gridlists = [];
        this.dtreactive.selection = false;
        this.lensTreatmentService.getLensTreatmentReactivateList(Id, Name, upc, categoryId, maxRecord, offset).subscribe(
            data => {
                for (let i = 0; i <= data['result']['Optical']['LensTreatment'].length - 1; i++) {
                    data['result']['Optical']['LensTreatment'][i]['itemId'] =
                        parseInt(data['result']['Optical']['LensTreatment'][i]['itemId'], 10);
                }
                this.gridlists = data['result']['Optical']['LensTreatment'];
            });

    }
    Cancel() {
        this.lensactivateMultipleSelection = [];
        this.lensReactivate.emit('lensClose');
        // this.lensTreatmentService.cancelLenstreatmentReactive.next('value');
    }
    Activate() {
        this.lensTreatmentService.errorHandle.msgs = [];
        if (this.lensactivateMultipleSelection.length === 0) {
            this.error.displayError({
                severity: 'info',
                summary: 'Warning Message', detail: 'Please select item.'
            });
            return;
        }

        this.activatedReqbody = [];
        this.lensactivateMultipleSelection.forEach(element => {
            this.activatedReqbody.push(element.itemId);
        });
        const activatedata = {
            'itemId': this.activatedReqbody
        };

        this._InventoryService.ActivateInventory(activatedata).subscribe(data => {
            this.gridlists = [];
            this.lensactivateMultipleSelection = [];
            this.error.displayError({
                severity: 'success',
                summary: 'Success Message', detail: 'Item Reactivated Successfully'
            });
            this.GetLensTreatmentDetails();
            this.lensReactivate.emit('');
            // this.lensTreatmentService.cancelLenstreatmentReactive.next('value');

            // this._router.navigate(['frames']);
        },
            error => {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Not able to Reactivated Item.'
                });

            }
        );

    }
    // SuccessShow(messagesucess) {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: messagesucess });
    // }
    // ErrorShow(message = '') {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: message });
    // }

    // ConflictShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: message });
    // }
}

