import { Component, OnInit, ViewChild } from '@angular/core';
import { SortEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { ErrorService } from '@app/core/services/error.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { DeactivateComponent } from '../deactivate/deactivate.component';
import { ImportCsvComponent } from '../import-csv/Import-csv.component';
import { LazyLoadEvent } from 'primeng/api';
import { TreatmentCategory } from '@app/core/models/masterDropDown.model';
// import { FramesService } from '@app/core/services/inventory/frames.service';


@Component({
    selector: 'app-inventory-lenstreatment',
    templateUrl: 'inventory-lenstreatment.component.html'
})
export class InventoryLensTreatmentComponent implements OnInit {


    addLoctionLensTreatmentType = false;

    display = false;

    @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;

    @ViewChild('deactivateRow') deactivateRows: DeactivateComponent;

    rowData: any[];

    lenstreatmentcolumns: any[];

    visible = true;

    sortallinventory = false;

    lenstreatmentsimport = false;

    pricecalculation = false;

    LenstreatmentReActivate = false;

    lenstreatmentGridlist: any[];

    searchValueID = '';

    searchValueName = '';

    searchValueVSPCode = '';

    searchValueUnit = '';

    searchValueCategory = '';

    searchValueUPC = '';

    inventoryLenstreatmentAdvancedFilter = false;

    dataviewvisible = false;

    public tableHeaderFilters = [];

    samplevisible = true;

    addlensTreatmentLabs = false;

    lensTreatMultipleSelection = [];

    public Lenscategory: any[];


    @ViewChild('dt') public dt: any;

    @ViewChild('importcsvBox') importCsvdialog: ImportCsvComponent;

    subscriptionLensTreatmentCancleObj: Subscription;

    subscriptionLensTreatmentReactive: Subscription;

    subscriptionCSV: Subscription;

    locationItemId: any;

    subscriptionAddLocation: any;

    totalRecords: any;

    paginationPage: any = 0;

    paginationValue: any = 0;

    selectedRowindex: number;

    reqbodysearch: any;
    /**
     * Addbarcodelabels  is boolean variable
     * showing pop up messages
     */
    addbarcodelabels:boolean;


    ngOnInit() {
        this.addbarcodelabels = false;
        this.LoadMasters();
        this._InventoryCommonService.InventoryType = 'LensTreatment';

    }


    ngOnDestroy(): void {

        // this.subscriptionCSV.unsubscribe();

    }


    constructor(private lensTreatmentService: LenstreatmentService,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private dropdownService: DropdownService,
    ) {



    }


    importcsv() {
        this.importCsvdialog.importCsvbox('lens');
    }


    LoadMasters() {
        this.loadlenstreatmentcolumns();
        this.LoadLenstreatmentcategorylist();
        this.GetLensTreatmentDetails();

    }


    sortallevent(event) {
        if (event === 'ok') {
            this.sortallinventory = false;
        }
    }


    dialogBox() {
        this.dataviewvisible = true;
    }


    deleteRows() {
        // this.DeleteRows.dialogBox();
    }


    deActive() {
        if (this.lensTreatMultipleSelection.length !== 0) {
            const lensdata = {
                'LensSelection': this.lensTreatMultipleSelection
            };
            this.deactivateRows.deactivate(lensdata);
        }

    }


    LoadLenstreatmentcategorylist() {
        this.Lenscategory = this.dropdownService.lensTreatments;
        if (this.utility.getObjectLength(this.Lenscategory) === 0) {
            this.dropdownService.getLensCategories().subscribe(
                (Values: TreatmentCategory) => {
                    try {
                        const dropData = Values.data;
                        this.Lenscategory.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.Lenscategory.push({ Id: dropData[i].id, Name: dropData[i].categoryname });
                        }
                        this.dropdownService.lensTreatments = this.Lenscategory;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

   


    onRowSelect(event) {
        this.lensTreatmentService.editLensTreatmentid = event.data.id;
        this._InventoryCommonService.FortrasactionItemID = event.data.id;
    }

    onRowUnselect() {
        this.lensTreatmentService.editLensTreatmentid = '';
        this._InventoryCommonService.FortrasactionItemID = '';

    }

    loadlenstreatmentcolumns() {
        this.lenstreatmentcolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'name', header: 'Name', visible: this.visible },
            { field: 'unitmeasure', header: 'Unit', visible: this.visible },
            { field: 'category', header: 'Category', visible: this.visible },
            { field: 'upc_code', header: 'UPC', visible: this.visible }

        ];
    }


    duplicateLensTreatment() {
        this.lensTreatmentService.errorHandle.msgs = [];
        this._InventoryCommonService.multiLocationForOtherInventory = [];
        if (this.lensTreatMultipleSelection.length === 1) {
            this.lensTreatmentService.editLensTreatmentid = this.lensTreatMultipleSelection[0].id;
            this._InventoryCommonService.multiLocationForOtherInventory = this.lensTreatMultipleSelection[0].id;
            this.lensTreatmentService.duplicate = 'duplicate'
            this.addlensTreatmentLabs = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select One Item.'
            });



        }
        this.lensTreatMultipleSelection = [];
    }


    /**
     * Adds lens treatmentclick
     * removed the condition because here in the add clcik we are making the multiseelction as empty
     *Ilocation item id should only the particular selected item
     */
    addLensTreatmentclick() {
        this._InventoryCommonService.FortrasactionItemID = '';
        this.locationItemId = undefined;
        this.lensTreatMultipleSelection = [];
        this.dt.selection = false;
        this.addlensTreatmentLabs = true;
        this.lensTreatmentService.editLensTreatmentid = '';
        this._InventoryCommonService.FortrasactionItemID = '';
        this.lensTreatmentService.duplicate = '';


    }


    updateLensTreatment() {
        this.lensTreatmentService.errorHandle.msgs = [];      
        this._InventoryCommonService.FortrasactionItemID = this.lensTreatMultipleSelection[0].id;
        if (this.lensTreatMultipleSelection.length === 1) {
            this.locationItemId = this.lensTreatMultipleSelection[0].id;
        } else {
            this.locationItemId = undefined;
        }

        if (this.lensTreatMultipleSelection.length === 1) {
            this.lensTreatmentService.duplicate = '';
            this.lensTreatmentService.editLensTreatmentid = this.lensTreatMultipleSelection[0].id;
            const selectedlenstreatementid = this.lensTreatmentService.editLensTreatmentid;


            if (!selectedlenstreatementid) {

                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Please Select Lens Treatment.'
                });




            } else {

                this.addlensTreatmentLabs = true;
            }
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select one Item.'
            });


        }
    }


    doubleClickLensTreatment(rowData) {
        this._InventoryCommonService.FortrasactionItemID = rowData.id;
        this.lensTreatmentService.editLensTreatmentid = rowData.id;
        this.lensTreatMultipleSelection = [];
        this.lensTreatMultipleSelection.push(rowData);
        this.addlensTreatmentLabs = true;
        this.lensTreatMultipleSelection = this.lensTreatMultipleSelection;
        this.lensTreatmentService.duplicate = '';
        this._InventoryCommonService.multiLocationForOtherInventory = [];


    }


    loadlenstreatmentbypage(event: LazyLoadEvent) {
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        this.GetlenstreatmentByPaginator(page);
    }


    Applyfilterforlenstreatment() {
        const Lenstreatment = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "5"
                },
                {
                    field: "id",
                    operator: 'LIKE',
                    value: '%' + this.searchValueID + '%'
                },
                {
                    field: "upc_code",
                    operator: 'LIKE',
                    value: '%' + this.searchValueUPC + '%'
                },
                {
                    field: "categoryid",
                    operator: "=",
                    value: this.searchValueCategory
                },
                {
                    field: "unitmeasure",
                    operator: "=",
                    value: this.searchValueUnit
                },
                {
                    field: "name",
                    operator: 'LIKE',
                    value: '%' + this.searchValueName + '%'
                },
                {
                    field: "del_status",
                    operator: '=',
                    value: '0'
                }
            ],


        };
        const resultData = Lenstreatment.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.reqbodysearch = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }

            ]

        }

    }


    GetlenstreatmentByPaginator(page: number = 0) {
        this.Applyfilterforlenstreatment();
        let lensdata: any = []
        this._InventoryCommonService.getInventoryBypage(this.reqbodysearch, page).subscribe(
            data => {
                lensdata = data;
                for (let i = 0; i <= lensdata.data.length - 1; i++) {

                    if (lensdata.data[i].categoryid > 0) {
                        //ar categorydescription = this.getcategorydatafromarray(lensdata.data[i].categoryid);

                        let cateogry: any[] = this.Lenscategory.filter(x => x.Id === lensdata.data[i].categoryid)

                        if (cateogry.length > 0) {
                            lensdata.data[i].category = cateogry[0].Name;
                        }



                    }
                }
                this.lenstreatmentGridlist = lensdata.data;
            },
        );
    }


    getcategorydatafromarray(id) {
        return this.Lenscategory.filter(x => x.id === id);
    }


    GetLensTreatmentDetails() {
        let lensdata: any = []
        this.lenstreatmentGridlist = [];
        this.Applyfilterforlenstreatment();
        this._InventoryCommonService.Getinventorydetailsbyfilter(this.reqbodysearch).subscribe(
            data => {
                lensdata = data;
                for (let i = 0; i <= lensdata.data.length - 1; i++) {

                    if (lensdata.data[i].categoryid > 0) {
                        //ar categorydescription = this.getcategorydatafromarray(lensdata.data[i].categoryid);

                        let cateogry: any[] = this.Lenscategory.filter(x => x.Id === lensdata.data[i].categoryid)

                        if (cateogry.length > 0) {
                            lensdata.data[i].category = cateogry[0].Name;
                        }



                    }
                }
                this.totalRecords = lensdata.total;
                this.lenstreatmentGridlist = lensdata.data;
            });
    }


    /**
     * Advances filter
     * @returns display the filter values details
     */
    advanceFilter(data) {
        let filterdata: any = [];
        filterdata = data;
        this.clearsearchvalues();
        this.searchValueName = filterdata.Name == null ? filterdata.Name = '' : filterdata.Name;
        this.searchValueCategory = filterdata.categories == null ? filterdata.categories = '' : filterdata.categories;
        this.searchValueUPC = filterdata.upc == null ? filterdata.upc = '' : filterdata.upc;
        this.searchValueID = filterdata.Id == null ? filterdata.Id = '' : filterdata.Id;
        this.lenstreatmentGridlist = [];
        this.GetLensTreatmentDetails();
        this.inventoryLenstreatmentAdvancedFilter = false;
    }

    Filtervalues() {
        this.lenstreatmentGridlist = [];
        this.dt.reset();
        this.GetLensTreatmentDetails();
    }

    advancedFilterSidebarClick() {
        this.inventoryLenstreatmentAdvancedFilter = true;
    }
    lenstreatmentsimportSet() {
        this.lenstreatmentsimport = true;
    }

    checkBoxClick(item, e) {

        if (item === 'selectAll') {
            this.visible = e.checked;
            this.loadlenstreatmentcolumns();
            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    applyDataViewChanges(item, selected) {
        this.lenstreatmentcolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;
            }
        });
        const colSpanVar = this.lenstreatmentcolumns.filter(x => x.visible).length;
        if (colSpanVar < this.lenstreatmentcolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }
    }
    headerFilters(value, field, type = 'in') {
        this.dt.filter(value, field, type);
        setTimeout(() => {
            this.tableHeaderFilters = Object.keys(this.dt.filters);
        }, 500);

    }

    refreshTable(table) {
        table.reset();
        this.applyDataViewChanges(null, null);
        this.samplevisible = true;
        this.visible = true;
        this.clearsearchvalues();
        this.loadlenstreatmentcolumns();
        this.GetLensTreatmentDetails();
        this.lensTreatMultipleSelection = [];
        this.paginationValue = 0;
        this.lensTreatmentService.duplicate = '';
    }

    clearsearchvalues() {
        this.searchValueID = '';
        this.searchValueName = '';
        this.searchValueVSPCode = '';
        this.searchValueUnit = '';
        this.searchValueCategory = '';
        this.searchValueUPC = '';
    }
    LenstreatmentReActi() {
        this.LenstreatmentReActivate = true;
    }
    pricecalculationSet() {
        this.pricecalculation = true;
    }
    addLocatiionLensTreatment() {

        this._InventoryCommonService.InventoryType = 'LensTreatment';
        this.lensTreatmentService.errorHandle.msgs = [];
        this._InventoryCommonService.multiLocationForOtherInventory = [];
        if (this.lensTreatMultipleSelection.length === 1) {            
            this._InventoryCommonService.multiLocationForOtherInventory = this.lensTreatMultipleSelection[0].id;;
            this.addLoctionLensTreatmentType = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select  Only One LensTreatment'
            });


        }
    }

    priceCalClose() {
        this.pricecalculation = false;
    }

    customSort(event: SortEvent) {
        this.dt.first = this.paginationValue;
        const tmp = this.lenstreatmentGridlist.sort((a: any, b: any): number => {
            if (event.field) {
                return a[event.field] > b[event.field] ? 1 : -1;
            }
        });
        if (event.order < 0) {
            tmp.reverse();
        }
        const thisRef = this;
        this.lenstreatmentGridlist = [];
        tmp.forEach(function (row: any) {
            thisRef.lenstreatmentGridlist.push(row);
        });
    }
    LensDeactiveevent(event) {
        if (event === 'Deactive') {
            this.lensTreatMultipleSelection = [];
            this.GetLensTreatmentDetails();
        }
    }
    lensLocationsEvent(event) {
        if (event === 'lens') {
            this.addLoctionLensTreatmentType = false;
        } else {
            this.addLoctionLensTreatmentType = false;
            // this.GetLensTreatmentDetails();
        }
    }
    lensReactivateEvent(event) {
        if (event === 'lensClose') {
            this.LenstreatmentReActivate = false;
        } else {
            this.GetLensTreatmentDetails();
            this.LenstreatmentReActivate = false;
        }
    }
    lensAdvFilterEvent(event) {
        this.advanceFilter(event);
    }


     /**
     * Singles grid value
     * in thsi fuction we are assigning the updated column to the
     *existing grid data without refresh * 
     * @param element is object which having the itemid an can match and pushing the data.
     */
    singleGridValue(element) {

        let catergory:string;
        if (element.griddata[0].categoryid > 0) {           

            let cateogry: any[] = this.Lenscategory.filter(x => x.Id === element.griddata[0].categoryid)

            if (cateogry.length > 0) {
                catergory= cateogry[0].Name;
            }
        }        

        if (element.status !== '') {
            this.lenstreatmentGridlist.forEach(data => {
                if (data.id === element.griddata[0].id) {
                    data.id = element.griddata[0].id,
                    data.name= element.griddata[0].name,
                    data.vsp_code= element.griddata[0].vsp_code,
                    data.locationId= element.griddata[0].locationId,
                    data.unitmeasure= element.griddata[0].unitmeasure,
                    data.categoryid= element.griddata[0].categoryid,  
                    data.category  = catergory;            
                    data.upc_code= element.griddata[0].upc_code,
                    data.retail_price= element.griddata[0].retail_price,
                    data.profit= element.griddata[0].profit,
                    data.wholesale_cost= element.griddata[0].wholesale_cost,
                    data.inventory= element.griddata[0].inventory,
                    data.send_to_lab_flag= element.griddata[0].send_to_lab_flag,
                    data.print_on_order= element.griddata[0].print_on_order,
                    data.structure_id= element.griddata[0].structure_id,
                    data.modifier_id= element.griddata[0].modifier_id,       
                    data.commission_type_id= element.griddata[0].commission_type_id,
                    data.amount= element.griddata[0].amount,
                    data.gross_percentage= element.griddata[0].gross_percentage,
                    data.spiff= element.griddata[0].spiff,
                    data.notes= element.griddata[0].notes,
                    data.module_type_id= element.griddata[0].module_type_id
                }
            });
        }
        
    }


    /**
     * Lens add item event
     * Here in this method new record then we are going to refresh the grid
     *updating the grid without refresh when item id is present.
    
     */
    lensAddItemEvent(data) {      
        if (data.status == 'new') {
            this.addlensTreatmentLabs = false;
            this.GetLensTreatmentDetails();           
        }
        if (data.status == 'update') {
            this.addlensTreatmentLabs = false;
            for (let i = 0; i <= this.lenstreatmentGridlist.length - 1; i++) {
                if (this.lenstreatmentGridlist[i].id == data.griddata[0].id) {
                    this.singleGridValue(data);
                   
                }

            }

        }

    }

    /**
     * Addbarcodelabels set
     */
    addbarcodelabelsSet() {
        if (this.lensTreatMultipleSelection.length !== 0) {
            this._InventoryCommonService.barCodelist = this.lensTreatMultipleSelection;
            this._InventoryCommonService.barCodeLable = 'LensTreatmentBarcode';
        } else {
            this._InventoryCommonService.barCodelist = [];
            this._InventoryCommonService.barCodeLable = 'LensTreatmentBarcode';
        }
        this.addbarcodelabels = true;
    }
}
