import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import {
    CommissionStructure,
    CommissionType,
    FrameSupplier,
    VcpDropDown,
    LocationsMaster,
    TreatmentCategory,
} from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-inventory-lenstreatment',
    templateUrl: 'add-inventory-lenstreatment.component.html'
})

export class AddInventoryLensTreatmentComponent implements OnInit {

    valueforitem: any;
    LensTreatmentForm: FormGroup;
    lensprocedureform: FormGroup;
    Accesstoken = '';
    postlensTreatment: any;
    postdata: any;
    private locationlist: any[];
    locations: Array<LocationsMaster>;
    public LensTreatments: any[];
    public lensTreatmentslist: any;
    addLensTreatmentInvTransaction = false;
    transactionsBtnHide = false;
    procedureCodeSearch = false;
    LenstreatmentsubscriptionStockDetailsUpdate: Subscription;
    transacctionStockDetails: any = [];
    StockDetails: any = [];
    UpdateStock: any;
    lenstreatmentUpdateGridlist: any[];
    vcpDropData: any[] = [];
    TypeDropData: any[] = [];
    StructureDropData: any[] = [];
    @Input() locationItem: any;
    locationGridData: any[] = [];
    locationNameData: any = [];
    FilteredTrasaction: any = [];
    test: any[] = [{ value: 'word1' }];
    cptFromData: any;
    indexvalue: number;
    @Output() lensAddItem = new EventEmitter<any>();
    locationsList: any;
    /**
     * Suppliers  of add inventory lens treatment component
     */
    suppliers: Array<object>;
    supplierslist: any;
    reqbodysearch: any;
    cptCodeDesc: any;
    LensTreatmentTrasactionItemId: string;
    constructor(
        private _fb: FormBuilder,
        private _router: Router,
        private lensTreatmentService: LenstreatmentService,
        private frameService: FramesService,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private dropdownservice: DropdownService,
        private utility: UtilityService,
        private dropdownService: DropdownService) {


    }

    ngOnDestroy(): void {

    }

    ngOnInit() {
        this.loadDropDowns();
        this.LoadLenscategories();
        this.Loadformdata();
        this.loadinventoryloactions();
    }



    Loadformdata() {
        this.LensTreatmentTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        this.Accesstoken = ''
        this.LensTreatmentForm = this._fb.group({
            name: ['', [Validators.required]],
            vsp_code: '',
            id: '',
            locationId: '',
            upc_code: ['', [Validators.required]],
            unitmeasure: ['', [Validators.required]],
            retail_price: '',
            categoryid: ['', [Validators.required]],
            profit: '',
            wholesale_cost: '',
            inventory: false,
            send_to_lab_flag: false,
            print_on_order: false,
            structure_id: '',
            modifier_id: '',            
            commission_type_id: '',
            amount: '',
            gross_percentage: '',
            spiff: '',
            notes: '',
            module_type_id: "5",
            procedureCodes: this._fb.array([
                this.initlanguage(),
            ])

        });
        this.GetSupplierDetails();
        const lenstreatmentid = this.lensTreatmentService.editLensTreatmentid ||
        this.lensTreatmentService.editLensTreatmentidDoubleClick;
        this.LoadLenstreatmentdata(lenstreatmentid);
    }


    Applyfilterforlenstreatment(lenstreatmentid) {
        const Lenstreatment = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "5"
                },
                {
                    field: "id",
                    operator: '=',
                    value: lenstreatmentid
                }
            ]
        };
        const resultData = Lenstreatment.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.reqbodysearch = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }

            ]

        }

    }

    /**
     * Loads drop downs
     * @returns vcp data , commision type data and structured data
     */
    loadDropDowns() {
        this.vcpDropData = this._InventoryCommonService.vpcData;
        if (this.utility.getObjectLength(this.vcpDropData) === 0) {
            this._InventoryCommonService.getVCPdropDownData().subscribe((values: VcpDropDown) => {
                try {
                    const dropData = values.data;
                    this.vcpDropData.push({ Id: '', Name: 'Select VCP Vision' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.vcpDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].description });
                    }
                    this._InventoryCommonService.vpcData = this.vcpDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }

            });
        }

        this.TypeDropData = this._InventoryCommonService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this._InventoryCommonService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this._InventoryCommonService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

        this.StructureDropData = this._InventoryCommonService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this._InventoryCommonService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this._InventoryCommonService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }

    }


    LoadLenstreatmentdata(lenstreatmentid) {
        if (lenstreatmentid) {
            this.Applyfilterforlenstreatment(lenstreatmentid);
            let lenstreatmentdata: any = [];
            this._InventoryCommonService.Getinventorydetailsbyfilter(this.reqbodysearch).subscribe(
                data => {
                    lenstreatmentdata = data;
                    this.LensTreatmentForm = this._fb.group({
                        id: lenstreatmentdata.data[0].id,
                        name: [lenstreatmentdata.data[0].name, [Validators.required]],
                        vsp_code: lenstreatmentdata.data[0].vsp_code,
                        locationId: '',
                        unitmeasure: [lenstreatmentdata.data[0].unitmeasure, [Validators.required]],
                        categoryid: [lenstreatmentdata.data[0].categoryid === 0 || lenstreatmentdata.data[0].categoryid === null ? null :
                        lenstreatmentdata.data[0].categoryid.toString(), [Validators.required]],
                        upc_code: [this.lensTreatmentService.duplicate === 'duplicate' ? '': lenstreatmentdata.data[0].upc_code,  [Validators.required]],
                        retail_price: lenstreatmentdata.data[0].retail_price.toFixed(2),
                        profit: '',
                        wholesale_cost:lenstreatmentdata.data[0].wholesale_cost.toFixed(2),
                        inventory: lenstreatmentdata.data[0].inventory === 1 ? true : false,
                        send_to_lab_flag: lenstreatmentdata.data[0].send_to_lab_flag === 1 ? true : false,
                        print_on_order: lenstreatmentdata.data[0].print_on_order === 1 ? true : false,
                        structure_id: lenstreatmentdata.data[0].structure_id === 0 || lenstreatmentdata.data[0].structure_id === null ? '' :
                        lenstreatmentdata.data[0].structure_id,
                        modifier_id: lenstreatmentdata.data[0].modifier_id === 0 || lenstreatmentdata.data[0].modifier_id === null ? '' :
                        lenstreatmentdata.data[0].modifier_id.toString(),       
                        commission_type_id: lenstreatmentdata.data[0].commission_type_id === 0 || lenstreatmentdata.data[0].commission_type_id === null ? '' :
                        lenstreatmentdata.data[0].commission_type_id.toString(),
                        amount: lenstreatmentdata.data[0].amount.toFixed(2),
                        gross_percentage: lenstreatmentdata.data[0].gross_percentage,
                        spiff: lenstreatmentdata.data[0].spiff,
                        notes: lenstreatmentdata.data[0].notes,
                        module_type_id: "5",
                        procedureCodes: this._fb.array([])

                    });
                    let proceduredata: any = [];
                    this.lensTreatmentService.getSavedProceduresById(lenstreatmentid).subscribe((saveData: any[]) => {
                        proceduredata = saveData;
                        if (proceduredata.data != null) {
                            for (let j = 0; j < proceduredata.data.length; j++) {
                                const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes'];
                                this._InventoryCommonService.getCptDesc(proceduredata.data[j].procedure_id).subscribe(data => {
                                    this.cptCodeDesc = data
                                    control.push(
                                        this._fb.group({
                                            id: proceduredata.data[j].id,
                                            procedure_id: proceduredata.data[j].procedure_id,
                                            retailprice: proceduredata.data[j].retailprice,
                                            procedure_code: proceduredata.data[j].procedure_code,
                                            procedure_desc: this.cptCodeDesc.cpt_desc
                                        })
                                    );
                                })                               
                            }
                        }
                    },
                        error => {
                            console.log('Error :: ' + error);
                            const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes'];
                            control.push(this.initlanguage());
                        });
                    this.profitcalculate();
                });


        }

    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    loadinventoryloactions() {
        if (this.locationItem !== undefined) {
            this.locationNameData = this.dropdownservice.inventoryLocation;
            if (this.utility.getObjectLength(this.locationNameData) === 0) {
                this.dropdownservice.getInventtoryLocations().subscribe(data => {
                    this.locationNameData = data['result']['Optical']['Locations'];
                    this.dropdownservice.inventoryLocation = data['result']['Optical']['Locations'];
                    this._InventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                        this.locationGridData = data['data'];
                        this.locationGridData.forEach(element => {
                            for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                if (element.loc_id === this.locationNameData[i].Id) {
                                    element.loc_id = this.locationNameData[i].Name;
                                }
                            }
                        });
                    });
                })
            } else {

                this._InventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                    this.locationGridData = data['data'];
                    this.locationGridData.forEach(element => {
                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                            if (element.loc_id === this.locationNameData[i].Id) {
                                element.loc_id = this.locationNameData[i].Name;
                            }
                        }
                    });
                });
            }

        }
    }

    profitcalculate() {
        if (this.LensTreatmentForm.controls.wholesale_cost.value !== '' || this.LensTreatmentForm.controls.wholesale_cost.value != null ||
            this.LensTreatmentForm.controls.retail_price.value !== '' || this.LensTreatmentForm.controls.retail_price.value != null) {
            const diffvalue = this.LensTreatmentForm.controls.retail_price.value - this.LensTreatmentForm.controls.wholesale_cost.value;
            this.LensTreatmentForm.controls.profit.patchValue(diffvalue.toFixed(2));
        }
        if (this.LensTreatmentForm.controls.wholesale_cost.value === '' && this.LensTreatmentForm.controls.retail_price.value === '') {
            this.LensTreatmentForm.controls.profit.patchValue('');
        }
    }

    LoadLenscategories() {
        this.LensTreatments = this.dropdownService.lensTreatments;
        if (this.utility.getObjectLength(this.LensTreatments) === 0) {
            this.dropdownService.getLensCategories().subscribe(
                (Values: TreatmentCategory) => {
                    try {
                        const dropData = Values.data;
                        this.LensTreatments.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.LensTreatments.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                        }
                        this.dropdownService.lensTreatments = this.LensTreatments;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }


    saveProcedures(itemId, lensprocedureform) {
        const reqbody = [];
        for (let j = 0; j < lensprocedureform.procedureCodes.length; j++) {
            if (lensprocedureform.procedureCodes[j].procedure_id != null) {
                const data = {
                    'procedure_id': parseInt(
                        lensprocedureform.procedureCodes[j].procedure_id, 10),
                    'procedure_code': lensprocedureform.procedureCodes[j].procedure_code,
                    'retailprice': lensprocedureform.procedureCodes[j].retailprice
                };
                reqbody.push(data);
            }
        }
        if (reqbody.length > 0) {
            this.lensTreatmentService.saveLenstreatmentProcedures(reqbody, itemId).subscribe(data => {

            });
        }

    }


    structureformcontrols() {
        this.lensprocedureform = this.LensTreatmentForm.value;
        this.LensTreatmentForm.removeControl('id');
        this.LensTreatmentForm.removeControl('locationId');
        this.LensTreatmentForm.removeControl('status');
        this.LensTreatmentForm.removeControl('profit');
        if (this.LensTreatmentForm.controls.inventory.value === true) {
            this.LensTreatmentForm.controls.inventory.patchValue('1');
        } else {
            this.LensTreatmentForm.controls.inventory.patchValue('0');
        }
        // this.LensTreatmentForm.removeControl('procedureCodes');
    }


    setgriddata( lenstreatmentid,status)
    {

        var filterPayLoad = {
            "filter": [
                {
                    "field": "module_type_id",
                    "operator": "=",
                    "value": "5"
                },
                {
                    "field": "id",
                    "operator": "=",
                    "value":  lenstreatmentid
                }

            ]

        }

        this._InventoryCommonService.Getinventorydetailsbyfilter(filterPayLoad).subscribe(data => {
            const itemValue = {
                'status': status,     
                'griddata': data['data']
            };
                this.lensAddItem.emit(itemValue);                        
           
        })
     
    }


    SaveLensTreatment() {

        this.frameService.errorHandle.msgs = [];
        if (!this.LensTreatmentForm.valid) {
            return;
        }
        this.structureformcontrols();

        try {
            if(this.lensTreatmentService.duplicate === 'duplicate')
            {
                this.lensTreatmentService.editLensTreatmentid=0;
            }
            if (this.lensTreatmentService.editLensTreatmentid > 0) {
                let savePayload = this.LensTreatmentForm.value;
                delete savePayload.procedureCodes;
                this._InventoryCommonService.updateInventorydetails(this.utility.formatWithOutControls(savePayload), this.lensTreatmentService.editLensTreatmentid).subscribe((data: any[]) => {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Success Message', detail: 'LensTreatment Updated Successfully'
                    });
                    try {
                        this.saveProcedures(this.lensTreatmentService.editLensTreatmentid, this.lensprocedureform);
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                    this.setgriddata(this.lensTreatmentService.editLensTreatmentid, 'update');                  
                   
                },
                    error => {
                        this.error.displayError({ severity: 'error', summary: 'Error Message', detail: error.error.Error });

                    });
            }
            else {
                let savePayload = this.LensTreatmentForm.value;
                delete savePayload.procedureCodes;
                this._InventoryCommonService.Saveinventorydetails(this.utility.formatWithOutControls(savePayload)).subscribe((data: any[]) => {

                    const key = "item_Id";
                    this.lensTreatmentService.editLensTreatmentid = data[key];
                    try {
                        this.saveProcedures(this.lensTreatmentService.editLensTreatmentid, this.lensprocedureform);
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Success Message', detail: 'LensTreatment Saved Successfully'
                    });

                    this.setgriddata(this.lensTreatmentService.editLensTreatmentid, 'new');
                    
                },
                    error => {
                        this.error.displayError({ severity: 'error', summary: 'Error Message', detail: error.error.Error });
                    });
            }
                } catch (error) {
            this.error.syntaxErrors(error);
        }


    }




    addLensTreatmentTransaction() {
        if(  this.LensTreatmentTrasactionItemId !== '') {
            this.addLensTreatmentInvTransaction = true;
        }
        else{
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Select any item or save new item for saving transactions'
            });
            this.addLensTreatmentInvTransaction = false;
        }
    }
    TrasactionOutput(data) {
        if (data === 'cancle') {
            this.addLensTreatmentInvTransaction = false;
        } else {
            this.UpdateStock = data;
            this.addLensTreatmentInvTransaction = false;
            this.filterTransactionOnLocation();
        }
    }
    onLensTreatmentTabsClick(event) {
        if (event.index === 0) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 1) {
            this.transactionsBtnHide = false;

        }
        if (event.index === 2) {
            this.transactionsBtnHide = true;
            this.filterTransactionOnLocation();
        }
    }

    getLensTreatmentStockDetails() {
        this.frameService.getstockDetail().subscribe(data => {
            for (let i = 0; i <= data['data'].length - 1; i++) {
                console.log(data['data'][i]['loc_id']);
                const loca = this.locationlist.filter(x => x.Id === data['data'][i]['loc_id']);
                console.log(loca);
                data['data'][i]['loc_id'] = loca[0]['Name'];
                data['data'][i]['trans_type'] = data['data'][i]['transaction_type']['transactiontype'];
            }
            this.StockDetails = data['data'];
            this.transacctionStockDetails = this.StockDetails;           
        });

    }
    salesApi() {
        // alert("sales")
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] > 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }
    trasactionApi() {
        // alert("transaction")
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] >= 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }
    filterTransactionOnLocation() {
        // alert(locationData)
        this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];
        if (this._InventoryCommonService.FortrasactionItemID === '') {
            this.transacctionStockDetails = [];
        } else {
            const data = {
                filter: [
                    {
                        field: 'item_id',
                        operator: '=',
                        value: this._InventoryCommonService.FortrasactionItemID,
                    },
                ]
            };
            this.frameService.filterTrasactionData(data).subscribe(filterData => {
                for (let i = 0; i <= filterData['data'].length - 1; i++) {
                    filterData['data'][i]['trans_type'] = filterData['data'][i]['transaction_type']['transactiontype'];

                }
                this.StockDetails = filterData['data'];
                this.transacctionStockDetails = this.StockDetails;
            });
        }
    }
    filterTransactionOnLocationBased() {
       this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];
        if (this._InventoryCommonService.FortrasactionItemID !== '') {
            if (this.LensTreatmentForm.controls['locationId'].value === '') {
                this.filterTransactionOnLocation();
            } else {
                const data = {
                    filter: [
                        {
                            field: 'item_id',
                            operator: '=',
                            value: this._InventoryCommonService.FortrasactionItemID,
                        },
                        {
                            field: 'loc_id',
                            operator: '=',
                            value: this.LensTreatmentForm.controls['locationId'].value,
                        }
                    ]
                };
                this.frameService.filterTrasactionData(data)
                    .subscribe(filterData => {
                        this.FilteredTrasaction = filterData['data'];
                        this.StockDetails = this.FilteredTrasaction;
                        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
                            this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];

                        }
                        this.transacctionStockDetails = this.StockDetails;
                    });
            }
        } else {
            this.transacctionStockDetails = [];
        }
    }
    keyPress(event: any) {
        const pattern = /[0-9]/;

        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    // words2 = [{value: 'word1'}, {value: 'word2'}, {value: 'word3'}, {value: ''}];
    initlanguage() {
        return this._fb.group({
            id: [],
            procedure_id: [],
            retailprice: [],
            procedure_code: [],
            procedure_desc: []
        });
    }
    add() {
        const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes'];
        control.push(this.initlanguage());
        // this.LensTreatmentForm.controls['words2'].value.push({value: 'gsre'});
    }
    removeLanguage(i: number) {
        const id = this.LensTreatmentForm.controls['procedureCodes']['controls'][i].controls.id.value;
        if (id !== '' || id != null) {
            const lenstreatmentid = this.lensTreatmentService.editLensTreatmentid ||
                this.lensTreatmentService.editLensTreatmentidDoubleClick;
            //  localStorage.getItem('editLensTreatmentid')
            this.lensTreatmentService.deleteSavedProceduresById(lenstreatmentid, id).subscribe(data => {

            });
            const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes'];
            control.removeAt(i);
        } else {
            const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes'];
            control.removeAt(i);
        }

    }

    procedureCodeClick(index) {
        this.indexvalue = index;
        const control = <FormArray>this.LensTreatmentForm.controls['procedureCodes']['controls'][0];
        // control.push(this.initlanguage());
        this.procedureCodeSearch = true;
    }
    procedurResult(event) {
        if (event !== 'cancle') {
            this.cptFromData = event;
            if (this.cptFromData !== '') {
                const id = this.LensTreatmentForm.controls['procedureCodes']['controls'][this.indexvalue]['controls'].id.value;
                if (id === '' || id == null) {
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].id.patchValue('');
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_id.patchValue(this.cptFromData.cpt_fee_id);
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_code.patchValue(this.cptFromData.cpt4_code);
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].retailprice.patchValue('0.00');
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_desc.patchValue(this.cptFromData.cpt_desc);
                } else {
                    const lenstreatmentid = this.lensTreatmentService.editLensTreatmentid ||
                        this.lensTreatmentService.editLensTreatmentidDoubleClick;                 
                    this.lensTreatmentService.deleteSavedProceduresById(lenstreatmentid, id).subscribe(data => {

                    });
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].id.patchValue('');
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_id.patchValue(this.cptFromData.cpt_fee_id);
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_code.patchValue(this.cptFromData.cpt4_code);
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].retailprice.patchValue('0.00');
                    this.LensTreatmentForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_desc.patchValue(this.cptFromData.cpt_desc);
                }
                this.procedureCodeSearch = false;
            } else {
                this.procedureCodeSearch = false;
            }
        } else {
            this.procedureCodeSearch = false;
        }


    }
   

    /**
     * Get supplier details
     * @returns supplier details
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }

}


