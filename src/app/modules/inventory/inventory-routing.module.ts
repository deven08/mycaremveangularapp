import { CommonModule } from '@angular/common';
import { InventoryBarcodelabelsComponent } from './barcode-labels/inventory-barcodelabels.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {
  AddModifyComponent,
  BarCodeLabelsComponent,
  ContactLensComponent,
  FrameComponent,
  InventoryDetailsComponent,
  InventoryLensTreatmentComponent,
  InventoryOthersComponent,
  InventoryPurchaseOrderComponent,
  InventoryServicesComponent,
  LensOnDemandComponent,
  LensTreatmentsImportComponent,
  PhysicalCountComponent,
  PriceCalculationComponent,
  ReceivingInventoryComponent,
  SpectacleLensComponent,
  SortAllInventoryComponent
} from './';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'frames',
        component: FrameComponent,
        data: { 'title': 'Frames  Inventory', 'banner' : false }

      },
      {
        path: 'contactlens',
        component: ContactLensComponent,
        data: { 'title': 'Contactlens  Inventory', 'banner' : false }
      },
      {
        path: 'spectaclelens',
        component: SpectacleLensComponent,
        data: { 'title': 'Spectaclelens  Inventory', 'banner' : false }
      },
      {
        path: 'others',
        component: InventoryOthersComponent,
        data: { 'title': 'Others  Inventory', 'banner' : false }
      },
      {
        path: 'services',
        component: InventoryServicesComponent,
        data: { 'title': 'Services  Inventory', 'banner' : false }
      },
      {
        path: 'inventory-details',
        component: InventoryDetailsComponent,
        data: { 'title': 'Inventory details  Inventory', 'banner' : false }
      },
      {
        path: 'physical-counts',
        component: PhysicalCountComponent,
        data: { 'title': 'Physical counts Inventory', 'banner' : false }
      },
      {
        path: 'addbarcodelabels',
        component: BarCodeLabelsComponent,
        data: { 'title': 'Addbarcodelabels Inventory', 'banner' : false }
      },
      {
        path: 'lenstreatmentsimport',
        component: LensTreatmentsImportComponent,
        data: { 'title': 'Lenstreatmentsimport Inventory', 'banner' : false }
      },
      {
        path: 'priceCalculation',
        component: PriceCalculationComponent,
        data: { 'title': 'PriceCalculation  Inventory', 'banner' : false }
      },
      {
        path: 'purchase-order',
        component: InventoryPurchaseOrderComponent,
        data: { 'title': 'Purchase Order Inventory', 'banner' : false }
      },
      {
        path: 'lens-treatment',
        component: InventoryLensTreatmentComponent,
        data: { 'title': 'Lens Treatment  Inventory', 'banner' : false }
      },
      {
        path: 'barcode-labels',
        component: InventoryBarcodelabelsComponent,
        data: { 'title': 'Barcode Labels  Inventory', 'banner' : false }
      },
      {
        path: 'receiving',
        component: ReceivingInventoryComponent,
        data: { 'title': 'Receiving  Inventory', 'banner' : false }
      },
      {
        path: 'Lensondemand',
        component: LensOnDemandComponent,
        data: { 'title': 'Lensondemand  Inventory', 'banner' : false }
      },
      {
        path: 'sortallinventory',
        component: SortAllInventoryComponent,
        data: { 'title': 'Sortallinventory Inventory', 'banner' : false }
      },
      {
        path: 'add-modify-frames',
        component: AddModifyComponent,
        data: { 'title': 'Add-Modify Frames  Inventory', 'banner' : false }
      }
    ],
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class InventoryRoutingModule { }
