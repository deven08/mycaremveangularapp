// Core Modules
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

// Models
// import { LazyLoadEvent } from './../../../shared/models/prime-datatable/lazyloadevent';
// import { ImportFile } from './../../../ConnectorEngine/models/physical-count/importFile.model';

// Service
// import { PhysicalCountService } from '../../../ConnectorEngine/services/physical-count.service';
import { PhysicalcountService } from '@services/inventory/physicalcount.service';
import { ErrorService } from '@app/core/services/error.service';
import { ImportFile } from '@app/core/models/importFile.model';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
  selector: 'app-physical-import-file',
  templateUrl: 'physical-count-import-file.component.html'
})

export class PhysicalCountImportFileComponent implements OnInit {

  // Import Form
  importForm: FormGroup;

  // Active Batch
  @Input() batch: number;

  // progressFor Importing files
  progressBar: boolean;

  // Files
  files: ImportFile[];

  // Files Data
  fileData: any = [];

  // counItems
  countItems: number;

  // For prime ng turbotable events
  @ViewChild('dt') public dt: any;

  // totalItems
  total: number;

  // Active File Id
  activeFileId: number;

  // Loading for Lazy Load
  loading: boolean;

  // File Items Data
  items: Array<any> = [];

  // Temporary Data Handle
  tempData: any = [];

  constructor(private _fb: FormBuilder, private service: PhysicalcountService, private error: ErrorService) {
    this.progressBar = false;
    this.countItems = 0;
    this.total = 0;

    // Setting File Id to 0
    this.activeFileId = 0;
  }

  // Intialize Component
  ngOnInit() {
    // Intialize Form
    this.importFormIntialize();

    // Assigning variables empty
    this.files = [];
  }

  // Intialize Form
  importFormIntialize() {
    this.importForm = this._fb.group({
      'fileToUpload' : '',
      'selectedFiles' : new FormControl()
    });
  }

  // Valid Batch
  validBatch() {
    if (this.batch > 0) {
      return true;
    } else {
      return false;
    }
  }

  // Load File Again
  loadFileAgain(file: number = 0) {
     // Check File is not zero for blank clicks
     if (file > 0) {

       // Assign File Id Active
       this.activeFileId = file;
       // Load Data related to File
       this.dt.reset();
       this.loadFileDatagrid(this.activeFileId, 0);

     }

  }

  // For Importing File
  importUserFile(event: { target: { files: (string | Blob)[]; }; }) {
    // Empty Array Messages
    this.service.errorHandle.msgs = [];

    // Assigning API Response Variable to handle key errors
    let fileUploaded: any = [];
    // Creating payload for sending in API
    const formData: FormData = new FormData();
    formData.append('file', event.target.files[0]);

    // Calling service to upload fie
    this.service.uploadUserImportFile(formData, this.batch).subscribe(
      data => {
        // Show Progress Bar
        this.progressBar = true;

        // Assigning response to data
        fileUploaded = data;
        // Pushing Data File Data List in Frontend
        this.files.push({
          file_id: ('file_id' in fileUploaded) ? fileUploaded.file_id : '',
          file_name: ('file_name' in fileUploaded) ? fileUploaded.file_name : '',
          total_results: ('total' in fileUploaded) ? fileUploaded.total : ''
        });

        // Upload Message
        if ('status' in fileUploaded) {

          // Assign File Id Active
          this.activeFileId = fileUploaded.file_id;

          // Show Message for success results.
          this.error.displayError({ severity: 'success', summary: 'Upload Status', detail: fileUploaded.status });
          // Loading Data in Grid
          this.loadFileDatagrid(fileUploaded.file_id);

          // Resetting Import Button
          this.importForm.controls['fileToUpload'].patchValue('');
        }
      },
      error => {
        this.error.displayError({
          severity: 'error',
          summary: 'Upload Status',
          detail: error.status
        });

        // Disabling Progress BAR
        this.progressBar = false;
      }
    );
  }


  // Loading File Data in Grid
  loadFileDatagrid(fileId: number, page: number = 0) {
    // Checking File Id
    if (fileId > 0) {
      // Get Data from file
      this.service.getFileItems(fileId, page).subscribe(
         data => {
           this.fileData[fileId] = data;
           this.total = this.fileData[fileId].total;
           this.items = this.fileData[this.activeFileId].data;
           this.progressBar = false;
         },
         error => {
          this.error.displayError({ severity: 'success', summary: 'Upload Status', detail: error.status });
         }
      );
    }
  }

  // Paginate and Load data for specific page
  lazyLoadFileItems(event: LazyLoadEvent) {
    // Generating Page
    let page = 0;
    page =  ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
    // Calling Page Data
    this.loadFileDatagrid(this.activeFileId, page);
  }

  // Delete File
  deleteFile() {
    // Get Selected files
    const selectedFiles = this.importForm.controls['selectedFiles'].value;

    // Remaing Files
    const remaining_files: ImportFile[] = [];

    // Deleting files using loop
    selectedFiles.forEach((fileId: any) => {

      // After Deleting left files
      const newFiles =  this.files.filter((p: { file_id: any; }) => p.file_id !== fileId);

      if (newFiles.length > 0) {
        remaining_files.push(newFiles[0]);
      }

      // Delete Data From item list grid
      delete this.fileData[fileId];
    });

    // Assigning Remaining files to file list grid
    this.files = remaining_files;
    // Remove Selected Files from checkbox variable
    this.importForm.controls['selectedFiles'].patchValue('');

    // For Resetting Files Data
    this.resetFilesData();
  }

  // For Resetting Files Data
  resetFilesData() {
    // Remaining File Data
    const remaining_files_Data = [];

    // ReIndex Remaining File Data array
    this.fileData.map((item: any, index: number) => {
      if (item !== null) {
        remaining_files_Data[index] = item;
      }
    });

    // Checking Active File Data Exists to show on data grid.
    if (remaining_files_Data[this.activeFileId] !== undefined) {

      // Resseting table
      this.dt.reset();

      // Resetting Data
      this.items = [];
      this.items = remaining_files_Data[this.activeFileId].data;
      this.total = remaining_files_Data[this.activeFileId].total;
    } else {
      this.activeFileId = 0;
      this.items = [];
      this.total = 0;
    }

    // Resetting File Data List
    this.fileData = [];
    this.fileData = remaining_files_Data;
  }

}
