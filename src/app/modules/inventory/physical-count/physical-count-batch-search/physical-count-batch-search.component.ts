import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-physical-count-batch-search',
  templateUrl: './physical-count-batch-search.component.html',
  styleUrls: ['./physical-count-batch-search.component.css']
})
export class PhysicalCountBatchSearchComponent implements OnInit {

  // Send data to parent screen
  @Output() batchsearchoutput = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  // On Closing
  onClickClose() {
    this.batchsearchoutput.emit('close');
  }
}
