import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { PhysicalCountService } from '../../ConnectorEngine/services/physical-count.service';
import { PhysicalcountService } from '@services/inventory/physicalcount.service';
import { ErrorService } from '@app/core/services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { DialogBoxComponent } from '@shared/components/dialog-box/dialog-box.component';
import { AppService } from '@app/core';



@Component({
    selector: 'app-physical-count',
    templateUrl: 'physical-count.component.html',
    styleUrls: ['physical-count.component.css']
})
export class PhysicalCountComponent implements OnInit {
  // Modal Pop Up confirmation
  className: any = 'dialog';
  showDialog: any = false;
  head_pop: any = 'My Care MVE';
  message_pop = '';
  button_pop: any = 'Ok';
  button_pop2: any = '';
  popFor = '';
  popParams: any[];

  // For blinking grid item.
  id: any = '';

  // For List of Inventory Items.
  inventoryItems: any = [];

  // For physical count complete form including batch and items.
  // physicalCountFormGroup: FormGroup;
  // batchFormGroup: FormGroup;
  physicalCount: FormGroup;
  progressBar = false;
  physicalSearch = false;
  batchSearch = false;
  // For Import Window
  physicalImportFile = false;

  // For Handling Manufactures temporary data while searching
  Manufacturers: any = [];

  // For Handling Brands temporary data while searching
  Brands: any = [];

  // For Manufactures
  // manufacturerlist:any[];
  // brandlist:any[];

  // For Handling Locations
  locations: any[];
  // loadedLocations:any = [];

  // For Handling Quantity for Items
  submitQuantity: any = [];

  // For Manufacturer Suggestions dropdown.
  filteredManufacturers: any[];

  // For Brands Suggestions dropdown
  filteredBrands: any[];

  // For Handling Item Type
  items: any = [];

  // stock_temp:any = [];

  // For Selected Manufacturer
  manufacturer: string;

  // For Selected Brand
  brand: string;

  // For Table Grid Height
  gridHeight: any = '';

  // For Handling Subscription
  itemsSubscription: any = '';

  // For Handling Temporary data
  temp_data: any = [];

  // For Batch Loading Items
  preStock: any = [];

  // Active Batch
  activeBatch: number;

  @ViewChild('grid') targetElement: any;

  @ViewChild('alertpopup') alertpopup: DialogBoxComponent;
  constructor(
    private _fb: FormBuilder,
    private service: PhysicalcountService,
    private error: ErrorService,
    private dropdown:DropdownService,
    public app: AppService
) {}

  ngOnInit() {
    // Active Batch
    this.activeBatch = 0;

    // Intialize Form
    this.physicalCountFormReIntializie();

    // Setting Quantity Null
    this.submitQuantity = [];

    this.gridHeight = ( window.innerHeight - $('.ecl-table').offset().top - $('.order-shipping-bottom-btn').height() ) + 'px';

    // Change Data
    this.changeData();

    // Locations
    this.LoadLocations();

    // Load Item Tyoes
    this.LoadItemTypes();
  }

  // On Data changes
  changeData() {
    // Batch Form Changes
    this.activeBatch = 0;
    this.physicalCount.valueChanges.subscribe(data => {
        this.service.data['batchform'] = data.batchGroup;
        this.activeBatch = (data.batchGroup.batchID !== '') ? data.batchGroup.batchID : 0;
    });
  }

  // Reintialize Form
  physicalCountFormReIntializie() {
    this.physicalCount = this._fb.group({
      batchGroup: this._fb.group(this.service.data['batchform']),
      physicalCountFormGroup: this._fb.group(
        {
          pc_qty: '',
          pc_upc: ''
        }
      )
    });
  }

  // DeleteBatch
  deleteBatch() {
    // Refreshing Growl Module
    this.service.errorHandle.msgs = [];
    // GET BATCH DATA
    const batchId = this.service.data['batchform'].batchID;
    this.itemsSubscription = this.service.deleteBatch(batchId).subscribe(
      data => {
          this.temp_data = data;
          if ('status' in this.temp_data) {
            this.error.displayError({ severity: 'success', summary: 'Batch Status', detail: this.temp_data.status });
          }
      },
      error => {
        this.error.displayError({ severity: 'error', summary: 'Batch Status', detail: 'Not able to delete batch.' });
      },
      () => {
        this.itemsSubscription.unsubscribe();
      }
    );
  }

  // Load Physical Counts
  addQuantityInItem() {
      const searchUPCForm = this.physicalCount['controls'].physicalCountFormGroup['controls'];
      let qty = searchUPCForm.pc_qty.value;
      let upc = searchUPCForm.pc_upc.value;

      const pdata: any = [];
      qty = (qty === '' || qty === null) ? '0' : qty;
      upc = (upc === '' || upc === null) ? '' : upc;

      // Refreshing Growl Module
      this.service.errorHandle.msgs = [];

      if (qty === null) {
        searchUPCForm.pc_qty.setValue(0);
      }

      if (upc === null || upc === '') {
        this.error.displayError({ severity: 'error', summary: 'Failed to add quantity', detail: 'UPC is required' });
        return false;
      }


      // Checking data in grid exists or not
      const check_upc = this.inventoryItems.filter(p => p.upc === upc);

      if (check_upc.length === 1) {
          this.stockQuantityManager({ itemid: check_upc[0].id, key: check_upc[0].key, stock: [], action: null, filledqty: qty });
          return false;
      }


      const reqBody = {  'filter' : [] };
//this.service._AppSettings.locid
      reqBody.filter.push({'field' : 'loc_id',  'operator': '=', 'value' : ''});

      if (upc !== '') {
        reqBody.filter.push({'field' : 'upc_code',  'operator': '=', 'value' : upc});
      }

      this.loadItems(reqBody, qty);
  }

  // Load Batch Items Check
  /**
   * Loads batch items
   *  dialogObject for sending dynamic headet,footer,buttons labes to dialog-box
   */
  loadBatchItems() {
    if (this.inventoryItems.length > 0) {
      this.showDialog = true;
      this.className = 'dialog card ';
      this.message_pop = 'Do you want to merge already scanned items with selected batch items?';
      this.button_pop = 'Yes';
      this.button_pop2 = 'No';
      const dialogObject = {
        'header':'My Care MVE',
        'footer':'Do you want to merge already scanned items with selected batch items?',
        'buttonone':'Yes',
        'buttontwo':'No'
      }
      this.alertpopup.dialogBox(dialogObject);
    } else {
      this.getBatchItems(false);
    }
  }

  // Button Pop Up Actions
  btnAct(event) {
    // Call If No
    this.getBatchItems(event.action);
    // Hide Dialogue
    this.showDialog = false;
  }

  // Getting Batch Items
  getBatchItems(action: boolean = null) {
    // Refreshing Growl Module
    this.service.errorHandle.msgs = [];

    // Get Batch Data
    const batchId = this.service.data['batchform'].batchID;
    // Batch Item Id's
    let batchItems = [];
    this.itemsSubscription = this.service.getBatchItems(batchId).subscribe(
      data => {
          this.temp_data = [];
          this.temp_data = data;
          batchItems = [];

          batchItems = this.getMergeAbleItemsInBatch(this.temp_data, action);
          const reqBody = {
            'filter' : [
              {
                'field': 'id',
                'operator': 'IN',
                'value': batchItems
              }
            ]
          };

          this.loadItems(reqBody, 0, data, action);
      },
      error => {
        this.temp_data = error;
        if ('error' in this.temp_data) {
          Object.keys(this.temp_data.error).forEach(key => {
              this.error.displayError({ severity: 'error', summary: 'Batch Status', detail: this.temp_data.error[key] });
          });
        }
      }
    );

  }


  // No Merge And Merge
  getMergeAbleItemsInBatch(temp_data: any = [], action: boolean = null) {

    // Class Object
    const obj = this;

    // item in grid temp data
    let item_to_load: any = [];
    let i = 0;
    // let test = [];

    const tempDataCopy = JSON.parse(JSON.stringify(temp_data['data']));
    const tempDataLength: number = tempDataCopy.length;
    this.preStock = tempDataCopy;
    if (obj.inventoryItems.length > 0) {
        obj.inventoryItems.forEach(function (item, key) {

          // Stock Manager
          obj.stockQuantityManager({itemid: item.id, key: key, stock: tempDataCopy, action: action, filledqty: 0});
          item_to_load = [];

          for ( i = 0; i < temp_data['data'].length; i++ ) {
            if ( tempDataCopy[i].item_id === item.id ) {
              delete temp_data['data'][i];
              item_to_load = tempDataCopy[i];
            }

          }

          if ( !action && item_to_load.length === 0) {
            delete obj.inventoryItems[key];
          }
        });

        obj.inventoryItems = obj.inventoryItems.filter(Boolean);
    }
    return temp_data['data'].filter(p => !null).map(function(element) {
      return element.item_id;
    });
  }


  // Load Items from API
  loadItems(reqBody: any, filledQty: number = 0, preStock: any = [], action: boolean = null) {
    // For Handling Loop End
    let i = 0;
    this.itemsSubscription = this.service.getPhysicalCounts(reqBody).subscribe(
      data => {
        this.temp_data = data;
        //  let stock = this.stock_temp;
        if (this.temp_data.data.length > 0) {
            this.temp_data.data.forEach( (item, index) => {
              i++;
              this.serializeItemData(item, this.inventoryItems.length, preStock, filledQty, action);
            });
        } else {
            this.error.displayError({ severity: 'error', summary: 'Not able to load item',
            detail: this.temp_data.status, life: 2000 });
        }
      },
      error => {
          this.error.displayError({ severity: 'error', summary: 'Not able to load item',
          detail: error.error.status, life: 2000 });

      },
      () => {
        this.itemsSubscription.unsubscribe();
      }
    );

  }


  // SerialLize Data from API
  serializeItemData(item, index, preStock: any = [], filledQty, action: boolean = null) {
        // Setting Default Quantity
        this.submitQuantity[index] = { 'quantity_scanned': 0, 'item_id': item.id };

        // Material
        const module_type = item.module_type_id;
        const material = (module_type === 1 && item.frame_material !== null) ? item.frame_material.materialname :
        (module_type === 2 && item.spectacle_material !== null) ? item.spectacle_material.material_name  : 'NA'  ;

        // Frame Type
        const frametype = (module_type === 1 && item.frame_type !== null) ? item.frame_type.type_name : 'NA';
        // stock this.service._AppSettings.locid
        const get_stock = item.locations.filter(p => p.loc_id === '');

        // Updating if stock exists
        this.stockQuantityManager({itemid: item.id, key: index, stock: preStock, action: action, filledqty: filledQty });

        this.inventoryItems[index] = {
                                      key: index,
                                      id: item.id,
                                      manufacturer: (item.manufacturer !== null) ? item.manufacturer.manufacturer_name : 'NA',
                                      brand:  (item.brand !== null) ? item.brand.brand_name : 'NA',
                                      name: item.name,
                                      upc: item.upc_code,
                                      color: item.color,
                                      color_code: item.color_code,
                                      material: material,
                                      type: frametype,
                                      eye: item.eyesize,
                                      retail_price: item.retail_price,
                                      wholesale_cost: item.wholesale_cost,
                                      stock: (get_stock.length > 0) ?  get_stock[0].stock : 0
                                    };

  }

  // PreLoaded Stock
  stockQuantityManager(config) {
    if (config.stock.length > 0  && config.action !== null) {
      const check_stock = config.stock.filter(p => p.item_id === config.itemid);
      let prev_Qty = 0;

      if (config.action) {
        prev_Qty = parseInt(this.submitQuantity[config.key].quantity_scanned, 10);
      }

      // If Stock Exists for particular item
      if (check_stock.length > 0) {
          this.id = config.key;
          this.submitQuantity[config.key].quantity_scanned = parseInt(check_stock[0].quantity_scanned, 10) + prev_Qty;
          setTimeout(() => {
            this.id = '';
          }, 500);
      }
    } else if (config.filledqty > 0 && config.action === null) {
      this.id = config.key;
      this.submitQuantity[config.key].quantity_scanned =
        parseInt(this.submitQuantity[config.key].quantity_scanned, 10) + parseInt(config.filledqty, 10);
      setTimeout(() => {
        this.id = '';
      }, 500);
    }
  }

  // On Click Search
  physicalSearchClick() {
    this.physicalSearch = true;
  }

  // Import File Screen
  ImportFile() {
    this.physicalImportFile = true;
  }

  // On Close Search
  searchevent(event) {
      this.physicalSearch = false;
      this.loadBatchItems();
  }

  // Batch search open
  batchSearchClik() {
    this.batchSearch = true;
  }

  // Batch Search Cancel
  batchSearchCancelevent(event) {
    this.batchSearch = false;
  }

  // Save Batch
  saveBatch() {

      // Refreshing Growl Module
      this.service.errorHandle.msgs = [];
      // GET BATCH DATA
      const batchForm = this.physicalCount['controls'].batchGroup['controls'];
      const reqdata = {
              'locationid': '',
              // this.service._AppSettings.locid,
              'receive_date': '',
              // this.service._AppSettings.transformDate(batchForm.batchDate.value) ,
              'notes': batchForm.batchNotes.value,
              'items': this.submitQuantity
            };
            if (batchForm.item.value > 0) {
              reqdata['module_type_id'] =  batchForm.item.value;
            }
      this.service.addUpdateBatch(reqdata, this.activeBatch).subscribe(
        data => {
            this.temp_data = data;
            if ('batch_id' in this.temp_data) {
              batchForm.batchID.patchValue(this.temp_data.batch_id);
              this.error.displayError({ severity: 'success', summary: 'Batch Status', detail: this.temp_data.status });
            } else {
              this.error.displayError({ severity: 'error', summary: 'Batch Status', detail: this.temp_data.status });
            }
        },
        error => {
          this.temp_data = error;

          if ('error' in this.temp_data) {
            Object.keys(this.temp_data.error).forEach(key => {
                this.error.displayError({ severity: 'error', summary: 'Batch Status', detail: this.temp_data.error[key][0] });
            });
          }
        }
      );

  }

  // Search Brands Related to Manufacturers
  searchBrandsRelatedManufactures(event: any) {
    const query = event.query;
    const reqbody = {
      'filter' : [
        {
          'field': 'frame_source',
          'operator': 'LIKE',
          'value': '%' + query + '%'
        }
      ]
    };
    const mid = (typeof this.manufacturer['Id'] !== undefined) ? this.manufacturer['Id'] : 1;

    this.service.getFilteredBrands(mid, reqbody).subscribe(
          data => {
              this.temp_data = data[0].manufacturer_brands;
              this.Brands = [];
              this.temp_data.forEach(item => {
                this.Brands.push({ Id: item.id, Name: item.frame_source});
              });
              this.filteredBrands = this.filteredBrandsManufacturersLoad(query, this.Brands);
          }
    );
  }

  // Search Both Manufacturer and Brands
  searchManufacturer(event: any) {
    const query = event.query;
    let mdata: any = [];
    this.brand = '';
    this.filteredBrands = [];
    const reqbody = {
      'filter' : [
        {
          'field' : 'manufacturer_name',
          'operator': 'LIKE',
          'value' : '%' + query + '%'
        }
      ]
    };
    this.service.getNewManufactures(reqbody).subscribe(
        data => {
          mdata = data;
          this.temp_data = mdata.data;
            this.Manufacturers = [];
            this.temp_data.forEach(item => {
              this.Manufacturers.push({ Id: item.id, Name: item.manufacturer_name});
            });
          this.filteredManufacturers = this.filteredBrandsManufacturersLoad(query, this.Manufacturers);
      },
      error => {
          this.filteredManufacturers = [];
      }
    );
  }

  // Filtered Brands Manufacturers
  filteredBrandsManufacturersLoad(query, data_m: any[]): any[] {
    const filtered: any[] = [];
    if (data_m.length > 0) {
      for (let i = 0; i < data_m.length; i++) {
          const data = data_m[i];
          if (data.Name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
              filtered.push(data);
          }
      }
    }
    return filtered;
  }

  // Load Locations
  LoadLocations() {
      this.locations = [];
      this.dropdown.getInventtoryLocations().subscribe(
          data => {
            this.temp_data = data['result']['Optical']['Locations'];
          // this.locations.push({ Id: '', Name: 'Select Location' });
            data['result']['Optical']['Locations'].forEach(item => {
                if (!this.locations.find(a => a.Name === item.Name)) {
                    this.locations.push({ Id: item.Id, Name: item.Name});
                }
            });
          }
      );
  }

  // Item Types
  LoadItemTypes() {
      if (this.items.length === 0) {
          this.service.getItemTypes().subscribe(
            data => {
                this.items = data;
            }
          );
      }
  }

  // Assigning Location
  changeLocation($event) {
      // this.service._AppSettings.locid = $event.target.value;
  }

  // Reversible Merge Data
  mergeData() {
    this.physicalCount.controls['batchGroup'].setValue(this.service.data['batchform']);
  }
}
