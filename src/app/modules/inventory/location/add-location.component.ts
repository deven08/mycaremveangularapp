// import { ApiAlertPipe } from '../../pipes/api-alert.pipe';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
// import { ContactlensService } from 'src/app/ConnectorEngine/services/contactlens.service';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';
// import { InventoryCommonService } from 'src/app/ConnectorEngine/inventory-common.service';
// import { LensTreatmentService } from 'src/app/ConnectorEngine/services/lenstreatments.service';
// import { OtherinventoryService } from 'src/app/ConnectorEngine/services/otherinventory.service';
// import { SpectcalelensService } from 'src/app/ConnectorEngine/services/spectcalelens.service';
import { Subscription } from 'rxjs';
import { ContactlensService } from '@app/core/services/inventory/contactlens.service';
import { FramesService } from '@app/core/services/inventory/frames.service';
import { OthersService } from '@app/core/services/inventory/others.service';
import { SpectaclelensService } from '@app/core/services/inventory/spectaclelens.service';
// import { LenstreatmentReactivateComponent } from '../lens-treatment';
import { LenstreatmentService } from '@app/core/services/inventory/lenstreatment.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { ErrorService } from '@app/core/services/error.service';
@Component({
    selector: 'app-add-location',
    templateUrl: 'add-location.component.html'
})
export class AddLocationComponent implements OnInit, OnDestroy {
    SpectacleSubscription: Subscription;
    Accesstoken: string;
    OtherID: number;
    OtherSubscription: Subscription;
    cols = [];
    locations = [];
    GetlocationsById = [];
    locationItemId = 0;
    locationstest = [];
    subscription: Subscription;
    contactLensSubscription: Subscription;
    SaveEnable = true;
    @Output() frameLocations = new EventEmitter<any>();
    @Output() spectLocations = new EventEmitter<any>();
    @Output() lensLocations = new EventEmitter<any>();
    @Output() otherLocations = new EventEmitter<any>();
    @Output() contactLocations = new EventEmitter<any>();
    constructor(private _FrameService: FramesService,
        private _ContactlensService: ContactlensService,
        private _OtherinventoryService: OthersService,
        private _SpectcalelensService: SpectaclelensService,
        private _LensTreatmentService: LenstreatmentService,
        private _InventoryCommonService: InventoryService,
        private error:ErrorService
        // private _ApiAlertPipe: ApiAlertPipe
        ) {

        // this.contactLensSubscription = _ContactlensService.contactListItemId$.subscribe(Id => {
        //     this.locationItemId = Id;
        //     this.GetlocationMasterDataById();
        // })
        // this.subscription = _FrameService.FrameListItemId$.subscribe(Id => {
        //     this.locationItemId = Id;
        //     this.GetlocationMasterDataById();
        // })
        // this.OtherSubscription = _OtherinventoryService.OtherListListItemId$.subscribe(Id=>{
        //     this.locationItemId = Id;
        //     this.GetlocationMasterDataById();
        // })

        // this.SpectacleSubscription = _SpectcalelensService.SpectacleListItemId$.subscribe(Id => {
        //     this.locationItemId = parseInt(Id);
        //     this.GetlocationMasterDataById();
        // });
        this.OtherInventoryId();
    }
    OtherInventoryId() {
        this.OtherID = parseInt(this._InventoryCommonService.multiLocationForOtherInventory, 10);
        // localStorage.getItem('multiLocationForOtherInventory')
        this.locationItemId = this.OtherID;
        // this.GetlocationMasterDataById();
        this.getLocationsMasterData();
        ///    this.Accesstoken = localStorage.getItem('tokenKey');
    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.contactLensSubscription.unsubscribe();
        // //  this.OtherSubscription.unsubscribe();
        // this.SpectacleSubscription.unsubscribe();
    }
    GetlocationMasterDataById() {
        this._FrameService.getItemLocations(this._InventoryCommonService.FortrasactionItemID).subscribe(data => {
            if (data['result']['Optical'] !== 'No record found') {
                this.GetlocationsById = [];
                this.GetlocationsById = data['result']['Optical']['ItemLocations'];
                this.locations.forEach(value => {
                    value.Check = false;
                    value.Enable = false;
                });
                this.locations.forEach(value => {
                    for (let j = 0; j < this.GetlocationsById.length; j++) {
                        if (value.Id === this.GetlocationsById[j].LocationID) {
                            value.Check = true;
                            value.Enable = true;
                            value.ItemId = this.GetlocationsById[j].ItemId;
                        }
                    }
                });
            }
            // else {
            //     this.getLocationsMasterData();
            // }

        });
    }
    getLocationsMasterData() {
        this.locations = [];
        this.locationstest = [];
        this._FrameService.getLocationsMasterDataApi().subscribe(DataSet => {
            DataSet['result']['Optical']['Locations'].forEach(item => {
                this.locations.push({ Id: item.Id, Name: item.Name, Check: false, Enable: false, ItemId: 0 });
            });
            this.GetlocationMasterDataById();
        });

    }
    ngOnInit(): void {

        // if (this.locationItemId == 0) {
        // this.getLocationsMasterData();
        // }
        this.cols = [
            { field: 'Enable', header: '' },
            { field: 'Id', header: '' },
            { field: 'Name', header: '' },
        ];
    }
    locationClose() {
        if (this._InventoryCommonService.InventoryType === 'Frames') {
            this.frameLocations.emit('frame');
            // this._FrameService.locationscloseObj.next('');
        }
        if (this._InventoryCommonService.InventoryType === 'SpectacleLens') {
            this.spectLocations.emit('spec');
            // this._SpectcalelensService.locationscloseObjspec.next('');
        }
        if (this._InventoryCommonService.InventoryType === 'LensTreatment') {
            this.lensLocations.emit('lens');
            // this._LensTreatmentService.locationscloseObjLt.next('');
        }
        if (this._InventoryCommonService.InventoryType === 'Others') {
            this.otherLocations.emit('other');
            // this._OtherinventoryService.locationscloseObjOther.next('');
        }
        if (this._InventoryCommonService.InventoryType === 'ContactLens') {
            this.contactLocations.emit('contact');
            // this._ContactlensService.locationscloseObjcl.next('');
        }
    }
    GetLocations(data: any) {
        for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i].Check === true && this.locations[i].Enable === true ||
                this.locations[i].Check === false && this.locations[i].Enable === false) {
                this.SaveEnable = true;
            }
        }

        for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i].Check === true && this.locations[i].Enable === false) {
                this.SaveEnable = false;
            }
        }
    }

    /**
     * Saves locations
     */
    saveLocations() {
        this._FrameService.errorHandle.msgs = [];
        this.locationstest = [];
        // this.SaveEnable = true
        for (let i = 0; i < this.locations.length; i++) {
            if (this.locations[i].Check === true && this.locations[i].Enable === false) {
                this.locationstest.push(this.locations[i].Id);
            }
        }

        if (this.locationstest.length !== 0) {
            console.log(this.locationstest);
            this._FrameService.SaveFrameLocations(this.locationstest, this._InventoryCommonService.FortrasactionItemID).subscribe(data => {
                if (data['Success'] === 1) {
                    this.SaveEnable = true;
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Success Message', detail: 'Locations Sasved Successfully'
                    });
                    // this.SuccessShow();
                    this.GetlocationMasterDataById();

                    if (this._InventoryCommonService.InventoryType === 'Frames') {
                        this.frameLocations.emit('');
                        // this._FrameService.locationscloseObj.next('');
                    }
                    if (this._InventoryCommonService.InventoryType === 'SpectacleLens') {
                        this.spectLocations.emit('');
                        // this._SpectcalelensService.locationscloseObjspec.next('');
                    }
                    if (this._InventoryCommonService.InventoryType === 'LensTreatment') {
                        this.lensLocations.emit('');
                        // this._LensTreatmentService.locationscloseObjLt.next('');
                    }
                    if (this._InventoryCommonService.InventoryType === 'Others') {
                        this.otherLocations.emit('other');
                        // this._OtherinventoryService.locationscloseObjOther.next('');
                    }
                    if (this._InventoryCommonService.InventoryType === 'ContactLens') {
                        this.contactLocations.emit('');
                        // this._ContactlensService.locationscloseObjcl.next('');
                    }

                }

            });
        } else {
            if (this._InventoryCommonService.InventoryType === 'Frames') {
                // this._FrameService.locationscloseObj.next('');
                this.frameLocations.emit('');
            }
            if (this._InventoryCommonService.InventoryType === 'SpectacleLens') {
                this.spectLocations.emit('');
                // this._SpectcalelensService.locationscloseObjspec.next('');
            }
            if (this._InventoryCommonService.InventoryType === 'LensTreatment') {
                this.lensLocations.emit('');
                // this._LensTreatmentService.locationscloseObjLt.next('');
            }
            if (this._InventoryCommonService.InventoryType === 'Others') {
                this.otherLocations.emit('other');
                // this._OtherinventoryService.locationscloseObjOther.next('');
            }
            if (this._InventoryCommonService.InventoryType === 'ContactLens') {
                this.contactLocations.emit('');
                // this._ContactlensService.locationscloseObjcl.next('');
            }
        }
        // console.log(this.locations);
    }
    // SuccessShow() {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Locations Saved Successfully' });
    // }
    // ErrorShow() {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please provide valid UPC Code.' });
    // }
}

