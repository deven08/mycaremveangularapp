import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';



@Component({
    selector: 'app-po-supplier',
    templateUrl: 'po-supplier.component.html'
})

export class PoSupplierComponent implements OnInit, OnDestroy {
    // @Input() savesuplier:any;
    /**
     * Output  of po supplier component
     */
    @Output() SupplierName: EventEmitter<any> = new EventEmitter();
    /**
     * Output  of po supplier component
     */
    @Output() suplierSaveFormData: EventEmitter<any> = new EventEmitter();
    checked = false;
    sortallinventory: boolean;
    locations: any[];
    supplierlist: any[];
    PurchaseForm: FormGroup;
    /**
     * Posupplier sbscription of po supplier component
     */
    POsupplierSbscription: Subscription
    /**
     * Posearch sbscription of po supplier component
     */
    POsearchSbscription: Subscription;
    /**
     * Posave subscription of po supplier component
     */
    POsaveSubscription: Subscription;
    constructor(private dropdownService: DropdownService,
        private errorService: ErrorService,
        private utility: UtilityService, private _fb: FormBuilder,
        private purchaseOrderService: PurchaseOrderService,
        private datePipe: DatePipe) {
         this.POsupplierSbscription =   purchaseOrderService.POitemsUpdate$.subscribe(data => {
            this.LoadForm();
            });
        this.POsearchSbscription = purchaseOrderService.POSelectedData$.subscribe(data => {
            this.purchaseOrderService.supplieerData = '';
            this.purchaseOrderService.POsupplier = data.supplierid;
            this.UpdateLoadFormStatic(data);
            // this.PurchaseForm.controls.vendor_id.patchValue(data.supplierid);
            // this.PurchaseForm.controls.vendor_address1.patchValue(data.saddress1);
            // this.PurchaseForm.controls.vendor_address2.patchValue(data.saddress2);
            // this.PurchaseForm.controls.city.patchValue(data.scity);
            // this.PurchaseForm.controls.state.patchValue(data.sstate);
            // this.PurchaseForm.controls.zip.patchValue(data.szip);
            // this.PurchaseForm.controls.mobile.patchValue(data.sphone);
            // this.PurchaseForm.controls.fax.patchValue(data.sfax);
            // this.PurchaseForm.controls.email.patchValue(data.semail);

            this.suplierSaveFormData.emit(this.PurchaseForm);
            this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
           this.supplierSaveEnable();
            // console.log(this.PurchaseForm.valid);
            // console.log('sub')
        })
        // this.POsaveSubscription = purchaseOrderService.POSaveData$.subscribe(PoOrderdata => {
        //     this.savingPOsupplier(PoOrderdata, this.PurchaseForm)
        // })
    }
    // savingPOsupplier(POrder, POsupplier) {


    // }
    /**
     * on init for initilizing methods
     */
    ngOnInit() {
        // alert(this.savesuplier);
        this.locations = [
            { name: 'MVE: My Vision', retail: '', source: '', Purchased: '', Sold: '', onhand: '', onorder: '', committed: '' },
        ];
        this.supplierDropData();
        this.LoadForm();
        this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
        this.supplierSaveEnable();
        this.suplierSaveFormData.emit(this.PurchaseForm);
        if (this.purchaseOrderService.supplieerData != '') {
            console.log(this.purchaseOrderService.supplieerData);
            this.UpdateLoadForm(this.purchaseOrderService.supplieerData);
            this.suplierSaveFormData.emit(this.PurchaseForm);
            this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
            this.supplierSaveEnable();
        }
        if (this.purchaseOrderService.searchsupplieerData != '') {
            let data = this.purchaseOrderService.searchsupplieerData;
            this.UpdateLoadFormStatic(data);
            // this.PurchaseForm.controls.vendor_id.patchValue(data.supplierid);
            // this.PurchaseForm.controls.vendor_address1.patchValue(data.saddress1);
            // this.PurchaseForm.controls.vendor_address2.patchValue(data.saddress2);
            // this.PurchaseForm.controls.city.patchValue(data.scity);
            // this.PurchaseForm.controls.state.patchValue(data.sstate);
            // this.PurchaseForm.controls.zip.patchValue(data.szip);
            // this.PurchaseForm.controls.mobile.patchValue(data.sphone);
            // this.PurchaseForm.controls.fax.patchValue(data.sfax);
            // this.PurchaseForm.controls.email.patchValue(data.semail);
            this.suplierSaveFormData.emit(this.PurchaseForm);
            this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
            this.supplierSaveEnable();
        }

    }
    /**
     * on destroy for unsubscribe observables
     */
    ngOnDestroy(): void {
        this.POsearchSbscription.unsubscribe();
        this.POsupplierSbscription.unsubscribe();
        // this.POsaveSubscription.unsubscribe();
    }
    /**
     * Loads form
     */
    LoadForm() {
        this.PurchaseForm = this._fb.group({
            vendor_id: ['', [Validators.required]],
            vendor_address1: '',
            vendor_address2: '',
            city: '',
            state: '',
            zip: '',
            mobile: '',
            fax: '',
            email: ''
        });
    }
    /**
     * Updates load form
     * @param data for update data
     */
    UpdateLoadForm(data) {
        this.PurchaseForm = this._fb.group({
            vendor_id: [data.id, [Validators.required]],
            vendor_address1: data.vendor_address,
            vendor_address2: data.address2,
            city: data.city,
            state: data.state,
            zip: data.zip,
            mobile: data.mobile,
            fax: data.fax,
            email: data.email
        });
    }
    /**
     * Updates load form static
     * @param data for update data
     */
    UpdateLoadFormStatic(data) {
        this.PurchaseForm = this._fb.group({
            vendor_id: [data.supplierid, [Validators.required]],
            vendor_address1: data.saddress1,
            vendor_address2: data.saddress2,
            city: data.scity,
            state: data.sstate,
            zip: data.szip,
            mobile: data.sphone,
            fax: data.sfax,
            email: data.semail
        });
    }
    /**
       * supplier drop data
       * @param {Array} supplierlist for binding dropdown data
       * @returns {object}  for dropdown data binding
       */
    supplierDropData() {
        this.supplierlist = [];
        let labsVenderData: any = [];
        this.supplierlist = this.dropdownService.supplierlist;
        if (this.utility.getObjectLength(this.supplierlist) === 0) {
            // this.supplierlist.push({ id: '', vendor_name: 'Select Lab' });
            this.dropdownService.getLabsVender().subscribe(
                data => {
                    try {
                        this.supplierlist.push({ id: '', vendor_name: 'Select Supplier' });
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            this.supplierlist.push(labsVenderData.data[i]);
                        }
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.dropdownService.lablist = this.supplierlist;
                });
        }

    }
    /**
     * Suppliers change data
     */
    supplierChange() {
        const value = this.PurchaseForm.controls.vendor_id.value;
        console.log(value)
        this.purchaseOrderService.getSupplierDetailsById(value).subscribe(data => {
            this.purchaseOrderService.searchsupplieerData = '';
            console.log(data)
            this.purchaseOrderService.supplieerData = data;
            this.purchaseOrderService.POsupplier = data['id'];
            this.UpdateLoadForm(data);
            this.SupplierName.emit(data['vendor_name']);
            this.suplierSaveFormData.emit(this.PurchaseForm);
            this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
            this.supplierSaveEnable();
        })
    }
    /**
     * Suppliers form change
     */
    supplierFormChange() {
        if (this.purchaseOrderService.supplieerData != '') {
            this.purchaseOrderService.supplieerData.address2 = this.PurchaseForm.controls.vendor_address2.value;
            this.purchaseOrderService.supplieerData.mobile = this.PurchaseForm.controls.mobile.value;
        }
        this.suplierSaveFormData.emit(this.PurchaseForm);
        this.purchaseOrderService.supplierValidate = this.PurchaseForm.valid;
        this.supplierSaveEnable();
    }
    /**
     * Suppliers save enable
     */
    supplierSaveEnable(){
        if(this.purchaseOrderService.supplierValidate == true && this.purchaseOrderService.formValidate == true){
            this.purchaseOrderService.savebuttonEnable = true;
        }else{
            this.purchaseOrderService.savebuttonEnable = false;
        }
    }
}
