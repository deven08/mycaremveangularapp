import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-add-grid-inventory-spectaclelens',
    templateUrl: 'add-grid-inventory-spectaclelens.component.html'
})
export class AddGridInventorySpectaclelensComponent implements OnInit {
    @Output() addgridoutput = new EventEmitter<any>();
    orderTypes: any[];

    ngOnInit() { }
    addGridClose() {
        this.addgridoutput.emit('close');
    }
}


