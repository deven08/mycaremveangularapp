import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core/services/error.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { SpectacleLensMaterial, LensStyleMaster, ManufacturerDropData } from '@app/core/models/masterDropDown.model';



@Component({
    selector: 'app-inventory-spectaclelens-advancedfilter',
    templateUrl: 'inventory-spectaclelens-advancedfilter.component.html'
})
export class InventorySpectaclelensAdvancedFilterComponent implements OnInit {
    orderTypes: any[];
    /**
     * Manufacturers  of inventory spectaclelens advanced filter component Manufacturers dropdown data
     */
    Manufacturers: any[];
    /**
     * Advance spectacle form of inventory spectaclelens advanced filter component for formgroup
     */
    AdvanceSpectacleForm: FormGroup;
    /**
     * Material list drop down of inventory spectaclelens advanced filter component material dropdown data 
     */
    MaterialListDropDown: any[];
    /**
     * Output  of inventory spectaclelens advanced filter component for sending events to spectacle lens main page
     */
    @Output() spectacleAdvFilter = new EventEmitter<any>();
    // constructor
    styles = [];
    lensstylcopy: any[];

    constructor(private _fb: FormBuilder,
        private spectcalelensServicee: SpectaclelensService,
        private dropdownService: DropdownService,
        private utility: UtilityService,
        private error: ErrorService) {

    }
    PostPolicyformObj: object = {
        manufacturer_id: '',
        upc_code: '',
        name: '',
        material_id: '',
        id: '',
        type_id:''
    };
    /**
     * on init
     * for initialize the dropdown and grid data 
     */
    ngOnInit() {

        this.LoadManufacturers();
        this.loadMaterialList();
        this.LoadLensStyle();
        this.AdvanceSpectacleForm = this._fb.group(this.PostPolicyformObj);
    }
    /**
     * Loads manufacturers list
     * loading the manufacturers drop-down data
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loads materialList list
     * loading the materialList drop-down data
     */
    loadMaterialList() {
        this.MaterialListDropDown = this.spectcalelensServicee.MaterialListDropDownData;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.spectcalelensServicee.getSpectaclesMasterList().subscribe((values: SpectacleLensMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ Id: '', Name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ Id: dropData[i].id, Name: dropData[i].material_name });
                    }
                    this.spectcalelensServicee.MaterialListDropDownData = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads lens style
     * @param {array} Styles for binding StylesDropDown data
     * @returns {object} for dropdown data
     */
    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        this.lensstylcopy = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    this.lensstylcopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Advances search
     * @returns for items search
     */
    advanceSearch() {
        const form = this.AdvanceSpectacleForm.value;
        this.spectacleAdvFilter.emit(form);

    }
    /**
     * Clears all
     * @returns for clear the form
     */
    clearAll() {
        this.AdvanceSpectacleForm = this._fb.group(this.PostPolicyformObj);
    }

}
