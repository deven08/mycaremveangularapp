import { Component, OnInit, EventEmitter, Output } from '@angular/core';


@Component({
    selector: 'app-add-prismgrid-inventory-spectaclelens',
    templateUrl: 'add-prismgrid-inventory-spectaclelens.component.html'
})
export class AddPrismgridInventorySpectaclelensComponent implements OnInit {
    orderTypes: any[];
    @Output() addprismgridoutput = new EventEmitter<any>();
    ngOnInit() { }
    addPrismGridClose() {
        this.addprismgridoutput.emit('close');
    }
}


