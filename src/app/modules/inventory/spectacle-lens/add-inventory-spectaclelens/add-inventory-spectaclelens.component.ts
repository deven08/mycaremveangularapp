import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, Data } from '@angular/router';
import { Subscription } from 'rxjs';
import { FramesService } from '@services/inventory/frames.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import {
    FrameColorData, LenColorData,
    CommissionStructure,
    CommissionType,
    FrameSupplier,
    SpectacleLensMaterial,
    LensStyleMaster,
    LensLab,
    ManufacturerDropData,
    LocationsMaster
} from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-inventory-spectaclelens',
    templateUrl: 'add-inventory-spectaclelens.component.html'
})
export class AddInventorySpectaclelensComponent implements OnInit {
    SpecConfigurations: any[] = [];
    measurmentsslistsavedata: any;
    measurmentsslist: any;
    treatmentslist: any[];
    orderTypes: any[];
    SpectaclesForm: FormGroup;
    Accesstoken = '';
    public manufacturerlist: Manufacturer[];
    Manufacturers: any[];
    private locationlist: any[];
    postdata: any;
    locations: Array<LocationsMaster>;
    upcdisable = false;
    addSpectacleLensInvTransaction = false;
    transactionsBtnHide = false;
    configBtnHide = false;
    inventoryTransfer = false;
    addProcedureCodeSidebar = false;
    MaterialListDropDown = [];
    lensLab: Array<object>;
    public supplierslist: any[];
    /**
     * Suppliers  of add inventory spectaclelens component
     */
    suppliers: Array<object>;
    price: any;
    styles = [];
    /**
     * Lensstylecopy  of add inventory spectaclelens component
     * For Handling styles data
     */
    lensstylecopy: any[];
    color = [];
    /**
     * Colorcopy  of add inventory spectaclelens component
     * Holding list of colors
     */
    colorcopy: Array<Data>;
    maxquantityenter: any;
    transacctionStockDetails: any = [];
    StockDetails: any = [];
    UpdateStock: any;
    subscriptionStockDetailsUpdateSpectacle: Subscription;

    @Input() locationItem: any;
    locationNameData: any[] = [];
    locationGridData: any[] = [];

    paramsavevalues: any;
    treatmentssavevalue: any;
    FilteredTrasaction: any = [];
    indexvalue: number;
    cptFromData: any;
    /**
     * Structure drop data of add inventory spectaclelens component
     * For Handling Structured data
     */
    StructureDropData: any[] = [];
    /**
     * Type drop data of add inventory spectaclelens component
     * For Handling CommisionType data
     */
    TypeDropData: any[] = [];
    @Output() spectacleAddItem = new EventEmitter<any>();
    locationsList: any;
    selectedupc: any;
    reqbodysearch: { filter: { field: string; operator: string; value: any; }[]; sort: { field: string; order: string; }[]; };
    data: any;
    /**
     * Units  of add inventory spectaclelens component
     * for handling unit data
     */
    units: any[] = [
        { id: null, Name: 'Select Unit' },
        { id: "Each", Name: "Each" },
        { id: "Pair", Name: "Pair" }]

    cptCodeDesc: any;

    /**
     * Tax dropdown of add inventory spectaclelens component for tax drop-down data
     */
    taxDropdown: any[];
    /**
     * Tax save list of add inventory spectaclelens component for Tax save list data
     */
    taxSaveList: Object;
    /**
     * Taxsave obj of add inventory spectaclelens component for tax save data list
     */
    taxsaveObj: any = [];
    spectacleTrasactionItemId: any;
    /**
     * Colors  of add inventory spectaclelens component
     * Holding lenscolor details
     */
    colors: Array<object>;

    constructor(
        private _fb: FormBuilder,
        private frameService: FramesService,
        private spectcalelensService: SpectaclelensService,
        private lensTreatmentService: LenstreatmentService,
        private _router: Router,
        private _InventoryCommonService: InventoryService,
        private dropdownService: DropdownService,
        private error: ErrorService,
        private utility: UtilityService) {


    }

    /**
     * on init for initilizing methods
     */
    ngOnInit() {
        this.LoadLocations();
        this.loadMaterialList();
        this.Loadmasters();
        this.Loadformdata();
        this.GetSupplierDetails();
        this.LoadLensStyle();
        this.LoadColors();
        this.loadStrutures();
        this.loadCommitionType();
        this.LoadManufacturers();
        this.gettreatments();
        this.getmeasurmenstmasster();
        this.getTaxDetailsList();
        if (this.locationItem !== undefined) {
            this.locationNameData = this.dropdownService.inventoryLocation;
            if (this.utility.getObjectLength(this.locationNameData) === 0) {
                this.dropdownService.getInventtoryLocations().subscribe(data => {
                    this.locationNameData = data['result']['Optical']['Locations'];
                    this.dropdownService.inventoryLocation = data['result']['Optical']['Locations'];
                    this._InventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                        this.locationGridData = data['data'];
                        this.locationGridData.forEach(element => {
                            for (let i = 0; i <= this.locationNameData.length - 1; i++) {

                                if (element.loc_id === this.locationNameData[i].Id) {
                                    element.loc_id = this.locationNameData[i].Name;
                                }
                            }
                        });
                    });
                })
            } else {
                this._InventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                    this.locationGridData = data['data'];
                    this.locationGridData.forEach(element => {
                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                            if (element.loc_id === this.locationNameData[i].Id) {
                                element.loc_id = this.locationNameData[i].Name;
                            }
                        }
                    });
                });
            }
        }
    }

    ngOnDestroy(): void {
        // this.subscriptionStockDetailsUpdateSpectacle.unsubscribe();
    }

    /**
     * Loadformdatas add inventory spectaclelens component for loading FromGroup Values
     */
    Loadformdata() {
        this.upcdisable = false;
        this.spectacleTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        this.Accesstoken = ''
        this.SpectaclesForm = this._fb.group({
            module_type_id: "2",
            id: '',
            upc_code: ['', [Validators.required]],
            name: ['', [Validators.required]],
            manufacturer_id: ['', [Validators.required]],
            vendor_id: '',
            lensTypeId: '',
            material_id: '',
            designId: '',
            treatmentId: [''],
            LocationId: '',
            supplierId: '',
            unitmeasure: null,
            retail_price: '',
            wholesale_cost: '',
            color_code: '',
            lab_id: '',
            returnable_flag: false,
            inventory: false,
            min_qty: '',
            max_qty: '',
            max_add_size: '',
            addd: '',
            diameter: '',
            oversize_diameter_charge: '',
            sphere: '',
            cylinder: '',
            bc: '',
            min_ht: '',
            notes: '',
            pricing_type: '',
            vsp_code: '',
            profit: '',
            type_id: '',
            eye_type: 'OD',
            colorid: '',
            amount: '',
            gross_percentage: '',
            spiff: '',
            structure_id: '',
            commission_type_id: '',
            procedureCodes: this._fb.array([
                this.initlanguage(),
            ])
        });
        this.selectedupc = this.spectcalelensService.selectedspectacleupc || this.spectcalelensService.selectedspectacleupcDoubleCLick;
        if (this.selectedupc) {
            this.upcdisable = true;
            this.updateFormItems(this.selectedupc);
        }
        const duplicateselectedupc = this.spectcalelensService.editDuplicatespectacleupc;
        if (duplicateselectedupc) {
            this.upcdisable = false;
            this.updateFormItems(duplicateselectedupc);
        }
    }
    /**
     * Retails chk
     * @param data for binding the retails drop-down
     */
    retailChk(data) {
        if (data == 'Retail') {
            this.SpectaclesForm.controls.pricing_type.patchValue('Retail');
        }
        if (data == 'RetailGrid') {
            this.SpectaclesForm.controls.pricing_type.patchValue('RetailGrid');
        }
    }
    /**
     * Updates form items
     * @param {string} ID for getting UPC value 
     */
    updateFormItems(data) {

        let filterdata = {
            filter: [{
                field: "upc_code",
                operator: "=",
                value: data
            },
            {
                field: "module_type_id",
                operator: "=",
                value: "2"
            },
            ]
        };
        let response: any = [];
        this.spectcalelensService.getSpectaclens(filterdata).subscribe(data => {
            response = data;
            const spectaclelensData = response.data[0];
            this.SpectaclesForm = this._fb.group({
                module_type_id: "2",
                id: spectaclelensData.id,
                upc_code: [spectaclelensData.upc_code, [Validators.required]],
                name: [spectaclelensData.name, [Validators.required]],
                manufacturer_id: [spectaclelensData.manufacturer_id, [Validators.required]],
                material_id: spectaclelensData.material_id === 0 || null ? '' : spectaclelensData.material_id,
                lab_id: spectaclelensData.lab_id === 0 || null ? '' : spectaclelensData.lab_id,
                vendor_id: spectaclelensData.vendor_id === 0 || null ? '' : spectaclelensData.vendor_id,
                type_id: spectaclelensData.type_id === '' ? '' : parseInt(spectaclelensData.type_id, 10),
                structure_id: spectaclelensData.structure_id === null || 0 ? null : spectaclelensData.structure_id,
                commission_type_id: spectaclelensData.commission_type_id === 0 ? null : spectaclelensData.commission_type_id.toString(),
                retail_price: spectaclelensData.retail_price,
                vsp_code: spectaclelensData.vsp_code,
                wholesale_cost: spectaclelensData.wholesale_cost,
                pricing_type: spectaclelensData.pricing_type,
                inventory: spectaclelensData.inventory === 1 ? true : false,
                returnable_flag: spectaclelensData.returnable_flag === 1 ? true : false,
                eye_type: spectaclelensData.eye_type === 1 ? true : false,
                min_qty: spectaclelensData.min_qty,
                max_qty: spectaclelensData.max_qty,
                max_add_size: spectaclelensData.max_add_size,
                diameter: spectaclelensData.diameter,
                oversize_diameter_charge: spectaclelensData.oversize_diameter_charge,
                sphere: spectaclelensData.sphere,
                cylinder: spectaclelensData.cylinder,
                bc: spectaclelensData.bc,
                min_ht: spectaclelensData.min_ht,
                notes: spectaclelensData.notes,
                procedureCode: spectaclelensData.procedure_code,
                profit: '',
                gross_percentage: spectaclelensData.gross_percentage,
                spiff: spectaclelensData.spiff,
                amount: spectaclelensData.amount,
                addd: spectaclelensData.addd,
                color_code: spectaclelensData.color_code,
                colorid: spectaclelensData.colorid === null ? '' : spectaclelensData.colorid,
                unitmeasure: spectaclelensData.unitmeasure == "0" ? null : spectaclelensData.unitmeasure,
                locationid: '',
                procedureCodes: this._fb.array([])
            });
            if (this.upcdisable == false) {
                this.SpectaclesForm.controls.upc_code.patchValue('');
            }
            const selectedID = this._InventoryCommonService.FortrasactionItemID;
            this.lensTreatmentService.getSavedProceduresById(selectedID).subscribe((savedProceduresdata: any[]) => {
                if (savedProceduresdata['data'] != null) {
                    for (let j = 0; j < savedProceduresdata['data'].length; j++) {
                        const control = <FormArray>this.SpectaclesForm.controls['procedureCodes'];
                        this._InventoryCommonService.getCptDesc(savedProceduresdata['data'][j].procedure_id).subscribe(data => {
                            this.cptCodeDesc = data
                            control.push(
                                this._fb.group({
                                    id: savedProceduresdata['data'][j].id,
                                    procedure_id: savedProceduresdata['data'][j].procedure_id,
                                    retailprice: savedProceduresdata['data'][j].retailprice,
                                    procedure_code: savedProceduresdata['data'][j].procedure_code,
                                    procedure_desc: this.cptCodeDesc.cpt_desc
                                })
                            );
                        });
                    }
                }
            },
                error => {
                    const control = <FormArray>this.SpectaclesForm.controls['procedureCodes'];
                    control.push(this.initlanguage());
                });
            this.profitcalculate();
            this.BindOdOdvalues(spectaclelensData.r_check, spectaclelensData.l_check);
            this.retailChk(spectaclelensData.pricing_type);
            this.getmeasurmentsbyconfiqID(spectaclelensData.id);
            this.gettreatmentsbyconfiqID(spectaclelensData.id);
        });
    }

    /**
     * Loadmasters add inventory spectaclelens component
     * @returns load th master drop-down
     */
    Loadmasters() {
        this.getlensLab();
    }


    /**
     * Binds od odvalues
     * @param [odvalue] binding the OD value
     * @param [osvalue]  binding the OS value
     */
    BindOdOdvalues(odvalue = 0, osvalue = 0) {
        if (odvalue === 1 && osvalue === 0) {
            this.SpectaclesForm.controls.eye_type.patchValue('OD');
        }
        if (osvalue === 1 && odvalue === 0) {
            this.SpectaclesForm.controls.eye_type.patchValue('OS');
        }
        if (odvalue === 1 && osvalue === 1) {
            this.SpectaclesForm.controls.eye_type.patchValue('OU');
        }
    }


    /**
     * Profitcalculates add inventory spectaclelens component
     * @returns for profit calculation
     */
    profitcalculate() {
        const value = this.SpectaclesForm.controls['retail_price'].value === 0 ? '' : this.SpectaclesForm.controls['retail_price'].value;
        this.SpectaclesForm.controls['retail_price'].patchValue(value);
        const cost = this.SpectaclesForm.controls['wholesale_cost'].value === 0 ? '' : this.SpectaclesForm.controls['wholesale_cost'].value;
        this.SpectaclesForm.controls['wholesale_cost'].patchValue(cost);
        if (this.SpectaclesForm.controls['wholesale_cost'].value !== '' ||
            this.SpectaclesForm.controls['wholesale_cost'].value != null || this.SpectaclesForm.controls['retail_price'].value !== '' ||
            this.SpectaclesForm.controls['retail_price'].value != null) {
            const diffvalue = this.SpectaclesForm.controls['retail_price'].value - this.SpectaclesForm.controls['wholesale_cost'].value;
            const calculatedvalue = diffvalue === 0 ? '' : diffvalue;
            this.SpectaclesForm.controls['profit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.SpectaclesForm.controls['retail_price'].value === '' && this.SpectaclesForm.controls['retail_price'].value === '') {
            this.SpectaclesForm.controls['profit'].patchValue('');
        }
    }

    /**
     * Loads manufacturers
     * loading the manufacturers list data
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Get lens lab
     * @returns loading the lens lab list data
     */
    getlensLab() {
        this.lensLab = this.dropdownService.lensLab;
        if (this.utility.getObjectLength(this.lensLab) === 0) {
            this.dropdownService.getLensLabDetails().subscribe((values: LensLab) => {
                try {
                    const dropData = values.data;
                    this.lensLab.push({ Id: '', Name: 'Select Lab' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.lensLab.push({ Id: dropData[i].id, Name: dropData[i].lab_name });
                    }
                    this.dropdownService.lensLab = this.lensLab;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads material list
     * loading the material list data
     */
    loadMaterialList() {
        this.MaterialListDropDown = this.spectcalelensService.MaterialListDropDownData;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.spectcalelensService.getSpectaclesMasterList().subscribe((values: SpectacleLensMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ Id: '', Name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ Id: dropData[i].id, Name: dropData[i].material_name });
                    }
                    this.spectcalelensService.MaterialListDropDownData = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    /**
     * Loads lens style
     * loading lensStyle data
     */
    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    this.lensstylecopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads colors
     * @returns list of colors data
     */
    LoadColors() {
        this.colors = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getFrameLenscolor().subscribe((values: LenColorData) => {
                try {
                    this.colors.push({ Id: '', Name: 'Select Color' });
                    values.data.forEach(element => {
                        this.colors.push({ Id: element.id, Name: element.colorname, colorcode: element.colorcode });
                    });
                    this.dropdownService.lensColors = this.colors;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }


    /**
     * Oncolorchanges add inventory spectaclelens component
     * @param  for color code change
     */
    oncolorchange(values) {
        const colorId = this.SpectaclesForm.controls['colorid'].value;
        this.SpectaclesForm.controls.color_code.patchValue('');
        if (colorId !== undefined) {
            const colordatails = this.color.filter(x => x.Id === colorId);
            this.SpectaclesForm.controls.color_code.patchValue(colordatails[0]['color_code']);
        }

    }

    /**
     * Binds colordropdown
     * @returns for the binding the color drop-down
     */
    BindColordropdown() {
        const colorcodevalue = this.SpectaclesForm.controls['color_code'].value;
        this.SpectaclesForm.controls.color.patchValue('');
        if (colorcodevalue !== undefined) {
            const colordatails = this.colorcopy.filter(x => x.colorcode === colorcodevalue);
            this.SpectaclesForm.controls.color.patchValue(parseInt(colordatails[0]['Id'], 10));
        }
    }

    /**
     * Loads locations
     * loading the location drop-down data
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
    /**
     * Gets supplier details
     * loading the supplier drop-down data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    }

    checkminmaxvalue(): boolean {
        this.frameService.errorHandle.msgs = [];
        const minvalue = parseInt(this.SpectaclesForm.controls['min_qty'].value, 10);
        const maxvalue = parseInt(this.SpectaclesForm.controls['max_qty'].value, 10);
        if (maxvalue != null && maxvalue !== 0 && minvalue != null && minvalue !== 0 && minvalue > maxvalue) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Min qty should be less then Max qty/Max qty should be Grater then Min qty.'
            });
            return false;
        }
    }
    structureformcontrols() {
        this.SpectaclesForm.removeControl('profit');
        if (this.SpectaclesForm.controls.inventory.value === true) {
            this.SpectaclesForm.controls.inventory.patchValue('1');
        } else {
            this.SpectaclesForm.controls.inventory.patchValue('0');
        }
    }

    /**
     * Save items data add inventory spectaclelens component
     * @returns save the items 
     */
    saveformdata() {

        this.frameService.errorHandle.msgs = [];
        const maxqtyrcheck = this.checkminmaxvalue();
        if (maxqtyrcheck === false) {
            return;
        }
        if (!this.SpectaclesForm.valid) {
            return;
        }
        const spherevaluetype = this.SpectaclesForm.controls['eye_type'].value;
        if (spherevaluetype === '') {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Provide OD/OS/OU Type.'
            });
            return;
        }
        this.structureformcontrols();
        if (this.SpectaclesForm.controls.type_id.value != null) {
            this.SpectaclesForm.controls.type_id.patchValue(this.SpectaclesForm.controls.type_id.value.toString());
        }
        let saveSpect = this.SpectaclesForm.value;
        delete saveSpect.procedureCodes;
        delete saveSpect.id;

        if (this.SpectaclesForm.controls.id.value == '') {

            this.spectcalelensService.Savedata(this.utility.formatWithOutControls(saveSpect)).subscribe(data => {
                this.postdata = data;
                this.saveProcedures(this.postdata.item_Id);
                this.savespectaclebymeasurmentsConfiqID(this.postdata.item_Id, this.measurmentsslist);
                this.savespectaclebytreatmentsConfiqID(this.postdata.item_Id, this.measurmentsslist);
                this.saveLocationTax();
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Spectacle Lens Saved Successfully'
                });
                this.spectacleAddItem.emit(this.postdata);
                // this._router.navigate(['/inventory/spectaclelens']);
            },
                error => {
                    if (error.statusText === 'Conflict') {
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Item with same upc already exists, but it is not the frame.'
                        });
                    } else {
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Unable to Save/Update the values'
                        });
                    }
                }
            );
        } else {
            this.spectcalelensService.updateSpectaclelensList(this.utility.formatWithOutControls(saveSpect), this.SpectaclesForm.controls.id.value).subscribe(data => {
                this.postdata = data;

                this.saveProcedures(this.postdata.item_Id);
                this.savespectaclebymeasurmentsConfiqID(this.postdata.item_Id, this.measurmentsslist);
                this.savespectaclebytreatmentsConfiqID(this.postdata.item_Id, this.measurmentsslist);
                this.saveLocationTax();
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Spectacle Lens Saved Successfully'
                });
                this.spectacleAddItem.emit(this.postdata);
                // this._router.navigate(['/inventory/spectaclelens']);
            },
                error => {
                    if (error.statusText === 'Conflict') {
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Item with same upc already exists, but it is not the frame.'
                        });
                    } else {
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Unable to Save/Update the values'
                        });
                    }
                }
            );
        }
    }
    /**
     * Saves procedures
     * @param itemId procedure code to the items
     */
    saveProcedures(itemId) {
        const reqbody = [];
        for (let j = 0; j < this.SpectaclesForm.controls['procedureCodes']['controls'].length; j++) {
            if (this.SpectaclesForm.controls['procedureCodes']['controls'][j].controls.procedure_id.value != null) {
                const data = {
                    'procedure_id': parseInt(this.SpectaclesForm.controls['procedureCodes']['controls'][j].controls.procedure_id.value, 10),
                    'procedure_code': this.SpectaclesForm.controls['procedureCodes']['controls'][j].controls.procedure_code.value,
                    'retailprice': this.SpectaclesForm.controls['procedureCodes']['controls'][j].controls.retailprice.value
                };
                reqbody.push(data);
            }

        }
        this.lensTreatmentService.saveLenstreatmentProcedures(reqbody, itemId).subscribe(data => { });
    }

    /**
     * Closes add modifyer
     * @returns close the Modify side-bar
     */
    closeAddModifyer() {
        this.spectacleAddItem.emit('');
    }

    /**
     * Clicks on inv transfer
     * @returns for displaying the transfer side-bar
     */
    clickOnInvTransfer() {
        this.inventoryTransfer = true;
    }

    /**
     * Procedures code
     * @returns for displaying the procedure code side-bar
     */
    procedureCode() {
        this.addProcedureCodeSidebar = true;
    }
    /**
     * Adds spectacle lens transaction
     * @returns for displaying the spectacle lens transaction side-bar
     */
    addSpectacleLensTransaction() {
        if (this.spectacleTrasactionItemId !== '') {
            this.addSpectacleLensInvTransaction = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Select any item or save new item for saving transactions'
            });
            this.addSpectacleLensInvTransaction = false;
        }

    }

    /**
     * Determines whether spectacle lens tabs click on
     * @param event for the active tabs
     */
    onSpectacleLensTabsClick(event) {
        if (event.index === 0) {
            this.transactionsBtnHide = false;
            this.configBtnHide = false;
        }
        if (event.index === 1) {
            this.transactionsBtnHide = false;
            this.configBtnHide = false;
            this.updateGirdDataLocation();
        }
        if (event.index === 2) {
            this.transactionsBtnHide = true;
            this.filterTransactionOnLocation();
            this.configBtnHide = false;
        }
        if (event.index === 3) {
            this.transactionsBtnHide = false;
            this.configBtnHide = true;
        }
        if (event.index === 4) {
            this.transactionsBtnHide = false;
            this.configBtnHide = false;
        }
    }
    getSpectacleLenstockDetails() {
        this.frameService.getstockDetail().subscribe(data => {
            for (let i = 0; i <= data['data'].length - 1; i++) {
                const loca = this.locationlist.filter(x => x.Id === data['data'][i]['loc_id']);
                data['data'][i]['loc_id'] = loca[0]['Name'];
                data['data'][i]['trans_type'] = data['data'][i]['transaction_type']['transactiontype'];
            }
            this.StockDetails = data['data'];
            this.transacctionStockDetails = this.StockDetails;
        });

    }
    salesApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] > 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }
    trasactionApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] >= 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }
    /**
     * Filters transaction on location
     * @returns filter transactins based on locations
     */
    filterTransactionOnLocation() {
        this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];
        if (this._InventoryCommonService.FortrasactionItemID === '') {
            this.transacctionStockDetails = [];
        } else {
            const dataspec = {
                filter: [
                    {
                        field: 'item_id',
                        operator: '=',
                        value: this._InventoryCommonService.FortrasactionItemID,
                    }
                ]
            };
            this.frameService.filterTrasactionData(dataspec).subscribe(data => {
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    // const loca = this.locationlist.filter(x => x.Id === data['data'][i]['loc_id']);
                    // data['data'][i]['loc_id'] = loca[0]['Name'];
                    data['data'][i]['trans_type'] = data['data'][i]['transaction_type']['transactiontype'];

                }
                this.StockDetails = data['data'];
                this.transacctionStockDetails = this.StockDetails;
            });
        }
    }
    /**
     * Filters transaction on location based
     * @returns filter the based on locations
     */
    filterTransactionOnLocationBased() {
        this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];

        if (this._InventoryCommonService.FortrasactionItemID !== '') {
            if (this.SpectaclesForm.controls['LocationId'].value === '') {
                this.filterTransactionOnLocation();
            } else {
                const dataSet = {
                    filter: [
                        {
                            field: 'item_id',
                            operator: '=',
                            value: this._InventoryCommonService.FortrasactionItemID,
                        },
                        {
                            field: 'loc_id',
                            operator: '=',
                            value: this.SpectaclesForm.controls['LocationId'].value,
                        }
                    ]
                };
                this.frameService.filterTrasactionData(dataSet).subscribe(data => {
                    this.FilteredTrasaction = data['data'];
                    this.StockDetails = this.FilteredTrasaction;
                    for (let i = 0; i <= this.StockDetails.length - 1; i++) {
                        // const loca = this.locationlist.filter(x => x.Id === this.StockDetails[i]['loc_id']);
                        // this.StockDetails[i]['loc_id'] = loca[0]['Name'];
                        this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];

                    }
                    // this.StockDetails = data['data'];
                    this.transacctionStockDetails = this.StockDetails;
                });
            }
        } else {
            this.transacctionStockDetails = [];
        }
    }

    /**
     * Getmeasurmenstmassters add inventory spectaclelens component
     * @returns for the measurments master data
     */
    getmeasurmenstmasster() {
        this.spectcalelensService.getmeasurmentsmaster().subscribe(
            data => {
                this.measurmentsslist = data['data'];
                for (let i = 0; i <= this.measurmentsslist.length - 1; i++) {
                    this.measurmentsslist[i].checkedvalues = false;
                    this.measurmentsslist[i].Availablebool = false;
                    this.measurmentsslist[i].requiredbool = false;
                }
            });
    }
    /**
     * Get treatments add inventory spectaclelens component
     * @returns for the treatments master data
     */
    gettreatments() {
        this.lensTreatmentService.getLensTreatmentList().subscribe(
            data => {
                this.treatmentslist = data['result']['Optical']['LensTreatment'].length;
                for (let i = 0; i <= this.treatmentslist.length - 1; i++) {
                    this.treatmentslist[i]['itemId'] = parseInt(data['result']['Optical']['LensTreatment'][i]['itemId'], 10);
                    this.treatmentslist[i].includebool = false;
                    this.treatmentslist[i].Availablebool = false;
                }
                this.treatmentslist = data['result']['Optical']['LensTreatment'];
            });
    }

    /**
     * Gettreatmentsbyconfiqs id
     * @param {string} confiqID for selected itemID   
     */
    gettreatmentsbyconfiqID(confiqID) {

        this.spectcalelensService.getTreatmentLensmeasurmenstByConfiqID(confiqID).subscribe(
            data => {
                this.treatmentssavevalue = data;
                // if (this.treatmentslist !== undefined) {
                this.treatmentssavevalue.forEach(element => {
                    const a = this.treatmentslist.findIndex(x => parseInt(x.itemId) === element.treatment_id);
                    if (a !== -1) {
                        // if (this.treatmentslist[a].itemId == element.treatment_id) {
                        this.treatmentslist[a].checkedvalues = true;
                        this.treatmentslist[a].includebool = element.include === 1 ? true : false;
                        this.treatmentslist[a].Availablebool = element.includeinorder === 1 ? true : false;
                        // }
                    }
                });
                // }

            });
    }

    getmeasurmentsbyconfiqID(confiqID) {
        this.spectcalelensService.getSpectacleLensmeasurmenstByConfiqID(confiqID).subscribe(data => {
            this.paramsavevalues = data;
            this.paramsavevalues.forEach(element => {
                const a = this.measurmentsslist.findIndex(x => x.id === element.measure_id);
                if (a !== -1) {
                    if (this.measurmentsslist[a].id === element.measure_id) {
                        this.measurmentsslist[a].checkedvalues = true;
                        this.measurmentsslist[a].Availablebool = element.available === 1 ? true : false;
                        this.measurmentsslist[a].requiredbool = element.required === 1 ? true : false;
                    }
                }
            });
        });
    }



    /**
     * Checkunchecktreatmentsevents add inventory spectaclelens component
     * @param {string} data for getting selected data
     * @param {string} add for boolean status
     * @param {string} index  for selected row index
     * @param {string} detailsofvalue  for type of the value
     */
    checkunchecktreatmentsevent(data, add, index, detailsofvalue) {
        if (this.treatmentslist !== undefined) {
            const a = this.treatmentslist.findIndex(x => x.itemId === data.itemId);
            if (a !== -1) {
                if (detailsofvalue === 'available') {
                    this.treatmentslist[a].itemId = data.itemId;
                    this.treatmentslist[a].checkedvalues = true;
                    this.treatmentslist[a].Availablebool = add;
                }
                if (detailsofvalue === 'include') {
                    this.treatmentslist[a].itemId = data.itemId;
                    this.treatmentslist[a].checkedvalues = true;
                    this.treatmentslist[a].includebool = add;
                }
                this.treatmentslist = this.treatmentslist;
            }
        }
    }

    /**
     * Savespectaclebytreatments confiq id
     * @param {string} configId for getting selected item id
     * @param {string} savevalues for getting saved values
     */
    savespectaclebytreatmentsConfiqID(configId, savevalues) {
        this.treatmentssavevalue = [];
        this.treatmentssavevalue.treatment = [];

        const details = this.treatmentslist.filter(x => x.checkedvalues === true);
        if (details != null && details !== undefined) {
            details.forEach(element => {
                const availablebool = element.Availablebool;
                const includebool = element.includebool;
                let available = availablebool === true ? 1 : 0;
                let required = includebool === true ? 1 : 0;
                if (element.includebool === true) {
                    available = 0;
                    required = 1;
                }
                if (element.Availablebool === true) {
                    available = 1;
                    required = 0;
                }
                if (element.Availablebool === true && element.includebool === true) {
                    available = 1;
                    required = 1;
                }
                // include is required
                // includeinorder available
                const values = {
                    'treatment_id': element.itemId,
                    'include': required,
                    'includeinorder': available
                };

                this.treatmentssavevalue.treatment.push(values);
            });

            const savevaluesdetails = {
                'treatment': this.treatmentssavevalue.treatment
            };

            this.spectcalelensService.SavetreatmentsLensmeasurmenstByConfiqID(configId, savevaluesdetails).subscribe(data => {

            },
                error => {

                }

            );

        }
    }


    /**
     * Checkunchekevents add inventory spectaclelens component
     * @param {string} data for getting selected data
     * @param {string} add for boolean status
     * @param {string} index  for selected row index
     * @param {string} detailsofvalue  for type of the value
     */
    checkunchekevent(data, add, index, detailsofvalue) {
        if (this.measurmentsslist !== undefined) {
            const a = this.measurmentsslist.findIndex(x => x.id === data.id);
            if (a !== -1) {
                if (detailsofvalue === 'available') {
                    this.measurmentsslist[a].id = data.id;
                    this.measurmentsslist[a].checkedvalues = true;
                    this.measurmentsslist[a].Availablebool = add;
                }

                if (detailsofvalue === 'required') {
                    this.measurmentsslist[a].id = data.id;
                    this.measurmentsslist[a].checkedvalues = true;
                    this.measurmentsslist[a].requiredbool = add;
                }
                this.measurmentsslist = this.measurmentsslist;
            }
        }
    }

    /**
     * Savespectaclebymeasurments confiq id
     * @param {string} configId for getting selected item id
     * @param {string} savevalues for getting saved values
     */
    savespectaclebymeasurmentsConfiqID(configId, savevalues) {
        this.measurmentsslistsavedata = [];
        this.measurmentsslistsavedata.measurements = [];

        const details = this.measurmentsslist.filter(x => x.checkedvalues === true);
        if (details !== null && details !== undefined) {

            details.forEach(element => {
                const availablebool = element.Availablebool;
                const requiredbool = element.requiredbool;
                let available = availablebool === true ? 1 : 0;
                let required = requiredbool === true ? 1 : 0;
                if (element.requiredbool === true) {
                    available = 0;
                    required = 1;
                }
                if (element.Availablebool === true) {
                    available = 1;
                    required = 0;
                }
                if (element.Availablebool === true && element.requiredbool === true) {
                    available = 1;
                    required = 1;
                }
                const values = {
                    'measure_id': element.id,
                    'available': available,
                    'required': required
                };

                this.measurmentsslistsavedata.measurements.push(values);
            });

            const savevaluesdetails = {
                'measurements': this.measurmentsslistsavedata.measurements
            };

            this.spectcalelensService.SaveSpectacleLensmeasurmenstByConfiqID(configId, savevaluesdetails).subscribe(data => {


            },
                error => {

                }

            );

        }

    }
    /**
     * Keys press
     * @param event for number keys validation
     */
    keyPress(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    getSpectacleLensConfiguration() {
        this.spectcalelensService.SpctaclleLensConfiguratiion(this._InventoryCommonService.FortrasactionItemID).subscribe(data => {
            this.SpecConfigurations = data['data'];
        });
    }


    initlanguage() {
        return this._fb.group({
            id: [],
            procedure_id: [],
            retailprice: [],
            procedure_code: [],
            procedure_desc: []
        });
    }
    add() {
        const control = <FormArray>this.SpectaclesForm.controls['procedureCodes'];
        control.push(this.initlanguage());
    }
    removeLanguage(i: number) {
        const id = this.SpectaclesForm.controls['procedureCodes']['controls'][i].controls.id.value;
        if (id !== '' || id != null) {
            const selectedupc = this._InventoryCommonService.FortrasactionItemID;
            this.lensTreatmentService.deleteSavedProceduresById(selectedupc, id).subscribe(data => {

            });
            const control = <FormArray>this.SpectaclesForm.controls['procedureCodes'];
            control.removeAt(i);
        } else {
            const control = <FormArray>this.SpectaclesForm.controls['procedureCodes'];
            control.removeAt(i);
        }

    }

    /**
     * Procedures code click
     * @param index for select procedure code items
     */
    procedureCodeClick(index) {
        this.indexvalue = index;
        const control = <FormArray>this.SpectaclesForm.controls['procedureCodes']['controls'][0];
        // control.push(this.initlanguage());
        this.addProcedureCodeSidebar = true;
    }
    /**
     * Procedurs result
     * @param event for cancel the procudure code items
     */
    procedurResult(event) {
        if (event !== 'cancle') {
            this.cptFromData = event;
            if (this.cptFromData !== '') {
                const id = this.SpectaclesForm.controls['procedureCodes']['controls'][this.indexvalue]['controls'].id.value;
                if (id === '' || id == null) {
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].id.patchValue('');
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_id.patchValue(this.cptFromData.cpt_fee_id);
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_code.patchValue(this.cptFromData.cpt4_code);
                    // this.SpectaclesForm.controls
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_desc.patchValue(this.cptFromData.cpt_desc);
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].retailprice.patchValue('0.00');
                } else {
                    const selectedupc = this._InventoryCommonService.FortrasactionItemID;
                    this.lensTreatmentService.deleteSavedProceduresById(selectedupc, id).subscribe(data => {

                    });
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].id.patchValue('');
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_id.patchValue(this.cptFromData.cpt_fee_id);
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_code.patchValue(this.cptFromData.cpt4_code);
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_desc.patchValue(this.cptFromData.cpt_desc);
                    this.SpectaclesForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].retailprice.patchValue('0.00');
                }

                this.addProcedureCodeSidebar = false;
            } else {
                this.addProcedureCodeSidebar = false;
            }
        } else {
            this.addProcedureCodeSidebar = false;
        }

    }
    SpectacletransEvent(data) {
        if (data === 'cancle') {
            this.addSpectacleLensInvTransaction = false;
        } else {
            this.UpdateStock = data;
            this.addSpectacleLensInvTransaction = false;
            this.filterTransactionOnLocation();
        }
    }


    /**
     * Loads strutures
     * @returns strutured data
     */
    loadStrutures() {
        this.StructureDropData = this._InventoryCommonService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this._InventoryCommonService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this._InventoryCommonService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads commition type
     * @returns CommisionType data
     */
    loadCommitionType() {
        this.TypeDropData = this._InventoryCommonService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this._InventoryCommonService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this._InventoryCommonService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Updates gird data location
     * for loading locations grid data items
     */
    updateGirdDataLocation() {
        this._InventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
            this.locationGridData = data['data'];
            this.locationGridData.forEach(element => {
                for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                    if (element.loc_id === this.locationNameData[i].Id) {
                        element.loc_id = this.locationNameData[i].Name;
                    }
                }
            });
        });
    }

    /**
     * Gets tax details list
     * loading tax drop-down items
     */
    getTaxDetailsList() {
        this.taxDropdown = [];
        this.taxDropdown = this.spectcalelensService.taxrate;
        if (this.utility.getObjectLength(this.taxDropdown) === 0) {
            this.spectcalelensService.getLocationTax().subscribe((values: any[]) => {
                try {
                    this.taxDropdown.push({ Id: null, name: 'Select Tax' });
                    const key = 'data';
                    for (let i = 0; i <= values[key].length - 1; i++) {
                        this.taxDropdown.push({ Id: values[key][i].id, name: values[key][i].taxrate });
                    }
                    this.spectcalelensService.taxrate = this.taxDropdown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Saves location tax
     * for tax 1 and tax 2 save based on locatons  
     */
    saveLocationTax() {
        this.taxsaveObj = [];
        for (let i = 0; i <= this.locationGridData.length - 1; i++) {
            if (this.locationGridData[i].taxid != null && this.locationGridData[i].taxid2 != null) {
                const locationTaxbody = {
                    id: this.locationGridData[i].id,
                    loc_id: this.locationGridData[i].loc_id,
                    item_id: this.locationGridData[i].item_id,
                    taxid: this.locationGridData[i].taxid === null ? 0 : parseInt(this.locationGridData[i].taxid),
                    taxid2: this.locationGridData[i].taxid2 === null ? 0 : parseInt(this.locationGridData[i].taxid2)
                }
                this.taxsaveObj.push(locationTaxbody);
            }
        }
        const object = { locations: this.taxsaveObj };

        this.spectcalelensService.saveLocationTax(object).subscribe(data => {
            this.taxSaveList = data;
        });
    }

    /**
     * Tax1s change
     * @param index getting select items
     * @param event getting selected data items
     */
    tax1Change(index, event) {
        this.locationGridData[index].taxid = event.Id;
    }
    /**
     * Tax2s change
     * @param index getting select items
     * @param event getting selected data items
     */
    tax2Change(index, event) {
        this.locationGridData[index].taxid2 = event.Id;
    }

}
