import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DatePipe } from '@angular/common';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { AppService } from '@app/core';
import { FrameSupplier, LocationsMaster, TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-spectaclelens-inventory-transaction',
    templateUrl: 'add-spectaclelens-transaction.component.html'
})

export class AddSpectaclelensInvTransactionComponent implements OnInit {

    StockDetails: any = [];
    paymentTermsData: any = [];
    /**
     * Type of transaction data of add spectaclelens inv transaction component
     */
    typeOfTransactionData: Array<object>;
    locationlist: any;
    locations: Array<LocationsMaster>;
    /**
     * Suppliers  of add spectaclelens inv transaction component
     */
    suppliers: Array<object>;
    public supplierslist: any[];
    spectacleLensTransactionForm: FormGroup;
    Accesstoken = '';
    trasactionFramesMsg: Object;
    // frameTrasactionItemId: string="";
    spectacleTrasactionItemId = '';
    @Output() SpecatcleTransOutput = new EventEmitter<string>();
    locationsList: any;
    constructor(
        private frameService: FramesService,
        private _fb: FormBuilder,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private datePipe: DatePipe,
        private dropdownService: DropdownService,
        public app: AppService,
        private utility: UtilityService) {

    }

    ngOnInit(): void {
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        // this.getFramesStockDetails();
        this.Loadformdata();
        this.GetSupplierDetails();
        this.LoadLocations();
    }

    Loadformdata() {
        // this.Accesstoken = localStorage.getItem('tokenKey');
        this.spectacleTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        this.spectacleLensTransactionForm = this._fb.group({
            // accessToken: this.Accesstoken,
            item_id: this.spectacleTrasactionItemId,
            loc_id: ['', [Validators.required]],
            // source: '',
            lot_no: ['', [Validators.required]],
            stock: ['', [Validators.required]],
            trans_type: ['', [Validators.required]],
            // reason: '',           
            cost: ['', [Validators.required]],
            invoiceno: ['', [Validators.required]],
            expirationdate: ['', [Validators.required]],
            supplierid: ['', [Validators.required]],
            notes: ['', [Validators.required]],
            received_date: ['', [Validators.required]],
            paymentterms: ['', [Validators.required]],
            returnDate: ['', [Validators.required]],
        });
    }

    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];

    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    typeOfInventoryTrasaction() {

        this.typeOfTransactionData = this.dropdownService.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdownService.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData  = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdownService.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }

    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdownService.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdownService.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdownService.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    saveFrameTransaction() {
        // this.frameService.errorHandle.msgs = [];
        // const itemId = this.spectacleLensTransactionForm.controls['item_id'].value;
        //this.spectacleLensTransactionForm.controls['item_id'].patchValue(itemId.toString());
        const transactionData = {
            inventory:
            {
                locationid: this.spectacleLensTransactionForm.controls['loc_id'].value,
                receive_date: this.datePipe.transform(this.spectacleLensTransactionForm.controls['received_date'].value, 'yyyy-MM-dd hh:mm:ss'),
                invoice: this.spectacleLensTransactionForm.controls['invoiceno'].value,
                notes: this.spectacleLensTransactionForm.controls['notes'].value,
                supplierid: this.spectacleLensTransactionForm.controls['supplierid'].value,
                module_type_id: '2'

            }
            ,
            stock:
            {
                trans_type: this.spectacleLensTransactionForm.controls['trans_type'].value,
                paymenttermsid: this.spectacleLensTransactionForm.controls['paymentterms'].value,
                returndt: this.datePipe.transform(this.spectacleLensTransactionForm.controls['returnDate'].value, 'yyyy-MM-dd hh:mm:ss'),
                cost: parseInt(this.spectacleLensTransactionForm.controls['cost'].value),
                stock: this.spectacleLensTransactionForm.controls['stock'].value,
                expirationdate: this.datePipe.transform(this.spectacleLensTransactionForm.controls['expirationdate'].value, 'yyyy-MM-dd hh:mm:ss'),
                lot_no: this.spectacleLensTransactionForm.controls['lot_no'].value,
                item_id: this.spectacleTrasactionItemId,
            }

        };
        console.log(this.spectacleLensTransactionForm.value);
        this.frameService.saveFrameTransactionDetails(transactionData).subscribe(data => {

            this.trasactionFramesMsg = data;
            console.log(this.trasactionFramesMsg);
            if (this.trasactionFramesMsg['status'] === 'Inventory Transaction added successfully') {

                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'SpectacleLens Transaction Saved Successfully'
                });
                // this.SuccessShow();
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Frame Transaction Saved Successfully' });
                this.SpecatcleTransOutput.emit('');
                // this._SpectcalelensService.transactionClose.next('');
            } else {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'SpectacleLens Transaction Failed'
                });
                // this.ErrorShow();

            }
        });
    }
    cancleClick() {
        this.SpecatcleTransOutput.emit('cancle');
    }
}
