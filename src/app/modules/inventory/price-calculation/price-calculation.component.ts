import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
    selector: 'app-pricecalculation',
    templateUrl: 'price-calculation.component.html'
})
export class PriceCalculationComponent implements OnInit {
    @Output() priceCalculationOut = new EventEmitter<any>();
    checked = false;
    FarmesForm: FormGroup;
    RoundDropData = [{ id: '1', name: 'UP' }, { id: '2', name: 'DOWN' }];
    constructor(private _fb: FormBuilder) {

    }
    ngOnInit() {
        this.FarmesForm = this._fb.group({
            sampleCost: '0.00',
            retailPrice: '0.00',
            Multiplier: '1.00',
            Amount: '0.00',
            Ends: '0.00',
            Round: '1'
        });

    }
    oKbtnClick() {
        this.priceCalculationOut.emit('test');
        //  alert(this.FarmesForm.controls["sampleCost"].value+this.FarmesForm.controls["retailPrice"].value)
        // let a = "0." + this.tester
        // console.log(parseFloat(a));
    }


    valuechange(data, controller) {
        let val;
        if (data != null) {
            if (this.FarmesForm.controls[controller].value != null) {
                val = this.FarmesForm.controls[controller].value.toString();
            }
            if (val > 1000) {
                val = val.slice(0, 3);
                this.FarmesForm.controls[controller].patchValue(val);
            }
        } else {
            this.FarmesForm.controls[controller].patchValue(null);
        }
    }
    keyPress(controller) {
        const sampleCost = this.FarmesForm.controls['sampleCost'].value;
        const retailPrice = this.FarmesForm.controls['retailPrice'].value;
        const MultiplierData = this.FarmesForm.controls['Multiplier'].value;
        const Amount = this.FarmesForm.controls['Amount'].value;
        const Ends = this.FarmesForm.controls['Ends'].value;

        let val = this.FarmesForm.controls[controller].value;
        if (val > 1000) {
            val = val.slice(0, 3);
            this.FarmesForm.controls[controller].patchValue(val);
        }
        this.FarmesForm.controls[controller].patchValue(parseFloat(val).toFixed(2));

        if (MultiplierData == null) {
            this.FarmesForm.controls['Multiplier'].patchValue('1.00');
        }
        if (MultiplierData === '0.00') {
            this.FarmesForm.controls['Multiplier'].patchValue('1.00');
        }
        if (sampleCost == null) {
            this.FarmesForm.controls['sampleCost'].patchValue('1.00');
        }
        if (sampleCost === '0.00') {
            this.FarmesForm.controls['sampleCost'].patchValue('0.00');
        }

        if (retailPrice == null) {
            this.FarmesForm.controls['retailPrice'].patchValue('1.00');
        }
        if (retailPrice === '0.00') {
            this.FarmesForm.controls['retailPrice'].patchValue('0.00');
        }

        if (Amount == null) {
            this.FarmesForm.controls['Amount'].patchValue('1.00');
        }
        if (Amount === '0.00') {
            this.FarmesForm.controls['Amount'].patchValue('0.00');
        }

        if (Ends == null) {
            this.FarmesForm.controls['Ends'].patchValue('1.00');
        }
        if (Ends === '0.00') {
            this.FarmesForm.controls['Ends'].patchValue('0.00');
        }
        const a = ((this.FarmesForm.controls['Multiplier'].value) * (this.FarmesForm.controls['sampleCost'].value));
        const b = (parseFloat(this.FarmesForm.controls['Amount'].value) + parseFloat(this.FarmesForm.controls['Ends'].value));
        this.FarmesForm.controls['retailPrice'].patchValue(a + b);
    }
}

