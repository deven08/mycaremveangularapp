import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DatePipe } from '@angular/common';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService } from '@app/core';
import { FrameSupplier, LocationsMaster, TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-add-other-inventory-transaction',
    templateUrl: 'add-other-inventory-transaction.component.html'
})

export class AddOtherInvTransactionComponent implements OnInit {
    StockDetails: any = [];
    paymentTermsData: any = [];
    /**
     * Type of transaction data of add other inv transaction component
     */
    typeOfTransactionData: Array<object>;
    locationlist: any;
    locations: Array<LocationsMaster>;
    /**
     * Supliers  stores the inventorycommon service variable
     */
    suppliers: Array<object>;
    /**
     * Supplierslist  stores suppliers data
     */
    public supplierslist: any[];
    otherTransactionForm: FormGroup;
    trasactionFramesMsg: Object;
    frameTrasactionItemId = '';
    @Output() OtherTransactionoutput = new EventEmitter<any>();
    locationsList: any;

    /**
     * Creates an instance of add other inv transaction component.
     * @param frameService  provide frameService
     * @param _fb form builder
     * @param _InventoryCommonService provide inventoryCommonService
     * @param error provide error service
     * @param utility provide  utility service
     */
    constructor(
        private frameService: FramesService,
        private _fb: FormBuilder,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private datePipe: DatePipe,
        public app: AppService,
        private dropdownService: DropdownService) {

    }

    ngOnInit(): void {
        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        // this.getFramesStockDetails();
        this.Loadformdata();
        this.GetSupplierDetails();
        this.LoadLocations();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
    }

    Loadformdata() {
       
        this.frameTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        this.otherTransactionForm = this._fb.group({
            item_id: this.frameTrasactionItemId,
            loc_id: ['', [Validators.required]],
            // source: '',
            lot_no: ['', [Validators.required]],
            stock: ['', [Validators.required]],
            trans_type: ['', [Validators.required]],
            // reason: '',           
            cost: ['', [Validators.required]],
            invoiceno: ['', [Validators.required]],
            expirationdate:['', [Validators.required]],
            supplierid: ['', [Validators.required]],
            notes:  ['', [Validators.required]],
            received_date: ['', [Validators.required]],
            paymentterms:['', [Validators.required]],
            returnDate: ['', [Validators.required]],
        });
    }
    /**
     * Loads locations 
     * @returns location details
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    /**
     * Gets supplier details
     * @returns supplier details data 
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    /**
     * Types of inventory trasaction
     * @returns type of transaction details
     */
    typeOfInventoryTrasaction() {
        this.typeOfTransactionData = this.dropdownService.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdownService.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData  = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdownService.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    /**
     * Gets frames payment terms
     * @returns payment terms
     */
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdownService.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdownService.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdownService.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Saves frame transaction
     * @returns save the transaction
     */
    saveFrameTransaction() {
        this.frameService.errorHandle.msgs = [];
        const transactionData = {
            inventory:
            {
                locationid: this.otherTransactionForm.controls['loc_id'].value,
                receive_date: this.datePipe.transform(this.otherTransactionForm.controls['received_date'].value, 'yyyy-MM-dd hh:mm:ss'),
                invoice: this.otherTransactionForm.controls['invoiceno'].value,
                notes: this.otherTransactionForm.controls['notes'].value,
                supplierid: this.otherTransactionForm.controls['supplierid'].value,
                module_type_id: '6'

            }
            ,
            stock:
            {
                trans_type: this.otherTransactionForm.controls['trans_type'].value,
                item_id: this.frameTrasactionItemId,
                paymenttermsid: this.otherTransactionForm.controls['paymentterms'].value,
                returndt: this.datePipe.transform(this.otherTransactionForm.controls['returnDate'].value, 'yyyy-MM-dd hh:mm:ss'),
                cost: parseInt(this.otherTransactionForm.controls['cost'].value),
                stock: this.otherTransactionForm.controls['stock'].value,
                expirationdate: this.datePipe.transform(this.otherTransactionForm.controls['expirationdate'].value, 'yyyy-MM-dd hh:mm:ss'),
                lot_no: this.otherTransactionForm.controls['lot_no'].value
            }

        };
        this.frameService.saveFrameTransactionDetails(transactionData).subscribe(data => {

            this.trasactionFramesMsg = data;
            console.log(this.trasactionFramesMsg);
            if (this.trasactionFramesMsg['status'] === 'Inventory Transaction added successfully') {
               this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Other Transaction Saved Successfully'
                });
               
                this.OtherTransactionoutput.emit('');
        
            } else {
               this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Other Transaction Failed'
                });

            }
        });
    }
    /**
     * Cancles click
     * @returns close the transaction page
     */
    cancleClick() {
        this.OtherTransactionoutput.emit('cancle');
    }
}
