import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core/services/error.service';
import { OthersService } from '@app/core/services/inventory/others.service';
import { OtherInvCategory } from '@app/core/models/masterDropDown.model';



@Component({
    selector: 'app-others-advancedfilter',
    templateUrl: 'others-advancedfilter.component.html'
})
export class OthersAdvancedFilterComponent implements OnInit {

    /**
     * Output  of others advanced filter component
     */
    @Output() othersAdvFilter = new EventEmitter<any>();
    // constructor
    /**
     * Styles  of others advanced filter component
     */
    styles = [];
    /**
     * Lensstylcopy  of others advanced filter component
     */
    lensstylcopy: any[];
    /**
     * Others advance filter form of others advanced filter component
     */
    othersAdvanceFilterForm: FormGroup;
    /**
     * Categories  of others advanced filter component
     */
    categories: any[] = [];
    /**
     * Categorylist  of others advanced filter component
     */
    categorylist: any[] = [];
    /**
     * Creates an instance of others advanced filter component.
     * @param fb for initilize form group
     * @param otherinventoryService for getting data from other service
     * @param utility for array length
     * @param error for error handling
     */
    constructor(private fb: FormBuilder,
                private otherinventoryService: OthersService,
                private utility: UtilityService,
                private error: ErrorService) {

    }

    /**
     * on init
     * for initialize the dropdown and grid data
     */
    ngOnInit() {
        this.formGroupdata();
        this.GetcategoryDetails();

    }
    /**
     * Forms groupdata
     */
    formGroupdata() {
        this.othersAdvanceFilterForm = this.fb.group({
            id: '',
            upc_code: '',
            name: '',
            categoryid: '',
            retail_price: '',
            procedure_code: '',
            qty_on_hand: ''
        });
    }


    /**
     * Getcategorys details for getting category dropdata
     */
    GetcategoryDetails() {

        this.categories = this.otherinventoryService.categoriesData;
        if (this.utility.getObjectLength(this.categories) === 0) {
            this.otherinventoryService.getcategorydata().subscribe((values: OtherInvCategory) => {
                try {
                    const dropData = values.data;
                    this.categories.push({ Id: '', Name: 'Select Category' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.categories.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                    }
                    this.otherinventoryService.categoriesData = this.categories;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }
    /**
     * Determines whether ok click on
     * for getting advance filter search data
     */
    onOkClick() {
        const form = this.othersAdvanceFilterForm.value;
        this.othersAdvFilter.emit(form);
    }
    /**
     * Clears all
     * for clearing enter form values
     */
    clearAll() {
        this.formGroupdata();
    }
}
