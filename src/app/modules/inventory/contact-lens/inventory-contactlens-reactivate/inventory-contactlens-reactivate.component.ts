import { Component, OnInit, ViewChild, OnDestroy, EventEmitter, Output } from '@angular/core';
// import { ContactlensService } from '../../../ConnectorEngine/services/contactlens.service';
// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';
import { Subscription } from 'rxjs';
import { InventoryService } from '@services/inventory/inventory.service';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { ErrorService } from '@app/core/services/error.service';


@Component({
    selector: 'app-inventory-contactlens-reactivate',
    templateUrl: 'inventory-contactlens-reactivate.component.html'
})
export class ContactlensRreactivateComponent implements OnInit, OnDestroy {
    cols: any[];
    gridlists: any[];
    reActivateItems: boolean;
    contactactivateMultipleSelection = [];
    activatedReqbody: any;
    reactiverefreshdetails: Subscription;
    @ViewChild('dtreactive') public dtreactive: any;
    @Output() ContactReactive = new EventEmitter<any>();

    /**
     * Creates an instance of contactlens rreactivate component.
     * @param contactlensService  provides contactlensService
     * @param _InventoryService provides InventoryService
     * @param error provides errorService
     */
    constructor(
        private contactlensService: ContactlensService,
        private _InventoryService: InventoryService,
        private error: ErrorService,
    ) {

    }
    ngOnInit() {
        this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'location', header: 'Location' },
            { field: 'name', header: 'Name' },
            { field: 'manufacturer_id', header: 'Manufacturer' },
            { field: 'lenscolor', header: 'Lens' },
            { field: 'upc_code', header: 'UPC' }
        ];
        this.dtreactive.selection = false;
        this.GetcontactlensList();
    }
    ngOnDestroy(): void {
        // this.reactiverefreshdetails.unsubscribe();
    }

    /**
     * Cancels contactlens reactivate component 
     */
    Cancel() {
        this.contactactivateMultipleSelection = [];
        this.ContactReactive.emit('contactClose');
        // this._ContactlensService.closeReactiveObj.next('value');
    }

    /**
     * Activates contactlens rreactivate component
     * @returns  if error
     */
    Activate() {
        this.contactlensService.errorHandle.msgs = [];
        if (this.contactactivateMultipleSelection.length === 0) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please select item to Re-Activate'
            });
            return;
        }

        this.activatedReqbody = [];
        this.contactactivateMultipleSelection.forEach(element => {
            this.activatedReqbody.push(element.id);
        });
        const activatedata = {
            'itemId': this.activatedReqbody
        };

        this._InventoryService.ActivateInventory(activatedata).subscribe(data => {
            this.gridlists = [];
            this.contactactivateMultipleSelection = [];
            this.error.displayError({
                severity: 'success',
                summary: 'Success Message', detail: 'Re-Activated Successfully'
            });
            this.GetcontactlensList();
            this.ContactReactive.emit('');
            // this.contactlensService.closeReactiveObj.next('value');

            // this._router.navigate(['frames']);
        },
            error => {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Unable to Re-Activate the selected values'
                });


            }
        );

    }

    /**
     * Getcontactlens list which are not activated
     */
    GetcontactlensList() {
        const constactLensSearchObject = {
            filter: [
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '3'
                },
                {
                    field: 'del_status',
                    operator: '=',
                    value: 1
                }

            ],
        }
        this.gridlists = [];
        this.contactlensService.getcontactlensNew(constactLensSearchObject).subscribe(
            data => {
                this.gridlists = data['data'];
            });

    }
}

