
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DatePipe } from '@angular/common';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService } from '@app/core';
import { FrameSupplier, LocationsMaster, TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-contactlens-inv-transaction',
    templateUrl: 'add-contactlens-transaction.component.html'
})

export class AddContactLensInvTransactionComponent implements OnInit {

    contactConfigSidebar = false;
    StockDetails: any = [];
    paymentTermsData: any = [];
    /**
     * Type of transaction data of add contact lens inv transaction component
     */
    typeOfTransactionData: Array<object>;
    locationlist: any;
    locations: Array<LocationsMaster>;
    /**
     * Suppliers  of add contact lens inv transaction component
     */
    suppliers: Array<object>;
    public supplierslist: any[];
    contactTransactionForm: FormGroup;
    Accesstoken = '';
    trasactionFramesMsg: Object;
    frameTrasactionItemId = '';
    ContactlensItemId = '';
    @Output() ContactLensTransOutput = new EventEmitter<any>();
    locationsList: any;
    /**
     * Creates an instance of add contact lens inv transaction component.
     * @param _ContactlensService contactlensservice data
     * @param _FrameService  provide frameservice data
     * @param _fb  form group 
     * @param _InventoryCommonService  provide inventorycommition type data
     * @param error provide error service data
     * @param utility provide utility service data
     */
    constructor(
        private _ContactlensService: ContactlensService,
        private _FrameService: FramesService,
        private _fb: FormBuilder,
        private _InventoryCommonService: InventoryService,
        // private _DropdownsPipe: DropdownsPipe,
        private error: ErrorService,
        private utility: UtilityService,
        private datePipe: DatePipe,
        private dropdownService: DropdownService,
        public app: AppService
    ) {

    }
    ngOnInit(): void {
        this.LoadLocations();
        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        // this.getFramesStockDetails();
        this.Loadformdata();
        this.GetSupplierDetails(); 
        // this.locations = this._DropdownsPipe.locationsDropData();
    }
    // SuccessShow() {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'ContactLens Transaction Saved Successfully' });
    // }
    // ErrorShow() {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'ContactLens Transaction Failed' });
    // }
    Loadformdata() {
        // this.Accesstoken = localStorage.getItem('tokenKey');
        this.ContactlensItemId = this._InventoryCommonService.FortrasactionItemID;
        // localStorage.getItem('FortrasactionItemID');
        this.contactTransactionForm = this._fb.group({
            // accessToken: this.Accesstoken,
            item_id: this.ContactlensItemId,
            loc_id: ['', [Validators.required]],
            // source: '',
            lot_no: ['', [Validators.required]],
            stock: ['', [Validators.required]],
            trans_type: ['', [Validators.required]],
            // reason: ['', [Validators.required]],           
            cost: ['', [Validators.required]],
            invoiceno: ['', [Validators.required]],
            expirationdate: ['', [Validators.required]],
            supplierid: ['', [Validators.required]],
            notes: ['', [Validators.required]],
            received_date: ['', [Validators.required]],
            paymentterms: ['', [Validators.required]],
            returnDate: ['', [Validators.required]],
        });
    }

    LoadLocations() {
        // Load Locations
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];

    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }
    /**
     * Gets supplier details
     * @returns supplierdetails
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    typeOfInventoryTrasaction() {
        this.typeOfTransactionData = this.dropdownService.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdownService.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData  = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdownService.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdownService.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdownService.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdownService.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    saveFrameTransaction() {
        this._FrameService.errorHandle.msgs = [];
        const transactionData = {
            inventory:
            {
                locationid: this.contactTransactionForm.controls['loc_id'].value,
                receive_date: this.datePipe.transform(this.contactTransactionForm.controls['received_date'].value, 'yyyy-MM-dd hh:mm:ss'),
                invoice: this.contactTransactionForm.controls['invoiceno'].value,
                notes: this.contactTransactionForm.controls['notes'].value,
                supplierid: this.contactTransactionForm.controls['supplierid'].value,
                module_type_id: '3'

            }
            ,
            stock:
            {
                trans_type: this.contactTransactionForm.controls['trans_type'].value,
                item_id: this.ContactlensItemId,
                paymenttermsid: this.contactTransactionForm.controls['paymentterms'].value,
                returndt: this.datePipe.transform(this.contactTransactionForm.controls['returnDate'].value, 'yyyy-MM-dd hh:mm:ss'),
                cost: parseInt(this.contactTransactionForm.controls['cost'].value),
                stock: this.contactTransactionForm.controls['stock'].value,
                expirationdate: this.datePipe.transform(this.contactTransactionForm.controls['expirationdate'].value, 'yyyy-MM-dd hh:mm:ss'),
                lot_no: this.contactTransactionForm.controls['lot_no'].value
            }

        };
        // console.log(this.contactTransactionForm.value);
        this._FrameService.saveFrameTransactionDetails(transactionData).subscribe(data => {

            this.trasactionFramesMsg = data;
            console.log(this.trasactionFramesMsg);
            if (this.trasactionFramesMsg['status'] === 'Inventory Transaction added successfully') {
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'ContactLens Transaction Saved Successfully'
                });

                // this.SuccessShow();
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Frame Transaction Saved Successfully' });
                this.ContactLensTransOutput.emit('');
                // this._ContactlensService.transactionCloseContact.next('');
            } else {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'ContactLens Transaction Failed'
                });

                // this.ErrorShow();

            }
        });
    }

    clickUPCConfig() {
        this.contactConfigSidebar = true;
    }
    cancleClick() {
        this.ContactLensTransOutput.emit('cancle');
    }
}
