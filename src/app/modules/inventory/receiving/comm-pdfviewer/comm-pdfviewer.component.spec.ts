import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommPdfviewerComponent } from './comm-pdfviewer.component';

describe('CommPdfviewerComponent', () => {
  let component: CommPdfviewerComponent;
  let fixture: ComponentFixture<CommPdfviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommPdfviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommPdfviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
