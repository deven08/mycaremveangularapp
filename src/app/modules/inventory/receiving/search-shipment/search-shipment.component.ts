import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppService } from '@app/core';

@Component({
  selector: 'app-search-shipment',
  templateUrl: 'search-shipment.component.html'
})
export class SearchShipmentComponent implements OnInit {
  constructor(public app: AppService) {

  }
  orderTypes: any[];
  @Output() shipmentOutput = new EventEmitter<any>();
  ngOnInit() { }
  onCloseShipment() {
    this.shipmentOutput.emit('close');
  }
}
