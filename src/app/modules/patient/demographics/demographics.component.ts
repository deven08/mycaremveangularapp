// Core Modules
import { Component, OnInit, OnChanges } from '@angular/core';

// Router
import { ActivatedRoute, Router} from '@angular/router';

// Services
import { IframesService } from '@shared/services/iframes.service';
import { AppService } from '@app/core';
// import {UserService} from '../../ConnectorEngine/services/user.service';


// Setting Up Component
@Component({
  selector: 'app-patient-demographics',
  templateUrl: 'demographics.component.html'
})


export class DemographicsComponent implements OnInit {
     DemographicsURL: Object;
     params: any;
     grid: String;
     /**
      * Creates an instance of demographics component.
      * @param router for navigating pages
      * @param activateRoute for activating routes
      * @param service for calling iframeservice methods
      * @param app for calling appservice methods 
      */
     constructor(
      //  private user: UserService,
       private router: Router,
       private activateRoute: ActivatedRoute,
       private service: IframesService,
       private app:AppService) {
     }

    ngOnInit() {
         this.params = this.activateRoute
         .queryParams
         .subscribe(params => {
             this.grid = params['grid'] || null;
             this.DemographicsURL = this.service.routeUrl('DemographicsURL', true, '?grid_activate=' + this.grid); 
         });  
         
     }

    /**
     * Resize the frame grid
     */
    resizeFrameGrid() {
        this.app.savePatientId = 0;
       this.service.resizeIframe();
       $('.loader-iframe').fadeOut();
     }
}
