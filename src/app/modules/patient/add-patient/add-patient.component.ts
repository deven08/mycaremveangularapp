import { ErrorService } from '@services/error.service';
import { AppService } from '@services/app.service';
// Core Module
import { Component, OnInit } from '@angular/core';

// import { PatientSearchService } from '../../ConnectorEngine/services/patient-search.service';
// import { UserService } from '../../ConnectorEngine/services/user.service';

// Frame Service
import { IframesService } from '@shared/services/iframes.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html'
})
export class AddPatientComponent implements OnInit {

  addPatientUrl: Object;
  checkReload: boolean;
  constructor(private router: Router, private service: IframesService, private app: AppService, private error: ErrorService) {
    this.addPatientUrl = this.service.routeUrl('addPatientUrl', true);
    // this.setPatientDynamically();
    this.app.patientId = 0;
    this.app.isPatientSigned = false;
  }

  ngOnInit() {
    this.checkReload = false;
  }

  setPatientDynamically() {
    let dt: any = [];
    if (this.app.patientId === 0) {
      this.app.getPatientInCON().subscribe(
        data => {
          try {
            dt = data;
            if (dt.result.Optical.PatientId !== undefined) {
              this.app.patientId = dt.result.Optical.PatientId;
              this.app.isPatientSigned = true;
              this.router.navigate(['/patient/demographics'], { queryParams: { 'grid': 'demographics' } });
              // this.user.resetPatientAssigned();
              // this.user.setPatientAssigned(dt.result.Optical.PatientId);
              // this._StateInstanceService.PatientId = dt.result.Optical.PatientId;
              // localStorage.setItem('PatientId', dt.result.Optical.PatientId);
            }
          } catch (err) {
            this.error.syntaxErrors(err);
          }
        },
        (err) => {

        }
      );
    }


  }

  onLoad() {
    this.app.patientId = 0;
    this.app.isPatientSigned = false;
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
    if (this.checkReload === false) {
      this.setPatientDynamically();
    }
    
  }

}
