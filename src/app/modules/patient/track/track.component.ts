// App Configuration Module
import {IframesService} from '@shared/services/iframes.service';
// Core Modules
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';


// Setting Up Component
@Component({
  selector: 'app-patient-track',
  templateUrl: './track.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientTrackComponent implements OnInit {
  MonitorUrl: Object;
  constructor(private service: IframesService) {
    this.MonitorUrl = this.service.routeUrl('MonitorUrl', true);
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
