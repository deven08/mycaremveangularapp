import { AddPatientAlertComponent,
         AddPatientComponent,
         AppointmentsComponent,
         DemographicsComponent,
         PatientAlertsComponent,
         PatientDocsComponent,
         PatientInsuranceComponent,
         PatientMergeComponent,
         PatientOrderHistoryComponent,
         PatientSearchComponent,
         PatientTrackComponent
         } from './';

import {SharedModule} from '@shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PatientRoutingModule } from './patient-routing.module';
import { FormsModule } from '@angular/forms';
import {PatientSearchService} from '@services/patient/patient-search.service';

// Patient Components

@NgModule({
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    FormsModule,
  ],
  declarations: [
    DemographicsComponent,
    PatientInsuranceComponent,
    PatientSearchComponent,
    PatientDocsComponent,
    PatientOrderHistoryComponent,
    PatientTrackComponent,
    PatientMergeComponent,
    AddPatientComponent,
    AddPatientAlertComponent,
    AppointmentsComponent,
    PatientAlertsComponent
  ],
  providers: [
    PatientSearchService
  ]

})
export class PatientModule { }
