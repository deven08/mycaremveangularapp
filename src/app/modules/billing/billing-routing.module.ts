import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    WriteOffCodesComponent,
    CPTComponent,
    FeeTableComponent,
    DXCodesComponent,
    ICD10Component,
    ModifiersComponent,
    ManagePOSComponent,
    PoliciesComponent,
    ElectronicComponent
} from './billing.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'cpt',
                component: CPTComponent,
                data: { title: 'CPT - Billing', banner: false }
            },
            {
                path: 'fee-table',
                component: FeeTableComponent,
                data: { title: 'Fee Table - Billing', banner: false }
            },
            {
                path: 'dx-codes',
                component: DXCodesComponent,
                data: { title: 'DX Codes - Billing', banner: false }
            },
            {
                path: 'icd10',
                component: ICD10Component,
                data: { title: 'ICD10 - Billing', banner: false }
            },
            {
                path: 'write-off-codes',
                component: WriteOffCodesComponent,
                data: { title: 'Write Off Codes - Billing', banner: false }
            },
            {
                path: 'modifiers',
                component: ModifiersComponent,
                data: { title: 'Modifiers - Billing', banner: false }
            },
            {
                path: 'manage-pos',
                component: ManagePOSComponent,
                data: { title: 'Manage POS - Billing', banner: false }
            },
            {
                path: 'policies',
                component: PoliciesComponent,
                data: { title: 'Policies - Billing', banner: false }
            },
            {
                path: 'electronic',
                component: ElectronicComponent,
                data: { title: 'Electronic - Billing', banner: false }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillingRoutingModule { }
