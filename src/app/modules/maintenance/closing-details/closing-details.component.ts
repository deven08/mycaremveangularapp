import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'closing-details',
    templateUrl: 'closing-details.component.html'
})

export class ClosingDetailsComponent implements OnInit {

    closeDetails: any[];
    adjustments: any [];
    ngOnInit(): void {
        this.closeDetails = [
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
            { orderid: '6213', patientname: '', charges: '0.00', payments: '0.00', patientbalance: '', insbalance: '', employee: '' },
        ];
        this.adjustments = [
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
            { id: '6213', type: '', paymentmethod: '', amount: '0.00', date: '', paymentuser: '' },
        ];
    }

}
