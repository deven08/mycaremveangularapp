import { MaintenanceRoutingModule } from './maintenance-routing.module';

describe('MaintenanceRoutingModule', () => {
  let maintenanceRoutingModule: MaintenanceRoutingModule;

  beforeEach(() => {
    maintenanceRoutingModule = new MaintenanceRoutingModule();
  });

  it('should create an instance', () => {
    expect(maintenanceRoutingModule).toBeTruthy();
  });
});
