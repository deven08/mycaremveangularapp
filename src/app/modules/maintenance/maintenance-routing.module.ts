import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DailyClosingComponent } from './daily-closing/daily-closing.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'daily-closing',
        component: DailyClosingComponent,
        data: { 'title': 'Daily Closing - Maintenance' }
      }
    ]
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
