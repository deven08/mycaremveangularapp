import { Component, OnInit } from '@angular/core';
import { MaintenanceService } from '@services/maintenance/maintenance.service';
import { ErrorService, AppService } from '@app/core';
import { UtilityService } from '@shared/services/utility.service';
import { DatePipe } from '@angular/common';
import { DropdownService } from '@shared/services/dropdown.service';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-daily-closing',
    templateUrl: 'daily-closing.component.html'
})

export class DailyClosingComponent implements OnInit {
    closingDetailsSidebar = false;
    openDateSidebar = false;
    mediaCountSidebar = false;
    printPreviewSidebar = false;
    orderTypes: any[];
    dailyClosingNotes = '';
    locations: Array<LocationsMaster>;
    amountEntered: any;
    lockedByUser: any;


    /**
     * Selected posting date of daily closing component 
     */
    selectedPostingDate: any;

    /**
     * Daily closing data of daily closing grid data.
     */
    dailyClosingData: any = [];

    /**
     * Sum variance of daily closing component all the variance is summed here
     */
    sumVariance = 0;

    /**
     * Daily closing object of daily closing component body for the post api
     */
    dailyClosingObject: any = [];

    /**
     * Locked status of daily closing status
     */
    lockedStatus: any;
    locationId: any;


    /**
     * Show enterdinbox of daily closing hiding and showing of the test box in table
     */
    showEnterdinbox: boolean = true;
    showEnterd: boolean = false;

    /**
     * Dailyclosingsaving data of daily closing  saving api responce storing
     */
    dailyclosingsavingData: any = [];

    /**
     * Daily closing retrival data of daily closing  retrival data
     */
    dailyClosingRetrivalData: any = [];

    /**
     * Disabled process btn is for disable the btn
     */
    disabledProcessBtn: boolean = true;

    /**
     * Creates an instance of daily closing component.
     * @param maintenanceService for calling maintainance method
     * @param error for finding the syntax errors
     * @param app for fetching the location id
     * @param utility for the formating of the jsonbosy without null and emptys
     * @param datePipe for the format of the date
     */
    constructor(
        private maintenanceService: MaintenanceService,
        private error: ErrorService,
        public app: AppService,
        private utility: UtilityService,
        private datePipe: DatePipe,
        private dropdowns: DropdownService,
    ) {

    }
    ngOnInit(): void {
        this.LoadLocations()
        this.ListOfDailyClosingItems();
    }

    /**
     * Closing details opeing and closing of the Closing details sidebar 
     */
    closingDetails() {
        this.closingDetailsSidebar = true;
    }

    /**
     * Opens dates opeing and closing of the Opens dates sidebar 
     */
    openDates() {
        this.openDateSidebar = true;
    }

    /**
     * Opens media count opeing and closing of themedia count sidebar
     */
    openMediaCount() {
        this.mediaCountSidebar = true;
    }

    /**
     * Prints preview opeing and closing of Prints preview  sidebar
     */
    printPreview() {
        this.printPreviewSidebar = true;
    }


    /**
     * list of daily closing items = for geeting the list of daily closing items
     * 
     */
    ListOfDailyClosingItems() {
        this.maintenanceService.getListOfDailyClosingItems().subscribe(data => {
            try {
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    data['data']['amount_entered'] = ''
                    data['data']['varience'] = ''
                    data['data']['varienceReason'] = ''
                }
                this.dailyClosingData = data['data'];
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        });
    }



    /**
     * Amounts entered for showing the variance between the amount and entered amount.
     * @param  {object} data what are the entered values in the amount entered 
     * @param {number} index  for getting the index
     */
    varianceCalculation(data, index) {
        this.dailyClosingData[index]['amount_entered'] = data.target.value
        this.dailyClosingData[index]['varience'] = this.dailyClosingData[index]['amount'] - data.target.value
    }
    variancereason(data, index) {
        this.dailyClosingData[index]['varienceReason'] = data.target.value
    }

    /**
     * Saves daily closing used for the saving the daily closing data
     *
     */
    saveDailyClosing() {
        for (let i = 0; i <= this.dailyClosingData.length - 1; i++) {
            this.sumVariance = this.sumVariance + parseFloat(this.dailyClosingData[i].varience);
            const dailyClosingDetailsData = {
                paymentmethod_id: this.dailyClosingData[i].payment_mode.pm_id,
                amount_calculated: this.dailyClosingData[i].amount,
                amount_entered: this.dailyClosingData[i].amount_entered,
                variance_reason: this.dailyClosingData[i].varienceReason
            };
            this.dailyClosingObject.push(dailyClosingDetailsData);
        }
        const dailyCLosingObjectData = {
            locationid: this.app.locationId,
            notes: this.dailyClosingNotes,
            sum_variance: this.sumVariance,
            dailyClosingDetails: this.dailyClosingObject,
        };
        this.maintenanceService.dailyClosingsaving(this.utility.formatWithOutControls(dailyCLosingObjectData)).subscribe(resp => {
            try {
                this.dailyclosingsavingData = resp;

                this.lockedStatus = this.dailyclosingsavingData['locked'] == 0 ? 'open' : 'closed';
                this.locationId = this.dailyclosingsavingData['locationid']
                if (this.dailyclosingsavingData['id'] != null) {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Daily Closing',
                        detail: 'Saved  Successfully'
                    });
                    this.disabledProcessBtn = false;
                }
                this.selectedPostingDate = this.dailyclosingsavingData['closing_date'];
                this.retrivalOfDailyClosing()
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        });
    }


    /**
     * Retrivals of daily closing based on the date and location selected
     */
    retrivalOfDailyClosing() {
        for (let i = 0; i <= this.dailyClosingData.length - 1; i++) {
            this.dailyClosingData[i]['amount_entered'] = "0"
            this.dailyClosingData[i]['amount'] = "0"
            this.dailyClosingData[i]['varience'] = "0"
            this.dailyClosingData[i]['varienceReason'] = "0"
        }
        this.maintenanceService.dailyCLosingRetrval(this.datePipe.transform(this.selectedPostingDate, 'yyyy-MM-dd'))
            .subscribe(data => {
                try {
                    this.dailyClosingRetrivalData = data;

                    // this.dailyClosingData = [];
                    // this.dailyClosingData.push({});
                    this.showEnterd = true;
                    this.showEnterdinbox = false;
                    for (let i = 0; i <= this.dailyClosingData.length - 1; i++) {
                        this.dailyClosingData[i]['amount_entered'] =
                            this.dailyClosingRetrivalData[this.dailyClosingRetrivalData.length - 1].detail[i].amount_entered;
                        this.dailyClosingData[i]['amount'] =
                            this.dailyClosingRetrivalData[this.dailyClosingRetrivalData.length - 1].detail[i].amount_calculated;
                        this.dailyClosingData[i]['varience'] =
                            this.dailyClosingRetrivalData[this.dailyClosingRetrivalData.length - 1].detail[i].amount_calculated
                            -
                            this.dailyClosingRetrivalData[this.dailyClosingRetrivalData.length - 1].detail[i].amount_entered;
                        this.dailyClosingData[i]['varienceReason'] =
                            this.dailyClosingRetrivalData[this.dailyClosingRetrivalData.length - 1].detail[i].variance_reason;

                    }
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
    }

    /**
     * Loads locations is for loading of locations
     */
    LoadLocations() {
        // Load Locations
        this.locations = this.dropdowns.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdowns.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdowns.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    /**
     * Dailys closing process data for the process closing
     */
    dailyClosingProcessData() {
        const dailyProcess = {
            dailyClosingID: [this.dailyclosingsavingData['id']],
        };
        this.maintenanceService.dailyclosingProcess(dailyProcess).subscribe(data => {
            try {
                const resp = data;
                if (resp[0]['id'] != null) {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'Daily Closing',
                        detail: 'Processed  Successfully'
                    });
                    this.disabledProcessBtn = true;
                }
                this.lockedStatus = resp[0]['locked'] == 0 ? 'open' : 'closed';
                this.locationId = resp[0]['locationid']
                this.selectedPostingDate = resp[0]['closing_date'];

            } catch (error) {
                this.error.syntaxErrors(error);
            }
        });
    }
}
