import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SupplierService } from '@app/core/services/inventory/supplier.service';
import { FrameSupplier } from '@app/core/models/masterDropDown.model';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
@Component({
    selector: 'app-advanced-filter',
    templateUrl: './advance-filter.component.html'
})

export class SupplierAdvancedFilterComponent implements OnInit {
    AdvanceSearchSupplierForm: FormGroup;
    Id = '';
    Supplier = '';
    supplierId = '';
    public Supplierlist: any[];
    /**
     * Suppliers  of supplier advanced filter component
     */
    Suppliers: Array<object>;
    selectedSupplier = 'null';
    SearchformObj: object = {
        Id: '',
        Supplier: ''
    };
    @Output() supplierAdvSearch = new EventEmitter<any>();
    constructor(private _fb: FormBuilder,
        private dropdownService: DropdownService,
        private error: ErrorService,
        private utility: UtilityService) {

    }

    LoadSuppliers() {
        this.Suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.Suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.Suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.Suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.Suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }



    ngOnInit() {
        this.AdvanceSearchSupplierForm = this._fb.group(this.SearchformObj);
        this.LoadSuppliers();
        this.Clear();
        this.selectedSupplier = 'null';
    }

    advanceLensTreatmentSearch() {
        const form = this.AdvanceSearchSupplierForm.value;
        this.supplierAdvSearch.emit(form);
        // this._SupplierService.advanceSearchSupplier.next(form);
    }
    Clear() {
        this.Id = '';
        this.Supplier = '';
        this.selectedSupplier = 'null';
    }
}
