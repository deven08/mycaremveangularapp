import { Component, OnInit } from '@angular/core';
import { TaxService } from '@services/settings/tax.service';
import { ErrorService } from '@services/error.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilityService } from '@shared/services/utility.service';



@Component({
    selector: 'app-tax',
    templateUrl: './tax.component.html'
})
export class TaxComponent implements OnInit {

    /**
     * Taxcolumns  will structure the cloumns to be displayed on the UI
     */
    taxcolumns: any[];

    /**
     * Visible  will handle the visisbility of the columns in the grid
     */
    visible = true;

    /**
     * Taxgridobj  will be assigned with data pulled from tax list API
     */
    taxgridobj: any[];

    /**
     * Tax form of tax component
     */
    TaxForm: FormGroup;

    /**
     * Taxheader  will handle displaying the screen header for add/modify
     */
    taxheader: string;

    /**
     * Isactivestatus  will store the status of the Tax selected
     */
    isactivestatus: boolean;

    /**
     * Taxdetailssidebar whill store/handle the slide componenet on screen
     */
    taxDetailsSidebar = false;

    /**
     * Creates an instance of tax component.
     * fb - formbuilder  delas with formgroup
     * taxservice - service to interact with tax component
     * error - Error handler to interact with tax component
     * utilityService - service to format Json
     */
    constructor(private fb: FormBuilder,
                private taxservice: TaxService,
                private error: ErrorService,
                private utilityService: UtilityService) { }



    /**
     * on init
     */
    ngOnInit() {
        this.isactivestatus = false;
        this.Loadformdata();
        this.GetTaxList();
        this.Gettaxdata();
    }


    /**
     * Loadformdata will bind the component elements with the formgroup strcucture
     */
    private Loadformdata() {
        let taxformdata: any = [];
        this.TaxForm = this.fb.group({
            taxdesc: ['', [Validators.required]],
            taxrate: ['', [Validators.required]],
            status: false,
        });
        const selectedtaxid = this.taxservice.edittaxid;
        if (selectedtaxid) {
            this.taxservice.gettaxbyID(selectedtaxid).subscribe(
                data => {
                    taxformdata = data;
                    this.TaxForm = this.fb.group({
                        taxdesc: [taxformdata.taxdesc, [Validators.required]],
                        taxrate: [taxformdata.taxrate, [Validators.required]],
                        status: taxformdata.del_status === 0 ? true : false
                    });

                }
            );
        }

    }


    /**
     * Getstaxlist will create the grid structure to display data on UI
     */
    GetTaxList(): void {
        this.taxcolumns = [
            { field: 'id', header: 'Id', checkbox: false, visible: this.visible },
            { field: 'taxdesc', header: 'Description', checkbox: false, visible: this.visible },
            { field: 'taxrate', header: 'Rate', checkbox: false, visible: this.visible },
            { field: 'del_status', header: 'Status', visible: this.visible }

        ];

    }

    /**
     * Gettaxdata will bind the taxes list received from the taxlist API
     */
    Gettaxdata() {
        let taxdata: any = [];
        this.taxservice.gettaxdata().subscribe(
            data => {
                taxdata = data;
                for (let i = 0; i <= taxdata.data.length - 1; i++) {
                    taxdata.data[i].del_status = taxdata.data[i].del_status === 0 ? 'Active' : 'Inactive';
                }
                this.taxgridobj = taxdata.data;
            });
    }

    /**
     * onRowSelect will trigger on row selection of grid
     * event - will include the trigger events
     */
    onRowSelect(event) {
        this.taxservice.edittaxid = event.data.id;
    }

    /**
     * onRowUnselect will trigger on row unselect  of grid
     * event - will include the trigger events
     */
    onRowUnselect(event) {
        this.taxservice.edittaxid = '';
    }


    /**
     * Updatetax will update the selected tax changes
     */
    updatetax() {
        this.isactivestatus = false;

        const selectedtaxid = this.taxservice.edittaxid;
        if (!selectedtaxid) {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Tax.' });

        } else {
            this.taxheader = 'Modify Tax Details';
            this.taxDetailsSidebar = true;
            this.Loadformdata();

        }
    }


    /**
     * DoubleClicktax will trigger on grid doble click to display the UI for selected tax
     * rowData - will include the trigger events
     */
    DoubleClicktax(rowData) {
        this.taxheader = 'Modify Tax Details';
        this.isactivestatus = false;
        this.taxservice.edittaxid = rowData.id;
        if (!this.taxservice.edittaxid) {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Tax.' });
            // this.dt.selection = false;
        } else {
            this.taxDetailsSidebar = true;
            this.Loadformdata();

        }
    }

    /**
     * Addtax will clean the assigned varibales and show the UI for creating new taxes
     */
    addtax() {
        this.isactivestatus = true;
        this.taxservice.edittaxid = '';
        this.taxDetailsSidebar = true;
        this.Loadformdata();
        this.taxheader = 'Add Tax Details';
        this.TaxForm.controls.status.patchValue(true);
    }


    /**
     * Updatetaxstatus will update the selected tax status to active/inactive
     * activatedata -  inputs the body object of request
     * activestatus - defines the actial status of the selected tax
     */
    updatetaxstatus(activatedata, activestatus) {
        const bodyString = JSON.stringify(activatedata);
        this.taxservice.updatetaxstatus(bodyString, this.taxservice.edittaxid, activestatus).subscribe(
            taxstatusdata => {
                this.Gettaxdata();
            });



    }

    /**
     * Savetax will sdo operations related to saving/updating the tax element and the process incudes the validatioin process
     */
    Savetax() {
        this.taxservice.errorHandle.msgs = [];

        if (!this.TaxForm.valid) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please enter Required fields'
            });
            return;
        }
        const activestatus = this.TaxForm.controls.status.value === true ? 'Active' : 'Inactive';
        const activatedata = {
            itemId: this.taxservice.edittaxid
        };
        this.TaxForm.removeControl('status');
        this.taxservice.Savetaxdata(this.utilityService.format(this.TaxForm), this.taxservice.edittaxid)
            .subscribe(taxdata => {
                this.error.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Tax Saved Successfully'
                });
                if (this.taxservice.edittaxid > 0) {
                    this.updatetaxstatus(activatedata, activestatus);
                } else {
                    this.Gettaxdata();
                }
                this.taxDetailsSidebar = false;
            },
                error => {
                    this.error.displayError({
                        severity: 'error',
                        summary: 'error Message', detail: 'Save Failed'
                    });
                }

            );
    }

    /**
     * Canceltax will allow the tax add/modify interface to close without any changes
     */
    canceltax() {
        this.taxDetailsSidebar = false;
    }

}
