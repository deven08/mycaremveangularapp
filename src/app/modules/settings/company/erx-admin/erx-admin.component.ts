import { Component, OnInit } from '@angular/core';
import { ErxAdminService } from '@services/settings/erx-admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorService } from '@app/core';
import { ErxAdmin } from '@models/settings/erxadmin.model';


@Component({
    selector: 'app-erx-admin',
    templateUrl: 'erx-admin.component.html'
})
export class ErxAdminComponent implements OnInit {
    /**
     * Erx admin of erx admin component for storing the resposevice data
     */
    erxAdmin: object;
    /**
     * Erx admin form of erx admin component for form group
     */
    erxAdminForm: FormGroup;
    /**
     * Status  of erx admin component for status dropdown
     */
    status: Array<any>;
    /**
     * Determines whether erxlist for update the erx data
     */
    eRxlist: object;
    /**
     * Enable cancelbtn of erx admin component for cancel button
     */
    enableCancelbtn: boolean;

    /**
     * Creates an instance of erx admin component.
     * @param fb is formbuilder
     * @param erxService for calling erx service medthods
     * @param errorService for calling errorhandling methods
     */
    constructor(private fb: FormBuilder,
                private erxService: ErxAdminService,
                private errorService: ErrorService) {
    }
    ngOnInit() {
        this.loadFormdata();
        this.geteRxDetails();
        this.status = [
            { id: null, Name: 'Select Status' },
            { id: 1, Name: 'Active' },
            { id: 0, Name: 'Inactive' }
        ];
    }
    /**
     * Loads formdata
     * load React FormBuilder
     */
    loadFormdata() {
        this.erxAdminForm = this.fb.group({
            portal: ['', [Validators.required]],
            erx_mode: ['', [Validators.required]],
            system_name: ['', [Validators.required]],
            secret_key: ['', [Validators.required]],
            erx_user_name: ['', [Validators.required]],
            erx_status: ['', [Validators.required]],
        });
    }
    /**
     * Loads update form data
     * @param eRxlist binding the form data
     */
    loadUpdateFormData(eRxlist) {
        this.erxAdminForm = this.fb.group({
            portal: [eRxlist.portal, [Validators.required]],
            erx_mode: [eRxlist.erx_mode, [Validators.required]],
            system_name: [eRxlist.system_name, [Validators.required]],
            secret_key: [eRxlist.secret_key, [Validators.required]],
            erx_user_name: [eRxlist.erx_user_name, [Validators.required]],
            erx_status: [eRxlist.erx_status, [Validators.required]]
        });
    }
    /**
     * Gets erx details
     * getting the erx data
     */
    geteRxDetails() {
        this.erxService.geteRXAdmin().subscribe((eRxData: ErxAdmin) => {
            this.eRxlist = eRxData;
            this.loadUpdateFormData(this.eRxlist);
        });
    }
    /**
     * Updateerxs details for update the erx data
     */
    updateerxDetails() {
        const reqBody = {
            portal: this.erxAdminForm.controls.portal.value,
            erx_mode: this.erxAdminForm.controls.erx_mode.value,
            system_name: this.erxAdminForm.controls.system_name.value,
            secret_key: this.erxAdminForm.controls.secret_key.value,
            erx_user_name: this.erxAdminForm.controls.erx_user_name.value,
            erx_status: this.erxAdminForm.controls.erx_status.value,
        };
        this.erxService.UpdateErxAdminSetting(reqBody).subscribe(data => {
            this.erxAdmin = data;
            this.errorService.displayError({
                severity: 'success',
                summary: 'success Message', detail: 'Update Successfully'
            });
        });
    }

    /**
     * Cancels update for cancel the update changes
     */
    cancelUpdate() {
        this.geteRxDetails();
        this.enableCancelbtn = false;
    }

    /**
     * Enables cancel for onchange the anable/disable cancel button
     */
    enableCancel() {
        if (this.erxAdminForm.valid === true) {
            this.enableCancelbtn = true;
        } else {
            this.enableCancelbtn = false;
        }
    }
}
