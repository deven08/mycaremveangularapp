
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@shared/services/iframes.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  CommonUrl: any = '';


  constructor(private iframeService: IframesService) {
    this.CommonUrl = this.iframeService.routeUrl('CompanyUrl', true);
  }

  changeFrameUrl(frameKey) {
    this.CommonUrl = this.iframeService.routeUrl(frameKey, true);
  }

  ngOnInit() {

  }

  onLoad() {
    this.iframeService.resizeIframe(true);
    $('.loader-iframe').fadeOut();
  }

}
