import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';


@Component({
    selector: 'app-commissions',
    templateUrl: 'commissions.component.html'
})
export class CommissionsComponent implements OnInit {
    servicesdata: any[];
    rowData: any;
    ngOnInit() {
        this.servicesdata = [
            { field: 'Type', header: 'Prescription Type Usage' },
            { field: 'Date', header: 'Physician Rx Date' },
            { field: 'Description', header: 'Description' },
            { field: 'Cylinder', header: 'Cylinder' },
            { field: 'Axis', header: 'Axis' },
            { field: 'Add', header: 'Add' },
            { field: 'Prism', header: 'Prism' },
            { field: 'Base', header: 'Base' },
            { field: 'Prism 2', header: 'Prism 2' },
            { field: 'Base 2', header: 'Base 2' },
            { field: 'Order', header: 'Order Now' }
        ];
    }
    constructor(private _router: Router) {

    }



}

