import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceGroupsComponent } from './insurance-groups.component';

describe('InsuranceGroupsComponent', () => {
  let component: InsuranceGroupsComponent;
  let fixture: ComponentFixture<InsuranceGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
