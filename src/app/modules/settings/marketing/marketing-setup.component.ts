import { Component, OnInit, ViewChild } from '@angular/core';
// import { DialogBoxComponent } from '@shared/components/dialog-box/dialog-box.component';



@Component({
    selector: 'app-marketing-setup',
    templateUrl: 'marketing-setup.component.html'
})
export class MarketingSetupComponent implements OnInit {
    addInventoryOthers;
    advancedFilter;
    addMarketingDetails = false;


    // @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    cols: any[];
    gridlists: any[];
    ngOnInit() {

        this.cols = [
            { field: 'location', header: 'Location' },
            { field: 'type', header: 'Type' },
            { field: 'code', header: 'Code' },
            { field: 'source', header: 'Source' },
            { field: 'description', header: 'Description' }
        ];
        this.gridlists = [
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'Marketing',
                'source': 'Referrals', 'description': ' Marketing Offer'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF',
                'source': 'Email Newsletter', 'description': ' Aura (A/R coating D)'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Newsletter', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L50',
                'source': 'Newsletter', 'description': ' Spectacle Lens Package'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L-50',
                'source': 'Email Newsletter', 'description': ' 50% offer'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'Marketing',
                'source': 'Referrals', 'description': ' Marketing Offer'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF',
                'source': 'Email Newsletter', 'description': ' Aura (A/R coating D)'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Newsletter', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L50',
                'source': 'Newsletter', 'description': ' Spectacle Lens Package'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L-50',
                'source': 'Email Newsletter', 'description': ' 50% offer'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
            {
                'location': 'MVE: My Vision', 'type': '241', 'code': 'BOGOF-L',
                'source': 'Handouts', 'description': ' Buy one get one free frame'
            },
        ];
    }
    constructor() {

    }

    deleteRows() {
        // this.DeleteRows.dialogBox();
    }


}
