// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing Module
import { PatientHistoryRoutingModule } from './patient-history-routing.module';

// Components
import { AllergiesComponent } from './allergies/allergies.component';
import { CcHistoryComponent } from './cc-history/cc-history.component';
import { GeneralHealthComponent } from './general-health/general-health.component';
import { ImmunizationsComponent } from './immunizations/immunizations.component';
import { LabComponent } from './lab/lab.component';
import { MedicationComponent } from './medication/medication.component';
import { OrderSetsComponent } from './order-sets/order-sets.component';
import { OcularComponent } from './ocular/ocular.component';
import { ProblemListComponent } from './problem-list/problem-list.component';
import { RadComponent } from './rad/rad.component';
import { SxProceduresComponent } from './sx-procedures/sx-procedures.component';
import { TestsComponent } from './tests/tests.component';
import { VitalSignsComponent } from './vital-signs/vital-signs.component';

@NgModule({
  imports: [
    CommonModule,
    PatientHistoryRoutingModule
  ],
  declarations: [
    OcularComponent,
    GeneralHealthComponent,
    AllergiesComponent,
    SxProceduresComponent,
    ImmunizationsComponent,
    CcHistoryComponent,
    VitalSignsComponent,
    OrderSetsComponent,
    ProblemListComponent,
    LabComponent,
    RadComponent,
    TestsComponent,
    MedicationComponent
  ]
})
export class PatientHistoryModule { }
