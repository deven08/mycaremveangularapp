/**
 * This component is used to show allergies page under patient history using iframe
 */

// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-allergies',
  templateUrl: './allergies.component.html'
})
export class AllergiesComponent implements OnInit {
  
  PatientHistoryUrl: Object;
  /**
   * @param  {IframesService} for calling frame url
   */
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=allergies');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
