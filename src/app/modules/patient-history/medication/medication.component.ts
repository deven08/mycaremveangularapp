// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html'
})
export class MedicationComponent implements OnInit {
  PatientHistoryUrl: Object;
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=medication');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
