// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';

// Setting Up Component
@Component({
  selector: 'app-sx-procedures',
  templateUrl: './sx-procedures.component.html'
})
export class SxProceduresComponent implements OnInit {

  PatientHistoryUrl: Object;
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=sx_procedures');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }
}
