// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html'
})
export class TestsComponent implements OnInit {

  TestsUrl: Object;
  constructor(private service: IframesService) {
     this.TestsUrl = this.service.routeUrl('TestsUrl', true);
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }


}
