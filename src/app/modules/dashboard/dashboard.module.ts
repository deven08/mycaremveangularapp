import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {DashboardService} from '@services/dashboard/dashboard.service';
import {PatientSearchService} from '@services/patient/patient-search.service';
import { SharedModule } from '@shared/shared.module';
import {DashboardRoutingModule} from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [
     DashboardComponent
    ],
  providers : [
     DashboardService,
     PatientSearchService,
  ]
})
export class DashboardModule { }

