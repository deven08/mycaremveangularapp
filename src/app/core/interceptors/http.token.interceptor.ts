import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY, throwError } from 'rxjs';

import { JwtService, ErrorService } from '../services';

import { catchError } from 'rxjs/operators';

import {IgnoreAPI} from '@models/ignore.model';

import { environment as env } from '@env/environment';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  // Check for Dot Net Gateway API
  isGateway: boolean;

  auth: string;

  isUploadRequest: boolean;

  constructor(private jwtService: JwtService, private error: ErrorService) {
    this.isGateway = false;
    this.auth = 'Authorization';
    this.isUploadRequest = false;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {



    // Creating Headers
    const headersConfig: any = this.createHeaders(req);

    const request = req.clone({ setHeaders: headersConfig, withCredentials: true });
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        this.error.showErrors(error, env.showAllErrors);
        return throwError(error);
      })
    );
  }


  createHeaders(req: any) {
    const headersConfig = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    const url = req.url.split('/');

    // Check for Dot Net And PHP API request
    this.isGateway = (url[5] === 'v1') ? true : false;



    // Check Token to be used
    const token = (this.isGateway) ? this.jwtService.getGatewayToken()  : this.jwtService.getToken();

    // Binding Token in headers
    if (token) {
      headersConfig[this.auth] = `Bearer ${token}`;
    }

    // Check Request is for Uploading
    const checkLink = url[url.length - 1].split('?')[0];
    const imageArray = IgnoreAPI.filter(p => p === checkLink);

    this.isUploadRequest = (imageArray.length !== 0) ? true : false;
    // this request is for import csv file
    // this.isUploadRequest = (url[5] === 'import') ? true : false;
    if (url[5] === 'import') {
      this.isUploadRequest = true;
    }
    if (this.isUploadRequest)  {
      delete headersConfig['Content-Type'];
    }

    return headersConfig;
  }
}
