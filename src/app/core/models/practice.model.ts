export class practice {

    account_name: string
    account_url_name: string
    allowpatientregistration: string
    created_date:string
    email: string
    externalid: string
    firstname:string
    group_id: string
    id:number
    isactive: boolean
    lastname: string
    middlename: string
    partnerkey: string
    portal_Password: string
    portal_accountid: string
    portal_sso_url:string
    portal_username:string
    reminderfortheday: string
    syncpassword: string
    syncusername: string
  
}
