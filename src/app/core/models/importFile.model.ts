export class ImportFile {
    file_id: number;
    file_name: string;
    total_results: number;
}
