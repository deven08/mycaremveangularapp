
    export interface Manufacturer {
        Id: string;
        Name: string;
        Address: string;
        City: string;
        State: string;
        ZipCode: string;
        ZipExtension: string;
        Fax: string;
        Phone: string;
        MobilePhone: string;
        Email: string;
        Frames: string;
        Lenses: string;
        ContactLenses: string;
        Suppliers: string;
        Medicines: string;
        Accessories: string;
        Active: string;
    }

    export interface Optical {
        Manufacturers: Manufacturer[];
    }

    export interface Result {
        Optical: Optical;
    }

    export interface ManufacturerObject {
        Success: number;
        result: Result;
    }


