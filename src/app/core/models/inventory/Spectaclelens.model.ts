    export interface SpectacleLens {
        ItemId: string;
        UPC: string;
        Name: string;
        ManufacturerId: string;
        Manufacturer: string;
        VendorId: string;
        Vendor: string;
        StyleId: string;
        StyleName: string;
        MaterialId: string;
        MaterialName: string;
        ColorId: string;
        ColorName: string;
        LabId: string;
        LabName: string;
        DesignId: string;
        DesignName: string;
        module_type_id: string;
        Sphere: string;
        Cylinder: string;
        Ssize: string;
        Sadd: string;
        StructureId: string;
        StructureName: string;
        Unit: string;
        MinHeight: string;
        VspCode: string;
        ReturnableFlag: string;
        pricing: string;
        pricingGridId: string;
        GridName: string;
        maxAddPower: string;
        OversizeDiameterCharge: string;
        ValidRangeId: string;
        VspOptionCodeConflict: string;
        VspCodeDescription: string;
        Inventory: string;
        MinQty: string;
        MaxQty: string;
        RetailPrice: string;
        Cost: string;
        BaseCurve: string;
        OD: string;
        OS: string;
        ColorCode: string;
        MaxDiameter: string;
        Notes: string;
        CommissionTypeId: string;
        CommissionTypeName: string;
        GrossPercentage: string;
        Spiff: string;
        Amount: string;
        Treatment: any[];
    }

    export interface Optical {
        Total: string;
        SpectacleLenses: SpectacleLens[];
    }

    export interface Result {
        Optical: Optical;
    }

    export interface RootObject {
        Success: number;
        result: Result;
    }
