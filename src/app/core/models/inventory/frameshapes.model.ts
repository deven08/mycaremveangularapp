export interface FramesShapes {

    Id: string;
    Name: string;
    Active: string;
}
export interface Optical {
    Total: string;
    FramesShapes: FramesShapes[];
}
