export interface Optical {
    ItemId: number;
}

export interface Result {
    Optical: Optical;
}

export interface ContactlensResponse {
    Success: number;
    result: Result;
}


export interface Contactlenses {
    accessToken: string;
    upc: string;
    name: string;
    manufacturerId: string;
    vendorId: string;
    brandId: string;
    packagingId: string;
    typeId: string[];
    materialId: string[];
    wearScheduleId: string[];
    replacementId: string[];
    supplyId: string[];
    colorId: string;
    retailPrice: string;


}

