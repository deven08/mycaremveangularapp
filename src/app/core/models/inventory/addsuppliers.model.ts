

export interface Suppliers {
    supplier: string;
    contact: string;
    title: string;
    frame: boolean;
    lenses: boolean;
    warehouse: boolean;
    contacts: boolean;
    labs: boolean;
    other: boolean;
    location: string;
    website: string;
    vSPLabCode: string;
    jobTransmissionMethod: string;
    labID: string;
    billAccount: string;
    shipAccount: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    zip: string;
    phone: string;
    fax: string;
    email: string;
    facID: string;
    login: string;
    password: string;
    notes: string;

}
