

/**
 * Contact lens rx here this model is used bind the contactlens RX data
 */
export class ContactLensRX {
    data: Array<ContactLensRXData> = [];
}
export class ContactLensRXData {
    Add_CCD: string;
    add_od: string;
    add_os: string;
    aspheric_od: string;
    aspheric_os: string;
    axis_od: string;
    axis_os: string;
    base_od: string;
    base_os: string;
    bc2_od: string;
    bc2_os: string;
    blendidod: string;
    blendidos: string;
    cl_care: string;
    clensodcolor: string;
    clensodcolorname: string;
    clensodmaterial: string;
    clensodmfg: string;
    clensodmfg_name: string;
    clensodname: string;
    clensoscolor: string;
    clensoscolorname: string;
    clensosmaterial: string;
    clensosmfg: string;
    clensosmfg_name: string;
    clensosname: string;
    contactid_od: string;
    contactid_os: string;
    ct_od: string;
    ct_os: string;
    custom_rx: number;
    cylinder_od: string;
    cylinder_os: string;
    del_status: number;
    det_order_id: number;
    dia2_od: string;
    dia2_os: string;
    diameter_od: string;
    diameter_os: string;
    dot_od: string;
    dot_os: string;
    hard_contact: number;
    id: number;
    location_id: number;
    multifocalid_od: number;
    multifocalid_os: number;
    mve_prescription_id: string;
    neutralize_rx: number;
    odlensname: string;
    operator_id: number;
    order_id: number;
    oslensname: string;
    outside_rx: number;
    oz_od: string;
    oz_os: string;
    patient_id: number
    pcr2_od: string;
    pcr2_os: string;
    pcr3_od: string;
    pcr3_os: string;
    pcw2_od: string;
    pcw2_os: string;
    pcw3_od: string;
    pcw3_os: string;
    physician_id: number;
    physician_name: string
    rx_dos: string
    rx_make_od: string
    rx_make_os: string
    rx_type: string;
    segdi_od: string;
    segdi_os: string;
    seght_od: string;
    seght_os: string;
    ship_home_chk: number;
    shpere2_os: string;
    sphere2_od: string;
    sphere2_os: string;
    sphere_od: string;
    sphere_os: string;
    telephone: string;
    warranty_od: number;
    warranty_os: number;
    wearing_plan: string;
}