export class orderSave {
  order_details_id: string;
  order_id: string;
  status: string;
}
export class OrderHistory {
  data: Array<History[]>;
}
export class History {
  entered_date: string;
  id: number;
  order_status_id: number;
  ordertypeid: number;
  patient_id: number;
  total_qty: object;
}
export class HistoryData {
  Id: string;
  Name: string;
  ordertypeID: number;
}
export class OrderFilter {
  filter: Array<object>;
  sort: Array<object>;
}
export class Event {
  query: string;
}
export class Ordertype {
  ordertypeid: number;
}
export class SearchFilter {
  Id: string;
  OrderType: number;
}

export class SoftContact {
  Softcontactlensod: SoftContactLensOd
}
export class SoftContactLensOd {
  id: string;
  retail_price: string;
  manufacturename: string;
  name: string;
  color_code: string;
  upc_code: string;
  sourceod: string;
}

export class OrderStatus {
  id: number;
  del_status: number;
  description: string;
}
/**
 * Other details is data getting otherdetails of the order like lab, status, order etc
 */
export class OtherDetailsModel {
  DeliveredBy: string;
  DeliveredDatetime: string;
  FiitedBy: string;
  FittedDatetime: string;
  InspectedBy: string;
  InspectedDatetime: string;
  LabOrder: string;
  LabStatus: number;
  TrayNumber: number;
  due_date: string;
  lab_id: number;
  order_status_id: number;
  reprocessreason_id: number;
  sold_by: string;
  sold_datetime: string;
}

/**
 * Lab integration for the particular order integated to the lab;
 */
export class LabIntegration {
  message: string;
  referenceId: string;

}

/**
 * Location is data getting location data
 */
export class LocationData {

  Id: number;
  Name: string;
}

export interface Optical {
  Locations: LocationData[];
}

export interface Result {
  Optical: Optical;
}

export interface Locations {
  result: Result;
}

