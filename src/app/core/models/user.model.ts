export class User {
    default_facility: string
    delete_status: string
    email: string
    facility: string
    fname: string
    id: number
    lname: string
    locked:string
    mname: string
    portal_created_date: string
    portal_modified_date: string
    portal_registered_id: string
    provider_color: string
    see_auth: string
    user_group_id: string
    user_type:string
    username: string
}
