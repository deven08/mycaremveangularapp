import { TestBed } from '@angular/core/testing';

import { ErxAdminService } from './erx-admin.service';

describe('ErxAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErxAdminService = TestBed.get(ErxAdminService);
    expect(service).toBeTruthy();
  });
});
