import { ApiService } from '@services/api.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  locationId: any;
  sessionTime: number;
  userLoggedIn: boolean;
  public patientId: any;
  RxValue: any;
  userName: any;
  validateBanner: boolean;
  enableSidebar = false;
  forOrders: boolean;
  sessionGatewayTime: Date;


  /**
   * Time zone of app service
   */
  timeZone: any = [];

  /**
   * Display pop up of app service for display alert popup
   */
  public displayalertsPopUp: boolean;

  orderTypeId: any;
  isPatientSigned: boolean;

  // public orderSelectTypeobj = new Subject<any>();
  // orderSelectTypeobj$ = this.orderSelectTypeobj.asObservable();

  ordersearchvalueService: any[];

  /**
   * Save patient id of app service for storing saving patient id
   */
  savePatientId: any;
  /**
   * Emr disabled of app service for emr tab status
   */
  emrDisabled: number;
  /**
   * Insurance value of app service
   */
  insuranceValue: any = '';
   /**
    * Banner order id of app service
    * for getting the selected order id for displaying patient banner
    */
   bannerOrderId: number;

  /**
   * isUserRegisteredtoPortal  will strore the status of the User Registration to Portal 
   */
  isUserRegisteredtoPortal: boolean ;

  /**
   * portalSSOUrl  will strore the ssourl of the Practice registered to the portal
   */
  portalSSOUrl: string ;
  /**
   * loginUserID store the logged userID from the Token Resposne
   */
  loginUserID: number;
  /**
  * Storing max and min years for DoB
  */
  maxMinYears: string;

  constructor(private api: ApiService) {
    this.locationId = '';
    this.sessionTime = 0;
    this.userLoggedIn = false;
    this.patientId = 0;
    this.isPatientSigned = false;
    this.forOrders = false;
    this.savePatientId = 0;
    this.displayalertsPopUp = false;
    this.bannerOrderId = null;
    const minYear = new Date().getFullYear() - 10;
    const maxYear = new Date().getFullYear() + 10;
    this.maxMinYears = minYear  +':' + maxYear;
  }

  isUserLoggedIn() {
    return this.userLoggedIn;
  }

  checkPatientSigned() {
    return this.isPatientSigned;
  }

  getPatientImage(id) {
    const mainpart = '/patient/' + id + '/image';
    return this.api.getApiMicro(mainpart);
  }

  getPatientDue(id) {
    const mainpart = '/charge/patient/' + id + '/sum/patientDue';
    return this.api.getApiMicro(mainpart);
  }

  getAccountStatusAll(patient) {
    const mainpart = '/patient/' + patient;
    return this.api.getApiMicro(mainpart);
  }

  getHeardAbout() {
    const mainpart = '/heardAbout';
    return this.api.getApiMicro(mainpart);
  }

  getAccountStatus() {
    const mainpart = '/PatientAccountStatuses';
    return this.api.getApiMicro(mainpart);
  }

  updatePatientStatus(body) {
    const patient = this.patientId;
    const mainpart = '/patient/' + patient;
    return this.api.putApiMicro(mainpart, body);
  }
  /**
   * Gets patient in con
   * @returns {object} for getting getPatientInContext values
   */
  getPatientInCON() {
    return this.api.GetApi('IMWAPI/optical/getPatientInContext');
  }

  /**
   * Gets order by search filter
   * @param filterData for getting payload
   * @returns for getting filtered orders data
   */
  getOrderBySearchFilter(filterData: object) {
    const mainPart = '/order/searchFilter';
    return this.api.postApiMicro(mainPart, filterData);
  }

  /**
   * Gets the PraciceInformation registered with the portal  
   * @returns practice information registered to portal
   */
  getPortalRegisteredPractice() {
    const mainpart = '/portalAccountDetails';
    return this.api.getApiMicro(mainpart);
  }

  /**
   * Gets the user information for the logged in user   
   * @returns the the user info for the specified User 
   */
  getUserPortalStatus(userId: number) {
    const mainpart = '/users/' + userId;
    return this.api.getApiMicro(mainpart);
  }

}
