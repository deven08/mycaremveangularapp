import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/services/api.service';
@Injectable({
  providedIn: 'root'
})
export class RxService {
  mainpart = '';
  constructor(private api: ApiService) {
  }

  /**
   * Gets lens selection
   * @param id for getting patient id
   * @returns  {object} for getting latest rx data 
   */
  getLensSelection(id) {
    const payload = {
      filter: [

        {
          field: 'patient_id',
          operator: '=',
          value: id
        }

      ],
      sort: [
        {
          field: 'id',
          order: 'DESC'
        }
      ]
    }

    this.mainpart = '/patient/' + id + '/rx/filter';
    return this.api.postApiMicro(this.mainpart, payload);
  }
  /**
   * Gets lens selection by order id
   * @param patientId for getting patient id
   * @param OrderId for getting order id
   * @returns  {object} for getting rx data by order id
   */
  getLensSelectionByOrderId(patientId, OrderId) {
    const payload = {
      filter: [
        {
          field: 'order_id',
          operator: '=',
          value: OrderId
        }
      ]
    }
    this.mainpart = '/patient/' + patientId + '/rx/filter';
    return this.api.postApiMicro(this.mainpart, payload);
  }

  /**
   * Gets contact lens rx
   * @param {string} patientId ;
   * @returns  contactlens rx data for subscription
   */
  getContactLensRx(patientId: string) {
    this.mainpart = '/patient/' + patientId + '/cl_rx';
    return this.api.getApiMicro(this.mainpart);
  }


  /**
   * Gets spectaclens rx
   * @param {string} patientId;
   * @returns all types of spectaclelens rx data for the subscription;
   */
  getSpectaclensRx(patientId: string) {
    this.mainpart = '/patient/' + patientId + '/rx/listAll';
    return this.api.getApiMicro(this.mainpart);
  }

}
