import { ApiService } from '@services/api.service';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PatientSearchService {
  mainpart: string;
  apiservice: any;
  data: any = [];
  error: any = [];
  /**
   * Creates an instance of patient search service.
   * @param api for calling http methods
   */
  constructor(public api: ApiService) { }

  patientSearch(fname = '', lname = '', dob = '', id = '') {
    const mainpart = 'IMWAPI/searchPatient';
    const inputpart = '&fname=' + fname + '&lname=' + lname + '&dob=' + dob + '&id=' + id + '&maxRecords=100';
    return this.api.GetApi(mainpart, inputpart);
  }

  /**
   * Loads patient is for the setPatientInContext
   * @param {string} patient patientID
   * @returns  data for the subscription
   */
  loadPatient(patient: object) {
    return this.api.postApi('IMWAPI/optical/setPatientInContext', patient);
  }

  getPatientInCON() {
    return this.api.GetApi('IMWAPI/optical/getPatientInContext');
  }
  /**
   * Loads new patient api
   * @param bodyString for payload
   * @returns  {object} patient data
   */
  loadNewPatientApi(bodyString) {
    // alert(bodyString.filter[0].value === "%  %")
   // const resultData = bodyString.filter.filter(p => p.value !== "" && p.value !== null && p.value !== "%  %");
    // const reqbody = {
    //   "filter": bodyString
    // }
    return this.api.postApiMicro('/patient/searchFilter', bodyString);
  }
  /**
   * Gets file items
   * @returns  Patient items details
   */
  getPatientDetails(testdata, page: number) {
    this.mainpart =  '/patient/searchFilter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.api.postApiMicro(this.mainpart, testdata);
  }

}
