import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  Mainurl = '';

  constructor(
    private api: ApiService) { }

  /* ToDo List API*/
  loadTodoList() {
     const Mainurl = '/dashboard/todo';
     return this.api.getApiMicro(Mainurl);
  }

  /* Appointment List API*/
  loadAppList() {
     const Mainurl = '/dashboard/appointments';
     return this.api.getApiMicro(Mainurl);
  }
  loadOrdersList(reqBody: object) {
    const Mainurl = '/order/filter';
    return this.api.postApiMicro(Mainurl, reqBody);
  }
  loadOrderType() {
    const Mainurl =  '/orderTypes ';
    return this.api.getApiMicro(Mainurl);
  }
  loadOrderstatus() {
    const Mainurl = '/orderStatus ';
    return this.api.getApiMicro(Mainurl);
  }
}
