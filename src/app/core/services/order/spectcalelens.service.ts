import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable, Subject } from 'rxjs';
import { SpectacleLens } from '@models/inventory/Spectaclelens.model';

import { ApiService } from '@services/api.service';

@Injectable({
  providedIn: 'root'
})
export class SpectcalelensService {

  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  AddframesUrl = '';
  Getframesfilter: string;

  // reason
  locationscloseObjspec = new Subject<any>();
  locationscloseObjspec$ = this.locationscloseObjspec.asObservable();
  public transactionClose = new Subject<any>();
  transactionClose$ = this.transactionClose.asObservable();
  public spectsObj = new Subject<any>();
  spectsChangeObj$ = this.spectsObj.asObservable();
  cancleSpectsObj = new Subject<string>();
  spectsCancleObj$ = this.cancleSpectsObj.asObservable();
  SpectacleListItemId = new Subject<string>();
  SpectacleListItemId$ = this.SpectacleListItemId.asObservable();
  public advancespectacleSearchObj = new Subject<any>();
  advancespectacleSearchObj$ = this.advancespectacleSearchObj.asObservable();
  public reactivespectaclescreenRefresh = new Subject<any>();
  reactivespectaclescreenRefresh$ = this.reactivespectaclescreenRefresh.asObservable();

  public spectCSVexport = new Subject<any>();
  spectCSVexport$ = this.spectCSVexport.asObservable();
  selectedspectacleupc: any = '';
  editDuplicatespectacleupc: any = '';
  selectedspectacleupcDoubleCLick: any = '';
  MaterialListDropDownData: any = [];
  colorcopyData: any = [];
  mainpart: string;
  inputpart: string;


  constructor(private http: HttpClient, private apiConnector: ApiService) {
  }

  getSpectacles(upc = '', maxRecord = '', materailid = '', lensstyleid = '', colorcode = '', ManufactureId = '',
                Name = '', Id = '') {
    this.APIstring =  'IMWAPI/optical/getSpectaclesLenses';
    this.inputpart = '&upc=' + upc + '&materialId=' + materailid + '&styleId=' + lensstyleid + '&colorId=&colorCode=' + colorcode +
      '&maxRecord=' + '15' + '&manufacturerId=' + ManufactureId + '&name=' + Name + '&Id=' + Id + '&offset=';
    return this.apiConnector.GetApi(this.APIstring,this.inputpart);
  }
  getSpectaclespagenation(upc = '', maxRecord = '', materailid = '', lensstyleid = '', colorcode = '', ManufactureId = '', Name = '',
                          offset = '') {
    this.APIstring =  'IMWAPI/optical/getSpectaclesLenses';
    this.inputpart = '&upc=' + upc + '&materialId=' + materailid + '&styleId=' + lensstyleid + '&colorId=&colorCode=' + colorcode +
      '&maxRecord=' + maxRecord + '&offset=' + offset + '&manufacturerId=' + ManufactureId + '&Name=' + Name;
    return this.apiConnector.GetApi(this.APIstring,this.inputpart);
  }


  Savedata(spectaclesLens: SpectacleLens) {
    const mainpart = 'IMWAPI/optical/addUpdateSpectacleLens';
    return this.apiConnector.postApi(mainpart, spectaclesLens);
  }
  getSpectaclesMasterList() {
    const mainpart = 'IMWAPI/optical/getSpectaclesLensMaterialList?accessToken=';
    const inputpart = '&Id=&Name=&includeInactive=&maxRecord=&offset=&sortBy=&sortOrder=';
    return this.apiConnector.GetApi(mainpart, inputpart);
  }
  // To DO
  getSpectaclesActiveandDeactive(upc = '', maxRecord = '', offset = '', delstatus = '') {
    this.APIstring =  'IMWAPI/optical/getSpectaclesLenses';
    this.inputpart =  '&upc=' + upc +'&maxRecord=' + maxRecord + '&offset=' + offset + '&del_status=' + delstatus;
    return this.apiConnector.GetApi(this.APIstring,this.inputpart);
  }

  // gets the contactlens details
  savespectacletreatment(id = '', value) {

    const mainpart = '/item/' + id + '/lensTreatments';
    return this.apiConnector.postApiMicro(mainpart, value);

  }

  getmeasurmentsmaster() {
    const mainpart = '/spectacleLens/measurement';
    return this.apiConnector.getApiMicro(mainpart);
  }
  getSpectacleLensmeasurmenstByConfiqID(confiqid) {
    const mainpart = '/item/' + confiqid + '/measurement';
    return this.apiConnector.getApiMicro(mainpart);
  }
  getTreatmentLensmeasurmenstByConfiqID(confiqid) {
    const mainpart = '/item/' + confiqid + '/lensTreatment';
    return this.apiConnector.getApiMicro(mainpart);
  }


  SaveSpectacleLensmeasurmenstByConfiqID(confiqid, data) {
    const mainpart = '/item/' + confiqid + '/measurement';
    return this.apiConnector.postApiMicro(mainpart, data);
  }

  SavetreatmentsLensmeasurmenstByConfiqID(confiqid, data) {
    const mainpart = '/item/' + confiqid + '/lensTreatment';
    return this.apiConnector.postApiMicro(mainpart, data);
  }
  SpctaclleLensConfiguratiion(SpecItemId) {
    const mainpart = '/item/' + '4' + '/spectacleConfigurations/';
    return this.apiConnector.getApiMicro(mainpart);
  }
  getPricegrid() {
    const mainpart = '/pricingGrid';
    return this.apiConnector.getApiMicro(mainpart);
  }


}
