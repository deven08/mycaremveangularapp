// import { Contactlenses } from '../../ConnectorEngine/models/addContactlens.model';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Subject } from 'rxjs';
import { AppService } from '@services/app.service';
import { ApiService } from '@services/api.service';
import { Contactlenses } from '@models/inventory/addContactlens.model';

@Injectable({
  providedIn: 'root'
})
export class ContactlensService {
  locationscloseObjcl = new Subject<any>();
  locationscloseObjcl$ = this.locationscloseObjcl.asObservable();
  cancel = false;
  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  AddcontactsUrl = '';
  public transactionCloseContact = new Subject<any>();
  transactionCloseContact$ = this.transactionCloseContact.asObservable();
  public contactObj = new Subject<any>();
  ContactChangeObj$ = this.contactObj.asObservable();
  cancleContactObj = new Subject<string>();
  ContactcancleObj$ = this.cancleContactObj.asObservable();
  public advanceSearchObj = new Subject<any>();
  advanceSearchObj$ = this.advanceSearchObj.asObservable();
  contactListItemId = new Subject<any>();
  contactListItemId$ = this.contactListItemId.asObservable();
  configurationItemId = new Subject<any>();
  cancelbutton = new Subject<any>();
  cancelbutton$ = this.cancelbutton.asObservable();
  savecontactlensupdate = '';

  refreshconfig = new Subject<any>();
  refreshconfig$ = this.refreshconfig.asObservable();

  configurationItemId$ = this.configurationItemId.asObservable();

  showconfigtabconfig = new Subject<any>();
  showconfigtabconfig$ = this.showconfigtabconfig.asObservable();

  loadReactiveContacts = new Subject<any>();
  loadReactiveContacts$ = this.loadReactiveContacts.asObservable();

  closeReactiveObj = new Subject<any>();
  closeReactiveObj$ = this.closeReactiveObj.asObservable();

  public contactCSVexport = new Subject<any>();
  contactCSVexport$ = this.contactCSVexport.asObservable();

  // Contact Lens Subscriptions related declarations;
  public IsSoftContactLens = new Subject<any>();
  IsSoftContactLens$ = this.IsSoftContactLens.asObservable();

  public Disablehardcontactcomponent = new Subject<any>();
  Disablehardcontactcomponent$ = this.Disablehardcontactcomponent.asObservable();

  // Contact Lens Subscriptions related declarations;
  public IsHardContactLens = new Subject<any>();
  IsHardContactLens$ = this.IsHardContactLens.asObservable();

  public SoftContactODvalue = new Subject<any>();
  SoftContactODvalue$ = this.SoftContactODvalue.asObservable();

  public SoftContactOSvalue = new Subject<any>();
  SoftContactOSvalue$ = this.SoftContactOSvalue.asObservable();

  public HardContactODvalue = new Subject<any>();
  HardContactODvalue$ = this.HardContactODvalue.asObservable();

  public HardContactOSvalue = new Subject<any>();
  HardContactOSvalue$ = this.HardContactOSvalue.asObservable();

  public SoftContactOtherODvalue = new Subject<any>();
  SoftContactOtherODvalue$ = this.SoftContactOtherODvalue.asObservable();

  public SoftContactOtherOSvalue = new Subject<any>();
  SoftContactOtherOSvalue$ = this.SoftContactOtherOSvalue.asObservable();


  public HardContactOtherODvalue = new Subject<any>();
  HardContactOtherODvalue$ = this.HardContactOtherODvalue.asObservable();

  public HardContactOtherOSvalue = new Subject<any>();
  HardContactOtherOSvalue$ = this.HardContactOtherOSvalue.asObservable();


  public HardContactbyorderID = new Subject<any>();
  HardContactbyorderID$ = this.HardContactbyorderID.asObservable();

  public contactlensotherdetails: any;
  editlensid: any = '';
  editlensidDoubleClick: any = '';
  editduplicateContactLensId: any;
  parametersData: any = [];
  ColorsData: any = [];
  traillensData: any = [];
  packageDataService: any = [];
  TypeDropDataService: any = [];
  StructureDropDataservice: any = [];
  mainpart: string;
  inputpart: string;


  constructor(private http: HttpClient, app: AppService, private apiConnector: ApiService) {
    // super( http, app);
  }

  /*
  getManufactures(): Observable<any[]> {

    this.Mainurl = this._AppSettings.ImedicUrl;
    this.Accesstoken = this._StateInstanceService.accessTokenGlobal;
    this.APIstring = this.Mainurl + 'IMWAPI/optical/getManufacturers?accessToken=' + this.Accesstoken + '&includeInactive=';
    return this.http.get<any[]>(this.APIstring)
      .do(data => data)


  } */

  /*
  getbrands(): Observable<any[]> {

    this.Mainurl = this._AppSettings.ImedicUrl;
    this.Accesstoken = localStorage.getItem('tokenKey');
    this.APIstring = this.Mainurl + 'IMWAPI/optical/getFrameBrands?accessToken=' + this.Accesstoken + '&maxRecord=&offset=';
    return this.http.get<any[]>(this.APIstring)
      .do(data => data)


  } */
  /*
  getColors(): Observable<any[]> {

    this.Mainurl = this._AppSettings.ImedicUrl;
    this.Accesstoken = localStorage.getItem('tokenKey');
    this.APIstring = this.Mainurl + 'IMWAPI/optical/getFrameColors?accessToken=' + this.Accesstoken + '&maxRecord=&offset=';
    return this.http.get<any[]>(this.APIstring)
      .do(data => data)
  } */


  getcontactlens(ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', name = '') {

    // this.Mainurl = this._AppSettings.ImedicUrl;
    // this.Accesstoken = localStorage.getItem('tokenKey');
    // this.APIstring = this.Mainurl + 'IMWAPI/optical/getContactLenses?accessToken=' + this.Accesstoken + '&upc=' + upc +
    // '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' + colorId + '&maxRecord=' + maxRecord + '';
    const mainpart = 'IMWAPI/optical/getContactLenses';
    const inputPart = '&Id=' + ID + '&upc=' + upc + '&name=' + name + '&manufacturerId=' + manufacturerId + '&brandId=' +
      brandId + '&colorId=' + colorId + '&maxRecord=' + '15' + '';
    return this.apiConnector.GetApi(mainpart, inputPart);
  }
  getcontactlensPagination(
    ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', name = '', offset = '') {

    // this.Mainurl = this._AppSettings.ImedicUrl;
    // this.Accesstoken = localStorage.getItem('tokenKey');
    // this.APIstring = this.Mainurl + 'IMWAPI/optical/getContactLenses?accessToken=' + this.Accesstoken + '&upc=' + upc +
    // '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' + colorId + '&maxRecord=' + maxRecord + '';
    const mainpart = 'IMWAPI/optical/getContactLenses';
    const inputpart = '&Id=' + ID + '&upc=' + upc + '&name=' + name + '&manufacturerId=' + manufacturerId + '&brandId=' +
      brandId + '&colorId=' + colorId + '&maxRecord=' + maxRecord + '&offset=' + offset;
    return this.apiConnector.GetApi(mainpart, inputpart);
  }
  getcontactlensReactivate(ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '') {

    // this.Mainurl = this._AppSettings.ImedicUrl;
    // this.Accesstoken = localStorage.getItem('tokenKey');
    // this.APIstring = this.Mainurl + 'IMWAPI/optical/getContactLenses?accessToken=' + this.Accesstoken + '&upc=' + upc +
    // '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' + colorId + '&maxRecord=' + maxRecord + '';
    const mainpart = 'IMWAPI/optical/getContactLenses';
    const inputPart = '&Id=' + ID + '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' +
      colorId + '&maxRecord=' + maxRecord + '' + '&del_status=1';
    return this.apiConnector.GetApi(mainpart, inputPart);
  }

  SaveContactLensCongfiguration(ContactlensorderID, config: any) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations';
    return this.apiConnector.postApiMicro(mainpart, config);
  }


  UpdateContactLensCongfiguration(ContactlensorderID, config: any, configId: any) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId;
    return this.apiConnector.putApiMicro(mainpart, config);
  }

  Savecontactlens(contactlenses: Contactlenses) {
    const mainpart = 'IMWAPI/optical/addUpdateContactLens';
    return this.apiConnector.postApi(mainpart, contactlenses);
  }

  getContactconfigurationdetails(ContactlensorderID = '') {
    const mainpart = '/item/' + ContactlensorderID + '/configurations';
    return this.apiConnector.getApiMicro(mainpart);
  }

  getSingleContactconfigurationdetails(ContactlensorderID = '', configId) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId;
    return this.apiConnector.getApiMicro(mainpart);
  }

  // config locations
  getContactconfigurationlocationdetails(ContactlensorderID = '', configId) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiConnector.getApiMicro(mainpart);
  }


  getcontactreplacements() {
    const mainpart = '/contactLens/repSchedule';
    return this.apiConnector.getApiMicro(mainpart);
  }

  SaveContactLensCongfigurationLocations(ContactlensorderID, config: any, configId: any) {

    const mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiConnector.postApiMicro(mainpart, config);
  }

  UpdateContactLensCongfigurationLocations(ContactlensorderID, config: any, configId: any) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiConnector.putApiMicro(mainpart, config);
  }

  deleteContactLensCongfigurationLocations(ContactlensorderID, configId: any, locationID) {
    const mainpart = '/item/' + ContactlensorderID + '/configurations/' +
      configId + '/locations/' + locationID + '/deactivate';
    return this.apiConnector.putApiMicro(mainpart, '');
  }

  getcontacttraillens(ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', ) {

    const mainpart = 'IMWAPI/optical/getContactLenses?accessToken=';
    const inputPart = '&Id=' + ID + '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId +
      '&colorId=' + colorId + '&trialOnly=true&maxRecord=' + maxRecord + '';
    return this.apiConnector.GetApi(mainpart, inputPart);
  }
  getPackageDetails() {
    const mainpart = '/contactLens/package';
    return this.apiConnector.getApiMicro(mainpart);
  }

  getcontactParametersDetails() {
    const mainpart = '/item/contactParameters';
    return this.apiConnector.getApiMicro(mainpart);
  }

  getcontactParametersByConfiqID(confiqid) {
    const mainpart = '/item/' + confiqid + '/parameters';
    return this.apiConnector.getApiMicro(mainpart);
  }

  SaveContactLensParametersByConfiqID(confiqid, data) {
    const mainpart = '/item/' + confiqid + '/parameters';
    return this.apiConnector.postApiMicro(mainpart, data);
  }




  // gets the contactlens details
  dynamicfilterforcontactlens(filtervalues: any) {
    const mainpart = '/item/filter';
    return this.apiConnector.postApiMicro(mainpart, filtervalues);

  }

  // ContactlensImageAdding(formData) {
  //   const mainpart = 'IMWAPI/optical/addItemImage';
  //   // let Headers = new HttpHeaders();
  //   // Headers = Headers.append('Authorization', 'Bearer ' + this.Accesstoken);
  //   // const httpOptions = {
  //   //   headers: Headers
  //   // };
  //   return this.apiConnector.postImageApi(mainpart, formData);
  // }

  contactlensListImages(ItemId) {
    // this.Accesstoken = this._StateInstanceService.accessTokenGlobal;
    const mainpart = 'IMWAPI/optical/item/listImages?accessToken=' + this.Accesstoken + '&itemId=' + ItemId;
    return this.http.get(mainpart);
  }


  ContactImageDeleting(formData) {
    const mainpart = 'IMWAPI/optical/item/image/delete';
    let Headers = new HttpHeaders();
    Headers = Headers.append('Authorization', 'Bearer ' + this.Accesstoken);
    const httpOptions = {
      headers: Headers
    };
    return this.http.post(mainpart, formData, httpOptions);
  }

  ContactlensGetImages(Id, ItemId) {
    // this.Accesstoken = this._StateInstanceService.accessTokenGlobal;
    const mainpart = 'IMWAPI/optical/item/getImage?accessToken=' +
      this.Accesstoken + '&itemId=' + ItemId + '&Id=' + Id;
    return this.http.get(mainpart);
  }










}
