import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class UserService {
  constructor(public api: ApiService) {}


  /**
   * Authenticates user
   * Use for authenticate microservices
   * @param params payload from component
   * @returns  return subscription object
   */
  authenticateUser(params: object) {
    return this.api.postApiMicro('/auth/login', params);
  }

  /**
   * Authenticates user gateway
   * Use for authenticate dot net api
   * @param params payload from component
   * @returns  return subscription object
   */
  authenticateUserGateway(params: object) {
    return this.api.postApiMicro('/tokens', params, 2);
  }

  /**
   * Phps token login
   * Use for authenticate EMR Application
   * @param mainpart prefix url for old style api
   * @param inputpart suffix url for old style api
   * @returns  return subscription object
   */
  phpTokenLogin(mainpart: string, inputpart: string) {
    return this.api.GetApi(mainpart, inputpart);
  }

  /**
   * Gets time zone
   * @param timezone for date and time
   * @returns  {object} for update location filter
   */
  getTimeZone(timezone){
    const mainpart = '/locations/filter';
    return this.api.postApiMicro(mainpart, timezone);
  }
}
