import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class JwtService {

  microToken: any;
  phpToken: any;
  gatewayToken: any;

  getToken(): string {
    return this.microToken;
  }

  getPHPToken(): string {
    return this.phpToken;
  }

  saveToken(token: any) {
    this.microToken = token;
    return this.getToken();
  }

  savePHPToken(token: any) {
    this.phpToken = token;
    return this.getPHPToken();
  }

  getGatewayToken(): string {
    return this.gatewayToken;
  }

  saveGatewayToken(token: any) {
    this.gatewayToken = token;
    return this.getGatewayToken();
  }

  destroyToken() {
    this.microToken = null;
    this.phpToken = null;
  }

}
