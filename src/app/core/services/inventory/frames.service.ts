import { Injectable } from '@angular/core';
import { ApiService } from '@app/core';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ErrorService } from '../error.service';
import { Frames } from '@app/core/models/inventory/addframes.model';
import { FrameGender } from '@app/core/models/masterDropDown.model';
// import { Frames } from '@app/core/models/addframes.model';
@Injectable()
export class FramesService {

  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  AddframesUrl = '';
  Getframesfilter: any = '';
  AddLocations = '';
  editframeid: any = '';
  FortrasactionItemIDFrame: any = '';
  editDuplicateId: any = '';
  mainpart: string;
  inputpart: string;
  editLensTreatmentid: any = '';
  editLensTreatmentidDoubleClick: any = '';
  editduplicateLensTreatmentid: any = '';
  lensTreatmentslist: any = [];
  editframeidDoubleClick: any = '';
  framesShapeDropDownData: any = [];
  framesTypeDropDownData: any = [];
  materialsData: any = [];
  framesRimTypeData: any = [];
  framesUsageData: any = [];
  framesSourceData: any = [];
  framesUpcTypeData: any = [];
  lenscolorData: any = [];
  /**
   * Gender  of add modify component
   */
    gender: Array<FrameGender>;
  constructor(private http: HttpClient, private apiservice: ApiService, public errorHandle: ErrorService) {
    this.gender = [];
  }

  /**
   * Params frames service
   * @param {any} payload for getting filter payload
   * @param {string} page  for getting page number
   * @returns {object} for getting frames data by pagination
   */
  getFrameDataByPagination(payload, page: number) {
    this.mainpart = '/item/filter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, payload);
  }
  /**
   * Gets frames grid data
   * @returns {object} for getting frames grid data
   */
  getFramesGridData() {
    var payLoad = {
      "filter": [
        {
          "field": "module_type_id",
          "operator": "=",
          "value": "1"
        }

      ],
      "sort": [
        {
          "field": "id",
          "order": "DESC"
        }
      ]
    }
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart, payLoad);
  }
  /**
   * Gets frames search data
   * @param {any} payLoad for filter payload 
   * @returns {object} for getting frames search data
   */
  getFramesSearchData(payLoad) {
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart, payLoad);
  }
  /**
   * Getframes frames service
   * @param [id] for selected item id
   * @returns {object} fro getting frames data by selected id
   */
  getframes(id = '') {
    this.mainpart = '/item/' + id;
    return this.apiservice.getApiMicro(this.mainpart);
    // .do(data => data);
  }
  getframesDetails(upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '',
    Framename = '', colorcode = '', del_status = '') {
    this.mainpart = 'IMWAPI/optical/getFrames';
    this.inputpart = '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' +
      colorId + '&colorCode=' + colorcode + '&Name=' + Framename + '&del_status=' + del_status + '&maxRecord=' + maxRecord + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  getframesPaginator(upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', Framename = '',
    colorcode = '', del_status = '', offset = '') {
    this.mainpart = 'IMWAPI/optical/getFrames';
    this.inputpart = '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' + colorId + '&colorCode=' + colorcode + '&Name=' + Framename + '&offset=' + offset + '&del_status=' +
      del_status + '&maxRecord=' + maxRecord + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  getframesactivedeactive(upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', Framename = '',
    del_status = '') {
    this.mainpart = 'IMWAPI/optical/getFrames';
    this.inputpart = '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId + '&colorId=' + colorId;
    this.inputpart = '&Name=' + Framename + '&maxRecord=' + maxRecord + '&del_status=' + del_status + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  /**
   * Getframes after save
   * @param [ID] selected Frame Item Id
   * @returns  getting frames data
   */
  getframesAfterSave(ID = '') {
    this.mainpart = 'IMWAPI/optical/getFrames';
    this.inputpart = '&Id=' + ID + '&upc=&manufacturerId=&brandId=&colorId=&colorCode=&Name=&del_status=&maxRecord=';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }

  getRimType() {
    this.mainpart = '/frame/rimType';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  getSource() {
    this.mainpart = '/source';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  getUsage() {
    this.mainpart = '/frame/usage/';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  getUpcType() {
    this.mainpart = '/upcType';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Saves frames
   * @param {any} frames for payload
   * @returns {object}  for saving frames
   */
  SaveFrames(frames) {
    this.mainpart = '/inventory/item';
    // const bodyString = JSON.stringify(frames);
    return this.apiservice.postApiMicro(this.mainpart, frames);
  }
  /**
   * Updates frames
   * @param {any} frames for payload
   * @param {string} id for selected id
   * @returns {object} for updating frames
   */
  updateFrames(frames, id) {
    this.mainpart = '/inventory/item/' + id;
    // const bodyString = JSON.stringify(frames);
    return this.apiservice.putApiMicro(this.mainpart, frames);
  }
  /**
   * Gets locations master data api
   * @returns {object} 
   */
  getLocationsMasterDataApi() {
    this.mainpart = 'IMWAPI/optical/getLocations';
    return this.apiservice.GetApi(this.mainpart, this.inputpart = '');
    // .do(data => data);
  }
  getItemLocations(itemId: any) {
    this.mainpart = 'IMWAPI/optical/getItemLocation';
    this.inputpart = '&itemId=' + itemId + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);

  }
  SaveFrameLocations(Id: any[], itemId: any) {
    this.mainpart = 'IMWAPI/optical/addItemLocation';
    const bodyString = {
      'itemId': itemId,
      'locationId': Id
    };
    // const reqbody = JSON.stringify(bodyString);
    return this.apiservice.postApi(this.mainpart, bodyString);
  }

  /**
   * Gets frames type
   * @returns  for getting frames type dropdown data
   */
  getFramesType() {
    this.mainpart = '/frame/type ';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  /**
   * Gets framesshape
   * @returns  for getting frames Shapes dropdown data
   */
  getFramesshape() {
    this.mainpart = '/frame/shape';
    return this.apiservice.getApiMicro(this.mainpart);
      // .do(data => data);
  }

  // TO get the retail price
  getframeRetailprice(id: any) {
    this.mainpart = '/item/' + id + '/charge';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  // Save the Lens Style of frame
  saveFrameLensStyle(id: any, data: any) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/item/' + id + '/lensStyles';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  /**
   * Saves frame lens materials
   * @param {number} id 
   * @param {object} data 
   * @returns the data for the subscription   of the materails saved for the item.
   */
  saveFrameLensMaterials(id: number, data: object) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/item/' + id + '/lensMaterial';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }
  // Get the Lens Style of frame  saved
  getFrameLensStylesaved(id: any) {
    this.mainpart = '/item/' + id + '/lensStyles';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  /**
   * Gets frame lens materials
   * @param {number} id 
   * @returns data for the subscription materials linked with item.
   */
  getFrameLensMaterials(id: number) {
    this.mainpart = '/item/' + id + '/lensMaterial';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  
  getstockDetail() {
    // FortrasactionItemID
    // const SelectedFrameId = this._InventoryCommonService.FortrasactionItemID;
    this.mainpart = '/stockDetail/' + '1';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  getLensRangeDetail(LensrangeId) {
    this.mainpart = '/frame/lensRange/' + LensrangeId;
    return this.apiservice.getApiMicro(this.mainpart);
  }
  // saveFrameTransactionDetails(data) {
  //   // const reqbody = JSON.stringify(data);
  //   this.mainpart = '/stockDetail';
  //   return this.apiservice.postApiMicro(this.mainpart, data);
  // }
  saveFrameTransactionDetails(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/inventory/transaction';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  /**
   * Gets procedure code
   * @returns   procedure code details api
   */
  getProcedureCode(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/cpt/filter';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  filterTrasactionData(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/stockDetail/filter';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }

  barcodeLabelsData() {
    this.Mainurl = '/barCodeLabels';
    return this.apiservice.getApiMicro(this.Mainurl);
  }

  getFrameLensRange() {
    this.mainpart = '/frame/lensRange';
    return this.apiservice.getApiMicro(this.mainpart);
  }
  postLensRangeComponent(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/frame/lensRange';
    return this.apiservice.postApiMicro(this.mainpart, data);

  }
  frameDeleteLensRange(id) {
    this.mainpart = '/frame/lensRange/' + id;
    return this.apiservice.deleteApiMicro(this.mainpart);
  }
  InvnetoryDetailsFilter(upc) {
    const reqBody = {
      'filter': [
        {
          'field': 'upc_code',
          'operator': '=',
          'value': upc
        }
      ]
    };
    this.Mainurl = '/item/filter';
    return this.apiservice.postApiMicro(this.Mainurl, reqBody);
  }
  /**
   * Frames image adding
   * @param formData for image formdata
   * @returns  {object} saving images
   */
  framesImageAdding(formData) {
    this.mainpart = 'IMWAPI/optical/addItemImage';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }
  framesListImages(ItemId) {
    this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    this.mainpart = 'IMWAPI/optical/item/listImages';
    this.inputpart = '&itemId=' + ItemId;
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
  }
  framesGetImages(Id, ItemId) {
    this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    this.mainpart = 'IMWAPI/optical/item/getImage';
    this.inputpart = '&itemId=' + ItemId + '&Id=' + Id;
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
  }

  /**
   * Frames image deleting
   * @param formData for image formdata
   * @returns  {object} delete images
   */
  framesImageDeleting(formData) {
    this.mainpart = 'IMWAPI/optical/item/image/delete';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }


  /**
   * Frames trace file adding
   * @param formData for Files formdata
   * @returns  {object} saving Files
   */
  framesTraceFileAdding(formData) {
    this.mainpart = 'IMWAPI/optical/addTraceFile';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }

  framesTraceFileGet(token, Id) {
    this.mainpart = 'IMWAPI/optical/getTraceFile';
    this.inputpart = token + '&itemId=' + Id;
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
  }

  /**
   * Frames trace file deleting
   * @param formData for files formdata
   * @returns  {object} delete files  
   */
  framesTraceFileDeleting(formData) {
    this.mainpart = 'IMWAPI/optical/deleteTraceFile';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }

  filterFramesdata(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/framesData/filter';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }






  // frame.service
  getFramesManufacturers() {
    this.mainpart = '/framesData/distinctManufacturer';
    return this.apiservice.getApiMicro(this.mainpart);
  }




  /**
   * Gets framesbrands
   * @returns frame brand collection data
   */
  getFramesbrands() {
    this.mainpart = '/framesData/distinctCollection';
    return this.apiservice.getApiMicro(this.mainpart);
  }


  /**
   * Gets framesstyles
   * @returns  framesStyles
   */
  getFramesstyles() {
    this.mainpart = '/framesData/distinctStyle';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  // frame.service
  getFramescolors() {
    this.mainpart = '/framesData/distinctColor';
    return this.apiservice.getApiMicro(this.mainpart);
  }


  /**
   * Saves frame more details
   * @param data mainpart path that will be added to url
   * @returns  modified data
   */
  saveFrameMoreDetails(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/inventory/item/bulkItems';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }
  /**
   * Saves materials
   * @param data mainpart path that will be added to url
   * @returns  styles data
   */
  saveStyles(data) {
    this.mainpart = '/inventory/item/bulkFrameLensStyle';
    return this.apiservice.putApiMicro(this.mainpart, data);

  }
  /**
   * Saves materials
   * @param data mainpart path that will be added to url
   * @returns  materials data
   */
  saveMaterials(data) {
    this.mainpart = '/inventory/item/bulkFrameLensMaterial';
    return this.apiservice.putApiMicro(this.mainpart, data);
  }


  getGender() {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/gender';
    return this.apiservice.getApiMicro(this.mainpart);
  }

}
