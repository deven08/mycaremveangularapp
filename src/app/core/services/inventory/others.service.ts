import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
// import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/internal/Observable';
import { ErrorService } from '../error.service';
import { Otherinventories } from '@app/core/models/inventory/addotherinventory.model';
// import { Otherinventories } from '@app/core/models/addotherinventory.model';
@Injectable()
export class OthersService {
  editotherinventoryid: any = '';
  editotherinventoryidDoubleClick: any = '';
  editDuplicateotherinventoryid: any = '';
  multiLocationForOtherInventory: any = '';
  TypeDropDataService: any = [];
  StructureDropDataService: any = [];
  categoriesData: any = [];
  mainpart: string;
  inputpart: string;
  constructor(private http: HttpClient, private apiservice: ApiService, public errorHandle: ErrorService) { }
  // old style api
  getotherinventories(upc = '', Name = '', Id = '', vendorId = '', categoryid = '', retailprice = ''): Observable<any> {
    this.mainpart = 'IMWAPI/optical/getOtherInventory';
    this.inputpart = '&upc=' + upc + '&Id=' + Id + '&Name=' + Name + '&vendorId=' + vendorId + '&maxRecord=' + '20000' + '&categoryId='
      + categoryid + '&retailPrice=' + retailprice + '&del_status=0';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  getotherinventoriesPagiantion(upc = '', Name = '', Id = '', vendorId = '', categoryid = '', retailprice = '', maxrecords = '',
    offset = ''): Observable<any> {
    this.mainpart = 'IMWAPI/optical/getOtherInventory';
    this.inputpart = '&upc=' + upc + '&Id=' + Id + '&Name=' + Name + '&vendorId=' + vendorId + '&maxRecord=' + maxrecords + '&categoryId='
      + categoryid + '&retailPrice=' + retailprice + '&offset=' + offset + '&del_status=0';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  /**
   * Getotherinventories reactive
   * @returns reactive  data filtering
   */
  getotherinventoriesReactive(reactiveFilter): Observable<any> {
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart, reactiveFilter);
    // .do(data => data);
  }


  // old styleAPI
  Saveotherinventory(inventories: Otherinventories) {
    this.mainpart = 'IMWAPI/optical/addUpdateOtherInventory';
    // const bodyString = JSON.stringify(inventories);
    return this.apiservice.postApi(this.mainpart, inventories);
  }
  getcategorydata() {
    this.mainpart = '/other/invCategory';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  /**
   * Gets other inventory details 
   * @returns  getting other inventory details details
   */
  getOtherInventoryDetails(otherdetails) {
    this.mainpart = '/item/filter';
    // this.inputpart = '&upc_code=' + upc + '&id=' + Id + '&name=' + Name + '&vendorId=' + vendorId + 
    // '&maxRecord=' + '20000' + '&categoryid'
    //   + categoryid + '&retail_price=' + retailprice + '&del_status=0';
    return this.apiservice.postApiMicro(this.mainpart, otherdetails);
  }
  /**
   * Gets inventory details
   * @returns  inventory filtering details
   */
  getInventoryDetails(testdata, page: number) {
    this.mainpart = '/item/filter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, testdata);
  }
  /**
   * Saves otherdetails data
   * @returns  saved other inventory data
   */
  SaveOtherdetailsData(otherinventory) {
    this.mainpart = '/inventory/item';
    return this.apiservice.postApiMicro(this.mainpart, otherinventory);
  }
  /**
   * Updates inventory others
   * @returns   other inventory details
   */
  updateInventoryOthers(object, item) {
    this.mainpart = '/inventory/item/'.concat(item);
    return this.apiservice.putApiMicro(this.mainpart, object);
  }
}
