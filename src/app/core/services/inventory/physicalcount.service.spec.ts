import { TestBed } from '@angular/core/testing';

import { PhysicalcountService } from './physicalcount.service';

describe('PhysicalcountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PhysicalcountService = TestBed.get(PhysicalcountService);
    expect(service).toBeTruthy();
  });
});
