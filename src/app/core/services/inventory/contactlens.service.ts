import { Injectable } from '@angular/core';
import { ApiService } from '@app/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from '../error.service';
import { ContactReplacement, ContactPackage } from '@app/core/models/masterDropDown.model';
// import { Contactlenses } from '@app/core/models/addContactlens.model';
@Injectable()
export class ContactlensService {
  cancel = false;
  Accesstoken = '';
  Mainurl = '';
  APIstring = '';
  AddcontactsUrl = '';
  savecontactlensupdate = '';
  public contactlensotherdetails: any;
  editlensid: any = '';
  editlensidDoubleClick: any = '';
  editduplicateContactLensId: any;
  parametersData: any = [];
  colorsData: Array<any>;
  traillensData: any = [];
  packageDataService: any = [];
  TypeDropDataService: any = [];
  StructureDropDataservice: any = [];
  mainpart: string;
  inputpart: string;

  /**
   * Contact lens details of contactlens service
   * storing lensdetails
   */
  contactLensDetails: any = [];
    modifyOrDuplicate: string;
    /**
     * Contact replacement of contactlens service
     */
    contactReplacement: Array<ContactReplacement>;
    /**
     * Contact package of contactlens service
     */
    contactPackage: Array<ContactPackage>;
  constructor(private http: HttpClient, private apiservice: ApiService, public errorHandle: ErrorService) {
   this.contactReplacement = [];
   this.contactPackage = [];
    this.colorsData = [];
  }
  //old style
  getcontactlens(ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', name = '') {
    this.mainpart = 'IMWAPI/optical/getContactLenses';
    this.inputpart = '&Id=' + ID + '&upc=' + upc + '&name=' + name + '&manufacturerId=' + manufacturerId + '&brandId=' +
      brandId + '&colorId=' + colorId + '&maxRecord=' + '15' + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }

  /**
   * Getcontactlens new for new style
   * @param {object} filterobject 
   * @returns  
   */
  getcontactlensNew(filterobject) {
    this.mainpart = '/item/filter';
    return this.apiservice.postApiMicro(this.mainpart, filterobject);
  }

  /**
   * Getcontactlensbys pagination
   * @param {object} filterobject 
   * @param {number} page 
   * @returns  
   */
  getcontactlensbyPagination(filterobject,page) {
    this.mainpart =  '/item/filter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.postApiMicro(this.mainpart, filterobject);
  }
 

  SaveContactLensCongfiguration(ContactlensorderID, config: any) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations';
    return this.apiservice.postApiMicro(this.mainpart, config);
  }


  UpdateContactLensCongfiguration(ContactlensorderID, config: any, configId: any) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId;
    return this.apiservice.putApiMicro(this.mainpart, config);
  }

  // Savecontactlens(contactlenses: Contactlenses) {
  //   this.mainpart = 'IMWAPI/optical/addUpdateContactLens';
  //   // const bodyString = JSON.stringify(contactlenses);
  //   return this.apiservice.postApi(this.mainpart, contactlenses); 
  // }
  Savecontactlens(contactlenses) {
    this.mainpart = '/inventory/item';
    // const bodyString = JSON.stringify(contactlenses);
    return this.apiservice.postApiMicro(this.mainpart, contactlenses);
  }
  Updatecontactlens(contactlenses, item) {
    this.mainpart = '/inventory/item/'.concat(item);
    // const bodyString = JSON.stringify(contactlenses);
    return this.apiservice.putApiMicro(this.mainpart, contactlenses);
  }
  getContactconfigurationdetails(ContactlensorderID = '') {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  getSingleContactconfigurationdetails(ContactlensorderID = '', configId) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId;
    return this.apiservice.getApiMicro(this.mainpart);
  }

  // config locations
  getContactconfigurationlocationdetails(ContactlensorderID = '', configId) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiservice.getApiMicro(this.mainpart);
  }


  getcontactreplacements() {
    this.mainpart = '/contactLens/repSchedule';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  SaveContactLensCongfigurationLocations(ContactlensorderID, config: any, configId: any) {

    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiservice.postApiMicro(this.mainpart, config);
  }

  UpdateContactLensCongfigurationLocations(ContactlensorderID, config: any, configId: any) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' + configId + '/locations';
    return this.apiservice.putApiMicro(this.mainpart, config);
  }

  deleteContactLensCongfigurationLocations(ContactlensorderID, configId: any, locationID) {
    this.mainpart = '/item/' + ContactlensorderID + '/configurations/' +
      configId + '/locations/' + locationID + '/deactivate';
    return this.apiservice.putApiMicro(this.mainpart, '');
  }

  getcontacttraillens(ID = '', upc = '', manufacturerId = '', brandId = '', colorId = '', maxRecord = '', ) {

    this.mainpart = 'IMWAPI/optical/getContactLenses';
    this.inputpart = '&Id=' + ID + '&upc=' + upc + '&manufacturerId=' + manufacturerId + '&brandId=' + brandId +
      '&colorId=' + colorId + '&trialOnly=true&maxRecord=' + maxRecord + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    // .do(data => data);
  }
  getPackageDetails() {
    this.mainpart = '/contactLens/package';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  getcontactParametersDetails() {
    this.mainpart = '/item/contactParameters';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  getcontactParametersByConfiqID(confiqid) {
    this.mainpart = '/item/' + confiqid + '/parameters';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  SaveContactLensParametersByConfiqID(confiqid, data) {
    this.mainpart = '/item/' + confiqid + '/parameters';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }




  // gets the contactlens details
  dynamicfilterforcontactlens(filtervalues: any) {

    this.mainpart = '/item/filter';
    // const bodyString = JSON.stringify(filtervalues);
    return this.apiservice.postApiMicro(this.mainpart, filtervalues);

  }

  /**
   * Contactlens image adding
   * @param formData for image formdata
   * @returns  {object} saving images
   */
  ContactlensImageAdding(formData) {
    this.mainpart = 'IMWAPI/optical/addItemImage';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }

  contactlensListImages(ItemId) {
    this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    this.mainpart = 'IMWAPI/optical/item/listImages';
    this.inputpart = '&itemId=' + ItemId;
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
  }


  /**
   * Contacts image deleting
   * @param formData for image formdata
   * @returns  {object} Deleting images
   */
  ContactImageDeleting(formData) {
    this.mainpart = 'IMWAPI/optical/item/image/delete';
    return this.apiservice.postImageApi(this.mainpart, formData);
  }

  ContactlensGetImages(Id, ItemId) {
    this.Accesstoken = ''
    // this._StateInstanceService.accessTokenGlobal;
    this.mainpart = 'IMWAPI/optical/item/getImage';
    this.inputpart = '&itemId=' + ItemId + '&Id=' + Id;
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
  }
  // private handleError(error: Response) {
  //   return Observable.throwError(error.statusText);
  // }





  // contactlens.service
  getContactlensColors(id: '') {
    this.mainpart = '/contactLens/color/' + id;
    return this.apiservice.getApiMicro(this.mainpart);
  }
}
